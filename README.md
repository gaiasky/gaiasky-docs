# Gaia Sky Documentation

This project contains the documentation of Gaia Sky.

-  Latest documentation: https://gaia.ari.uni-heidelberg.de/gaiasky/docs/master


Gaia Sky resources:

-  [Gaia Sky home page](https://zah.uni-heidelberg.de/gaia/outreach/gaiasky/)
-  [Gaia Sky repository](https://codeberg.org/gaiasky/gaiasky)


# Building the Documentation

We use ``pipenv`` to configure the build virtual environment. The documentation build process depends on Python packages listed in the `Pipfile` file at the project's root. This file is read by ``pipenv``, which initialises a virtual environment and installs required packages into it.

Install ``pipenv`` using your Linux distribution package manager, and initialise a new virtual environment in the root directory of the project. For example, in Arch Linux use:

```bash
pacman -S python-pipenv  # Install pipenv
pipenv install           # Initialise virual environment
```

In other Linux distributions the `pipenv` package may also be named `pipenv` or `python3-pipenv`. For Homebrew, or if `pipenv` is unavailable from your package manager, follow the [installation instructions in the `pipenv` documentation](https://pipenv.pypa.io/en/latest/installation.html).

You'll need our customised `sphinx-rtd-dark-mode` theme if building the documentation in HTML format. Clone it from [its GitHub repository](https://github.com/langurmonkey/sphinx_rtd_dark_mode), build and install into the virtual environment:

```bash
cd /path/to/sphinx_rtd_dark_mode
python3 setup.py sdist
cd -  # Back to the top-level directory
pipenv install /path/to/sphinx_rtd_dark_mode/dist/sphinx_rtd_dark_mode-1.3.0.tar.gz
```

Since the documentation of archetypes and components is generated from `gaiasky` sources, you need the full source distribution, available from the [main repository on Codeberg](https://codeberg.org/gaiasky/gaiasky). Additionally, set the environment variable `GS` to the absolute path of the top-level `gaiasky` source directory:

```bash
export GS=/path/to/gaiasky
```

**Note:** All `make` commands in the rest of this guide are expected to be run from the `docs/` subdirectory as the current working directory, where the files `makefile` and `conf.py` for Sphinx are situated.

```
cd docs
```

To generate and build the full HTML documentation set for all versions of Gaia Sky, run the following command:

```
make generate versions
```

The ``generate`` target generates documentation pages of archetypes and components from `gaiasky` sources.

Note that the ``sphinx-multiversion`` Sphinx extension, used by default, retrieves the documentation source files directly from the local Git repository, and will ignore any local modifications made to checked-out files in the Git working directory. If you've made any changes, commit them first.

**Internal use only.** To generate, build **and** publish to the server (requires a valid SSH key), run:

```
make server   # equivalent to 'make generate versions publish'
```

To make a static HTML documentation set for a single version from the Git working directory, check out the Git tag corresponding to the desired version, comment out the `"sphinx-multiversions"` line in the `docs/conf.py` file, and run:

```
make local   # equivalent to 'make generate html'
```

You can also build the documentation in a large variety of output formats.  
**Note:** Make sure to run `make generate` once before using any of these `make` targets.

```bash
make html       # make standalone HTML files
make versions   # make versioned HTML files
make dirhtml    # make HTML files named index.html in directories
make singlehtml # make a single large HTML file
make pickle     # make pickle files
make json       # make JSON files
make htmlhelp   # make HTML files and an HTML help project
make qthelp     # make HTML files and a qthelp project
make applehelp  # make an Apple Help Book
make devhelp    # make HTML files and a Devhelp project
make epub       # make an epub book
make epub3      # make an epub3 book
make latex      # make LaTeX files. Accepts PAPER=a4 or PAPER=letter on command line
make latexpdf   # make LaTeX files and run them through pdflatex
make latexpdfja # make LaTeX files and run them through platex/dvipdfmx
make text       # make text files
make man        # make manual pages
make texinfo    # make Texinfo files
make info       # make Texinfo files and run them through makeinfo
make gettext    # make PO message catalogs
make changes    # make an overview of all changed/added/deprecated items
make xml        # make Docutils-native XML files
make pseudoxml  # make pseudoxml-XML files for display purposes

# These targets run checks on the project but produce no output.
make linkcheck  # check all external links for integrity
make doctest    # run all doctests embedded in the documentation (if enabled)
make coverage   # run coverage check of the documentation (if enabled)
make dummy      # check document sources for syntax errors
```

