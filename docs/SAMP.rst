.. _samp:

.. raw:: html

   <div class="new new-prev">Since 2.0.0</div>

SAMP integration
****************

Gaia Sky supports interoperability via `SAMP <http://www.ivoa.net/documents/SAMP/>`__.
However, due to the nature of Gaia Sky, not all functions are yet implemented and not all types of data tables
are supported.

Since Gaia Sky only displays 3D positional information there are a few restrictions as to how the integration with SAMP is implemented.

The current implementation only allows using Gaia Sky as a SAMP client. This means that
when Gaia Sky is started, it automatically looks for a preexisting SAMP hub. If it is found, then
a connection is attempted. If it is not found, then Gaia Sky will attempt further
connections at regular intervals of 10 seconds. Gaia Sky will
never run its own SAMP hub, so the user always needs a SAMP-hub application (Topcat,
Aladin, etc.) to use the interoperability that SAMP offers.

Also, the only supported format in SAMP is VOTable through the STIL data provider. The datasets must be curated as described in the :ref:`Preparing datasets <stil-data-provider>` section. 



Implemented features
====================

The following SAMP features are implemented:

-  **Load VOTable** (``table.load.votable``) -- the VOTable will be loaded into Gaia Sky if it adheres to the format above.
-  **Highlight row** (``table.highlight.row``) -- the row (object) is set as the new focus if the table it comes from is already loaded. Otherwise, Gaia Sky will **not** load the table lazily.
-  **Broadcast selection** (``table.highlight.row``) -- when a star of a table loaded via SAMP is selected, Gaia Sky broadcasts it as a row highlight, so that other clients may act on it.
-  **Point at sky** (``coord.pointAt.sky``) -- puts camera in free mode and points it to the specific direction.
-  **Multi selection** (``table.select.rowList``) -- Gaia Sky does not have multiple selections so far, so only the first one is used right now.

Unimplemented features
======================

The following SAMP functions are not yet implemented:

-  ``table.load.*`` -- only VOTable supported.
-  ``image.load.fits``
-  ``spectrum.load.ssa-generic``
-  ``client.env.get``
-  ``bibcode.load``
-  ``voresource.loadlist``
-  ``coverage.load.moc.fits``



