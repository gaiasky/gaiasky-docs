About
*****

Contact
=======

If you have doubts or issues you can contact us using one of the
following methods.

-  Submit an issue to our `bug tracking system <https://codeberg.org/gaiasky/gaiasky/issues>`__.
-  Drop us a line at `tsagrista@ari.uni-heidelberg.de <mailto:tsagrista@ari.uni-heidelberg.de>`__.

Visit our homepage at `gaiasky.space <https://gaiasky.space>`__.

Author
======

**Toni Sagristà Sellés** -- `tonisagrista.com <https://tonisagrista.com>`__

Acknowledgements
================

The most up to date list of acknowledgements is always in the
`ACKNOWLEDGEMENTS.md <https://codeberg.org/gaiasky/gaiasky/src/branch/master/ACKNOWLEDGEMENTS.md>`__ file.

Funding for the project is provided by the following agencies:

-  `ZAH <https://zah.uni-heidelberg.de>`__
-  `DLR <https://www.dlr.de>`__
-  `BMWi <https://www.bmwi.de>`__


Stats
=====

Gaia Sky download numbers (including documentation requests and data packages) can be found `here <https://stats.gaiasky.space>`__.
