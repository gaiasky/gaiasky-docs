.. _mwgaiadn-plntleiden-2024:

.. role:: grey
.. role:: red
.. role:: green
.. role:: yellow
.. role:: orange
.. role:: blue
   
Outreach tutorial (MWGaiaDN 2024)
=================================

   .. hint:: This tutorial is designed to be followed with Gaia Sky 3.5.8!

This page contains the tutorial on the general usage of Gaia Sky given at the MWGaiaDN Induction School in PLNT Leiden (February 2, 2024).

The main aim of this tutorial is to provide a general understanding of the scripting system in Gaia Sky, and to train the participants in the creation of outreach videos.

**Presentation (slides):** Find the accompanying presentation for this tutorial `here <https://gaia.ari.uni-heidelberg.de/gaiasky/presentation/202402/mwgaiadn/>`__.

The topics covered in this tutorial are the following:

- Gaia Sky introduction:

  - Dataset manager.
  - Controls, movement, selection.
  - User interface.
  - Camera, type visibility.
  - Render modes (3D, planetarium, 360, reprojection).
  - Visual settings.
  - External datasets (loading, filters, SAMP).

- Scripting:

  - The API (basic functions, etc.).
  - Writing Python scripts for Gaia Sky.
  - Running scripts on a Gaia Sky instance.
  - Advanced topics: camera and scene parking runnables.

- Camera paths:

  - Recording and playback.
  - Keyframe system.
  - Still frame output system.

- Still frame output mode.

*Estimated duration:* 3 hours

Before starting...
------------------

In order to follow the course it is strongly recommended to have a local installation of Gaia Sky 3.5.8 so that you can explore and try out the teachings for yourself. In order to install Gaia Sky, follow the instructions for your operating system in :ref:`the installation section <installation>` of the Gaia Sky documentation.

Welcome window
--------------

When your start up Gaia Sky 3.5.8, you will be greeted with this view:

.. figure:: img/mwgaiadn-2024/202402_welcome.jpg
    :alt: Gaia Sky welcome UI
    :align: center
    :target: ../_images/202402_welcome.jpg

    Gaia Sky welcome UI


From here you can access the global preferences (cog wheel to the bottom right), fire up the **dataset manager**, or start Gaia Sky.

Dataset manager
---------------

The dataset manager is used to download, update and manage datasets. It consists of two tabs:

- **Available for download** --- contains datasets that are available to be downloaded and installed.  
- **Installed** --- contains the datasets currently installed locally.

The first time your start Gaia Sky you need to download at least the **Base data pack** (key ``default-data``) to even be able to start the program. The base data pack contains essential data like most of the Solar System, the Milky Way, grids, constellations and other objects.


.. figure:: img/mwgaiadn-2024/202402_dataset-manager.jpg
    :alt: The dataset manager
    :align: center
    :target: ../_images/202402_dataset-manager.jpg

    The dataset manager

You can explore the available datasets freely.

Basic controls
--------------

When Gaia Sky is ready to go, you will be presented with this screen:

.. figure:: img/mwgaiadn-2024/202402_ui-initial.jpg
    :alt: The scene
    :align: center
    :target: ../_images/202402_ui-initial.jpg

    Gaia Sky default scene 

In it you can see a few things already. To the bottom right the :ref:`focus panel <focus-panel>` tells you that you are in focus mode, meaning that all our movement is relative to the focus object. The default focus of Gaia Sky is the Earth. You can also see in the **quick info bar** at the top that our focus is the :green:`Earth`, and that the closest object to our location is also the :blue:`Earth`. Additionally you see that your home object is again the :orange:`Earth`. Finally the **control panes** are accessible via the buttons anchored to the top left. If you click on one of these buttons, the respective pane opens. We will use them later.

Movement
~~~~~~~~

But right now let's try some movement. In **focus mode** the camera will by default orbit around the focus object. **Try clicking and dragging with your left mouse button**. The camera should orbit around the Earth showing parts of the surface which were previously hidden. You will notice that the whole scene rotates. Now **try scrolling with your mouse wheel**. The camera will move either farther away from (scroll down) or closer up to (scroll up) the Earth. You can always press and hold :guilabel:`z` to speed up the camera considerably. Now, if you **click and drag with your right mouse button**, you can offset the focus object from the center, but your movement will still be relative to it.

You can also use your keyboard arrows :guilabel:`←` :guilabel:`↑` :guilabel:`→` :guilabel:`↓` to orbit left or right around the focus object, or move closer to or away from it.

You can use :guilabel:`shift` with a **mouse drag** in order to roll the camera. 

More information on the controls is available in the :ref:`controls section <controls>` of the Gaia Sky user manual.

Selection
~~~~~~~~~

You can change the focus by simply **double clicking** on any object on the scene. You can also press :guilabel:`f` to bring up the **search** dialog where you can look up objects by name. Try it now. Press :guilabel:`f` and type in "mars", without the quotes, and hit :guilabel:`esc`. You should see that the camera now points in the direction of :red:`Mars`. To actually go to :red:`Mars` simply scroll up until you reach it, or click on the |goto| icon next to the name in the focus info panel. If you do so, Gaia Sky takes control of the camera and brings you to :red:`Mars`. 

If you want to move instantly to your current focus object, hit :guilabel:`ctrl` + :guilabel:`g`.

At any time you can use the :guilabel:`home` key in your keyboard to return back to Earth or whatever :orange:`home` object you have defined in the :ref:`configuration file <configuration-file>`.

.. |goto| image:: ../img/ui/go-to.png
   :class: filter-invert

The User Interface
------------------

The user interface of Gaia Sky consists of basically two components: keyboard shortcuts and a graphical user interface in the form of a few panes, buttons and windows. The most important of those are the **control panes**, accessible via a series of buttons anchored to the left.

.. figure:: img/mwgaiadn-2024/202402_ui-all.jpg
    :alt: Gaia Sky User Unterface
    :align: center
    :target: ../_images/202402_ui-all.jpg

    Gaia Sky user interface with the most useful functions

Control panes
~~~~~~~~~~~~~

.. hint:: The control panes are described in detail in :ref:`its own section <user-interface>` of the Gaia Sky user manual.

The control panes (previously called *control panel* in the old UI---it can still be used but is off by default) are made up of seven different panes, accessed using the buttons anchored to the top-left: *Time*, *Camera*, *Type visibility*, *Visual settings*, *Datasets*, *Location log* and *Bookmarks*. Each pane can be **expanded** and **collapsed** by clicking on the button or by using the respective keyboard shortcut (listed in the button tooltip).

Anchored to the bottom-left of the screen we can find six buttons to perform a few special actions:

- |minimap| Toggle the mini-map
- |dsload| Load a dataset
- |prefsicon| Open the preferences window
- |logicon| Show the session log
- |helpicon| Show the help dialog
- |quiticon| Exit Gaia Sky

Quick info bar
~~~~~~~~~~~~~~

To the top of the screen you can see the **quick info bar** which provides information on the current time, the :green:`current focus` object (if any), the :blue:`current closest` object to our location and the :orange:`current home` object. The colors of these objects (:green:`green`, :blue:`blue`, :orange:`orange`) correspond to the colors of the crosshairs. The crosshairs can be enabled or disabled from the interface tab in the preferences window (use :guilabel:`p` to bring it up).

.. |minimap| image:: ../img/ui/map-icon.png
   :class: filter-invert
.. |dsload| image:: ../img/ui/open-icon.png
   :class: filter-invert
.. |prefsicon| image:: ../img/ui/prefs-icon.png
   :class: filter-invert
.. |logicon| image:: ../img/ui/log-icon.png
   :class: filter-invert
.. |helpicon| image:: ../img/ui/help-icon.png
   :class: filter-invert
.. |quiticon| image:: ../img/ui/quit-icon.png
   :class: filter-invert

Debug panel
~~~~~~~~~~~

Gaia Sky has a built-in debug information panel that provides system information and is hidden by default. You can bring it up with :guilabel:`ctrl` + :guilabel:`d`, or by ticking the "*Show debug info*" check box in the system tab of the preferences window. By default, the debug panel is collapsed.

.. figure:: ../img/debug/debug-collapsed.jpg
    :alt: Collapsed debug panel
    :align: center
    :target: ../_images/debug-collapsed.jpg

    Collapsed debug panel

You can expand it with the ``+`` symbol to get additional information.

.. figure:: ../img/debug/debug-expanded.jpg
    :alt: Expanded debug panel
    :align: center
    :target: ../_images/debug-expanded.jpg

    Expanded debug panel

As you can see, the debug panel shows information on the current graphics device, system and graphics memory, the amount of objects loaded and on display, the octree (if a LOD dataset is in use) or the SAMP status.

Additional debug information can be obtained in the system tab of the help dialog (:guilabel:`?` or :guilabel:`h`).

Time controls
~~~~~~~~~~~~~

Gaia Sky can simulate time. Play and pause the simulation using the |play-icon|/|pause-icon| ``Play/Pause`` buttons in the time pane, or toggle using :guilabel:`Space`. You can also change time warp, which is expressed as a scaling factor, using the provided **Warp factor** slider. Use :guilabel:`,` or |bwd-icon| and :guilabel:`.` or |fwd-icon| to divide by 2 and double the value of the time warp respectively. If you keep either of those pressed, the warp factor will increase or decrease steadily.

Use the :guilabel:`Reset time and warp` button to reset the time warp to x1, and set the time to the current real world time (UTC).

.. figure:: ../img/ui/pane-time.jpg  
   :width: 35%                                  
   :align: center
    
   The time pane in the controls window of Gaia Sky.


.. |play-icon| image:: ../img/ui/media/iconic-media-play.png
   :class: filter-invert
.. |pause-icon| image:: ../img/ui/media/iconic-media-pause.png
   :class: filter-invert
.. |fwd-icon| image:: ../img/ui/media/iconic-media-skip-forward.png
   :class: filter-invert
.. |bwd-icon| image:: ../img/ui/media/iconic-media-skip-backward.png
   :class: filter-invert

Now, go ahead and press :guilabel:`home`. This will bring us back to Earth. Now, start the time with |play-icon| or :guilabel:`space` and **drag the slider slightly to the right** to increase its speed. You will see that the Earth rotates faster and faster as you move the slider to the right. Now, **drag it to the left** until time is reversed and the Earth starts rotating in the opposite direction. Now time is going backwards!

If you set the time warp high enough you will notice that as the bodies in the Solar System start going crazy, the stars start to slightly move. That's right: Gaia Sky also simulates proper motions.

Camera modes
------------

We have already talked about the **focus camera mode**, but Gaia Sky provides some more :ref:`camera-modes`:

- :guilabel:`0` - **Free mode**: the camera is not locked to a focus object and can roam freely. The movement is achieved with the scroll wheel of your mouse, and the view is controlled by clicking and draggin the left and right mouse buttons
- :guilabel:`1` - **Focus mode**: the camera is locked to a focus object and its movement depends on it
- :guilabel:`2` - **Game mode**: similar to free mode but the camera is moved with :guilabel:`w`:guilabel:`a`:guilabel:`s`:guilabel:`d` and the view (pitch and yaw) is controlled with the mouse. This control system is commonly found in FPS (First-Person Shooter) games on PC
- :guilabel:`3` - **Spacecraft mode**: take control of a spacecraft (outside the scope of this tutorial)

The most interesting mode is **free mode** which lets us roam freely. Go ahead and press :guilabel:`0` to try it out. The controls are a little different from those of **focus mode**, but they should not be to hard to get used too. Basically, use your **left mouse button** to yaw and pitch the view, use :guilabel:`shift` to roll, and use the **right mouse button** to pan.

Special render modes
--------------------

There are three special render modes: :ref:`3D mode <3d-mode>`, :ref:`planetarium mode <planetarium-mode>`, :ref:`panorama mode <panorama-mode>` and :ref:`orthosphere view <orthosphere-mode>`. You can access these modes using the buttons at the bottom of the camera pane or the following shortcuts:

- |3d-icon| or :guilabel:`ctrl` + :guilabel:`s` - 3D mode
- |dome-icon| or :guilabel:`ctrl` + :guilabel:`p` - Planetarium mode
- |cubemap-icon| or :guilabel:`ctrl` + :guilabel:`k` - Panorama mode
- |orthosphere-icon| or :guilabel:`ctrl` + :guilabel:`j` - Orthosphere view

.. |3d-icon| image:: ../img/ui/3d-icon.png
   :class: filter-invert
.. |dome-icon| image:: ../img/ui/dome-icon.png
   :class: filter-invert
.. |cubemap-icon| image:: ../img/ui/cubemap-icon.png
   :class: filter-invert
.. |orthosphere-icon| image:: ../img/ui/orthosphere-icon.png
   :class: filter-invert

Component visibility
--------------------

The visibility of most graphical elements can be switched off and on using the buttons in the **type visibility pane** in the control panel.
For example you can hide the stars by clicking on the
``stars`` |stars| button. The object types available are the following:

-  |stars| -- Stars
-  |planets| -- Planets
-  |moons| -- Moons
-  |satellites| -- Satellites
-  |asteroids| -- Asteroids
-  |clusters| -- Star clusters
-  |milkyway| -- Milky Way
-  |galaxies| -- Galaxies
-  |nebulae| -- Nebulae
-  |meshes| -- Meshes
-  |equatorial| -- Equatorial grid
-  |ecliptic| -- Ecliptic grid
-  |galactic| -- Galactic grid
-  |labels| -- Labels
-  |titles| -- Titles
-  |orbits| -- Orbits
-  |locations| -- Locations
-  |cosmiclocations| -- Cosmic locations
-  |countries| -- Countries
-  |constellations| -- Constellations
-  |boundaries| -- Constellation boundaries
-  |ruler| -- Rulers
-  |effects| -- Particle effects
-  |atmospheres| -- Atmospheres
-  |clouds| -- Clouds
-  |axes| -- Axes
-  |arrows| -- Velocity vectors
-  |others| -- Others

.. |stars| image:: ../img/ui/ct/icon-elem-stars.png
   :class: filter-invert
.. |planets| image:: ../img/ui/ct/icon-elem-planets.png
   :class: filter-invert
.. |moons| image:: ../img/ui/ct/icon-elem-moons.png
   :class: filter-invert
.. |satellites| image:: ../img/ui/ct/icon-elem-satellites.png
   :class: filter-invert
.. |asteroids| image:: ../img/ui/ct/icon-elem-asteroids.png
   :class: filter-invert
.. |clusters| image:: ../img/ui/ct/icon-elem-clusters.png
   :class: filter-invert
.. |milkyway| image:: ../img/ui/ct/icon-elem-milkyway.png
   :class: filter-invert
.. |galaxies| image:: ../img/ui/ct/icon-elem-galaxies.png
   :class: filter-invert
.. |nebulae| image:: ../img/ui/ct/icon-elem-nebulae.png
   :class: filter-invert
.. |meshes| image:: ../img/ui/ct/icon-elem-meshes.png
   :class: filter-invert
.. |equatorial| image:: ../img/ui/ct/icon-elem-equatorial.png
   :class: filter-invert
.. |ecliptic| image:: ../img/ui/ct/icon-elem-ecliptic.png
   :class: filter-invert
.. |galactic| image:: ../img/ui/ct/icon-elem-galactic.png
   :class: filter-invert
.. |labels| image:: ../img/ui/ct/icon-elem-labels.png
   :class: filter-invert
.. |titles| image:: ../img/ui/ct/icon-elem-titles.png
   :class: filter-invert
.. |orbits| image:: ../img/ui/ct/icon-elem-orbits.png
   :class: filter-invert
.. |locations| image:: ../img/ui/ct/icon-elem-locations.png
   :class: filter-invert
.. |cosmiclocations| image:: ../img/ui/ct/icon-elem-cosmiclocations.png
   :class: filter-invert
.. |countries| image:: ../img/ui/ct/icon-elem-countries.png
   :class: filter-invert
.. |constellations| image:: ../img/ui/ct/icon-elem-constellations.png
   :class: filter-invert
.. |boundaries| image:: ../img/ui/ct/icon-elem-boundaries.png
   :class: filter-invert
.. |ruler| image:: ../img/ui/ct/icon-elem-ruler.png
   :class: filter-invert
.. |effects| image:: ../img/ui/ct/icon-elem-effects.png
   :class: filter-invert
.. |atmospheres| image:: ../img/ui/ct/icon-elem-atmospheres.png
   :class: filter-invert
.. |clouds| image:: ../img/ui/ct/icon-elem-clouds.png
   :class: filter-invert
.. |axes| image:: ../img/ui/ct/icon-elem-axes.png
   :class: filter-invert
.. |arrows| image:: ../img/ui/ct/icon-elem-arrows.png
   :class: filter-invert
.. |others| image:: ../img/ui/ct/icon-elem-others.png
   :class: filter-invert

Velocity vectors
~~~~~~~~~~~~~~~~

One of the elements, the **velocity vectors**, enable a few properties when selected. See the :ref:`velocity vectors section <velocityvectors>` in the Gaia Sky user manual for more information on that.

*  **Number factor** -- control how many velocity vectors are rendered. The stars are sorted by magnitude (ascending) so the brightest stars will get velocity vectors first
*  **Length factor** -- length factor to scale the velocity vectors
*  **Color mode** -- choose the color scheme for the velocity vectors
*  **Show arrowheads** -- Whether to show the vectors with arrow caps or not

.. hint:: Control the width of the velocity vectors with the **line width** slider in the **visual settings** pane.

.. figure:: ../img/screenshots/velocity-vectors.jpg
    :alt: Velocity vectors in Gaia Sky
    :align: center
    :target: ../_images/velocity-vectors.jpg

    Velocity vectors in Gaia Sky

Visual settings
---------------

The **visual settings** pane contains a few options to control the shading of stars and other elements:

-  **Star brightness** -- control the brightness of stars
-  **Magnitude multiplier** -- exponent of power function that controls the brightness of stars. Controls the brightness difference between bright and faint stars 
-  **Star glow factor** -- close-by star size
-  **Point size** -- size of point-like stars and other objects
-  **Base star level** -- the minimum brightness level for all stars
-  **Ambient light** -- control the amount of ambient light. This only
   affects the models such as the planets or satellites
-  **Line width** -- control the width of all lines in Gaia Sky (orbits, velocity vectors, etc.)
-  **Label size** -- control the size of the labels
-  **Elevation multiplier** -- scale the height representation for planets with elevation maps

.. figure:: ../img/ui/pane-visual-settings.jpg
    :alt: Visual settings pane
    :align: center
    :target: ../_images/pane-visual-settings.jpg

    The visual settings pane.

External datasets
-----------------

Gaia Sky supports the loading of external datasets at runtime. Right now, ``VOTable``, ``csv`` and ``FITS`` formats are supported. Gaia Sky needs some metadata in the form of UCDs or column names in order to parse the dataset columns correctly. Refer to the :ref:`stil-data-provider` section of the Gaia Sky user manual for more information on how to prepare your dataset for Gaia Sky.

The datasets loaded in Gaia Sky at a certain moment can be found in the :ref:`datasets <datasets>` pane of the control panel.

.. figure:: img/mwgaiadn-2024/202402_ds-1.jpg
    :alt: Datasets pane
    :align: center
    :target: ../_images/202402_ds-1.jpg

    Datasets pane of Gaia Sky.


There are four main ways to load new datasets into Gaia Sky:

* Directly from the UI, using the |dsload| button or pressing :guilabel:`ctrl` + :guilabel:`o`
* Through SAMP, via a connection to another astronomy software package such as Topcat or Aladin
* Via a script (addressed later on in the workshop if time allows)
* Creating a descriptor file, saving it, along with the dataset, in the data directory, and selecting it in the dataset manager (avanced!)

**Loading a dataset from the UI** -- Go ahead and remove the current star catalog by clicking on the |binicon| icon in the datasets pane. Now, download a raw `Hipparcos dataset VOTable <https://gaia.ari.uni-heidelberg.de/gaiasky/files/catalogs/hip/hipparcos.vot>`__, click on the |dsload| icon (or press :guilabel:`ctrl` + :guilabel:`o`) and select the file. In the next dialog just click :guilabel:`Ok` to start loading the catalog. In a few moments the Hipparcos new reduction dataset should be loaded into Gaia Sky.

**Loading a dataset via SAMP** -- This section presupposes that Topcat is installed on the machine and that the user knows how to use it to connect to the VO to get some data. The following video demonstrates how to do this (`Odysee mirror <https://odysee.com/@GaiaSky:8/gaia-sky-loading-data-with-topcat:9>`__, `YouTube mirror <https://youtu.be/sc0q-VbeoPE>`__):

.. figure:: img/dpac-2020/samp.jpg
	:width: 500px
	:align: center
	:target: https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202401_mwgaiadn/tap-load-topcat.mkv

	Loading a dataset from Topcat through SAMP (click for video)

**Loading a dataset via scripting** -- Wait for the scripting section of this course.

**Preparing a descriptor file** -- Not addressed in this tutorial. See :ref:`the catalog formats section <catalog-formats>` for more information.

Working with datasets
~~~~~~~~~~~~~~~~~~~~~

All datasets loaded are displayed in the datasets pane in the control panel.
A few useful tips for working with datasets:

-  The visibility of individual datasets can be switched on and off by clicking on the |eye-s-on| button
-  Remove datasets with the |binicon| button
-  You can **highlight a dataset** by clicking on the |highlight-s-off| button. The highlight color is defined by the color selector right on top of it. Additionally, we can map an attribute to the highlight color using a color map. Let's try it out:
  
    1.  Click on the color box in the Hipparcos dataset we have just loaded from Topcat via SAMP
    2.  Select the radio button "Color map"
    3.  Select the *rainbow* color map
    4.  Choose your attriubte. In this case, we will use the number of transits, *ntr*
    5.  Click :guilabel:`Ok`
    6.  Click on the highlight dataset |highlight-s-off| icon to apply the color map

-  You can **define basic filters** on the objects of the dataset using their attributes from the dataset preferences window |prefs-s-icon|. For example, we can filter out all stars with :math:`\delta > 50^{\circ}`:

    1.  Click on the dataset preferences button |prefs-s-icon|
    2.  Click on :guilabel:`Add filter`
    3.  Select your attribute (declination :math:`\delta`)
    4.  Select your comparator (:math:`<`)
    5.  Enter your value, in this case 50
    6.  Click :guilabel:`Ok`
    7.  The stars with a declination greater than 50 degrees should be filtered out

Multiple filters can be combined with the **AND** and **OR** operators

.. |binicon| image:: ../img/ui/bin-icon.png
   :class: filter-invert
.. |eye-s-on| image:: ../img/ui/eye-s-on.png
   :class: filter-invert
.. |prefs-s-icon| image:: ../img/ui/prefs-s-icon.png
   :class: filter-invert
.. |highlight-s-off| image:: ../img/ui/highlight-s-off.png
   :class: filter-invert

External information
--------------------

Gaia Sky offers three ways to display external information on the current focus object: **Wikipedia**, **Gaia archive** and **Simbad**.

.. figure:: img/dpac-2021/external-info.jpg
    :align: center
    :alt: External information
    :target: ../_images/external-info.jpg

    Wikipedia, Gaia archive and Simbad connections

-  When the :guilabel:`+Info` button appears in the focus info pane, it means that there is a Wikipedia article on this object ready to be pulled and displayed in Gaia Sky
-  When the :guilabel:`Archive` button appears in the focus info pane, it means that the full table information of selected star can be pulled from the Gaia archive
-  When the ``Simbad`` link appears in the focus info pane, it means that the objects has been found on Simbad, and you can click the link to open it in your web browser


Scripting
---------

Gaia Sky exposes an API that is accessible through Python (via Py4j) or through HTTP over a network (using the :ref:`REST API HTTP server <rest-server>`). The full documentation on the scripting system can be found in the :ref:`scripting section <scripting>` of the Gaia Sky user manual.

In this tutorial, we focus on the writing of Python scripts that tap into the Gaia Sky API.

- Scripting **API specification**:

    - `API Gaia Sky master (development branch) <https://codeberg.org/gaiasky/gaiasky/src/branch/master/core/src/gaiasky/script/IScriptingInterface.java>`__
    - `API Gaia Sky 3.5.8 <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/3.5.8/gaiasky/script/IScriptingInterface.html>`__

- Interesting **showcase scripts** can be found `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/scripts/showcases>`__.
- Basic **testing scripts** can be found `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/scripts/tests>`__.

This section includes a **hands-on session** where we work on some scripts (`full file listing <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/>`__) and write new ones to later run them on Gaia Sky. The scripts are:

- `Locating_the_Hyades_tidal_tails.py <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/Locating_the_Hyades_tidal_tails.py>`__ --- a simple sequential script which exemplifies some of the most common API calls, and can be used to capture a video. The script requires the following data and subtitles files to run (save them in the same directory as the script):

    - `Aldebaran.vot <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/Aldebaran.vot>`__
    - `Hyades_stars.csv <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/Hyades_stars.csv>`__
    - `Hyades_subtitles.srt <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/Hyades_subtitles.srt>`__
    - `distSDR3_N.csv <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/distSDR3_N.csv>`__

- `line-objects-update.py <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/202402_mwgaiadn/line-objects-update.py>`__ --- a script showcasing the feature to run scripting code within the Gaia Sky main loop, so that it runs every frame. This is used to run update operations every single frame. In our test script, we create a line between the Earth and the Moon, start the time simulation, and update the position of the line every frame so that it stays in sync with the scene.



Camera paths
-------------

Gaia Sky includes a feature to record and play back camera paths. This comes in handy if you want to showcase a certain itinerary through a dataset, for example.

**Recording a camera path** --- The system will capture the camera state at every frame and save it into a ``.gsc`` (for Gaia Sky camera) file. You can start a recording by clicking on the |rec| icon in the camera pane of the control panel. Once the recording mode is active, the icon will turn red |recred|. Click on it again in order to stop recording and save the camera file to disk with an auto-generated file name (default location is ``$GS_DATA/camera`` (see the :ref:`folders <folders>` section in the Gaia Sky documentation).

**Playing a camera path** --- In order to playback a previously recorded ``.gsc`` camera file, click on the |play| icon and select the desired camera path. The recording will start immediately.

.. hint:: **Mind the FPS!** The camera recording system stores the position of the camera for every frame! It is important that recording and playback are done with the same (stable) frame rate. To set the target recording frame rate, edit the "Target FPS" field in the camcorder settings of the preferences window. That will make sure the camera path is using the right frame rate. In order to play back the camera file at the right frame rate, you can edit the "Maximum frame rate" input in the graphics settings of the preferences window.

.. figure:: img/mwgaiadn-2024/202402_camera-paths.jpg
    :alt: Camrecorder
    :align: center
    :target: ../_images/202402_camera-paths.jpg

    Location of the controls of the camcorder in Gaia Sky

.. |rec| image:: ../img/ui/rec-icon-gray.png
   :class: filter-invert
.. |recred| image:: ../img/ui/rec-icon-red.png
.. |play| image:: ../img/ui/play-icon.png
   :class: filter-invert

More information on camera paths in Gaia Sky can be found in :ref:`the camera paths section <camera-paths>` of the Gaia Sky user manual.

Keyframe system
~~~~~~~~~~~~~~~

The camera path system offers an additional way to define camera paths based on keyframes. Essentially, the user defines the position and orientation of the camera at certain times and the system generates the camera path from these definitions. Gaia Sky incorporates a whole keyframe definition system which is outside the scope of this tutorial.

As a very short preview, in order to bring up the keyframes window to start defining a camera path, click on the icon |keyframes|. 

.. |keyframes| image:: ../img/ui/rec-key-icon-gray.png
   :class: filter-invert

More information on the keyframe system can be found in the :ref:`keyframe system subsection <keyframe-system>` of the Gaia Sky user manual.

Frame output mode
-----------------

In order to create high-quality videos, Gaia Sky offers the possibility to export every single still frame to an image file. The resolution of these still frames can be set independently of the current screen resolution.

You can start the frame output system by pressing :guilabel:`F6`. Once active, the frame rate will go down (each frame is being saved to disk). The save location of the still frame images is, by default, ``$GS_DATA/frames/[prefix]_[num].jpg``, where ``[prefix]`` is an arbitrary string that can be defined in the preferences. The save location, mode (simple or advanced), and the resolution can also be defined in the preferences.

.. figure:: img/dpac-2023/2023_frame-output-prefs.jpg
    :alt: Frame output
    :align: center
    :target: ../_images/2023_frame-output-prefs.jpg

    The configuration screen for the frame output system

Once we have the still frame images, we can convert them to a video using ``ffmpeg`` or any other encoding software. Additional information on how to convert the still frames to a video can be found in the :ref:`capturing videos section <capture-videos>` of the Gaia Sky user manual.

Conclusion
----------

Congratulations! You have reached the end of the tutorial. You are now a Gaia Sky master ;)
