.. _dpac-plenary-hdb-2023:

Scripting workshop (DPAC 2023)
==============================

This page has been retired. However, you can still browse the workshop notes in the link below:

*  `Scripting workshop notes (DPAC 2023) <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/3.5.8/workshops/dpac-plenary-hdb-2023.html>`__
