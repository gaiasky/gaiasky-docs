.. _dpac-plenary-online-2021:
   
General tutorial (DPAC 2021)
============================

This page has been retired. However, you can still browse the tutorial notes in the link below:

*  `Tutorial notes (DPAC 2021) <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/3.5.8/workshops/dpac-plenary-online-2021.html>`__
