.. _particle-catalogs:

Particle catalog formats
========================

In order to load simple particles (also referred to as point clouds), Gaia Sky accepts catalogs in common formats like VOTable or CSV (see :ref:`the STIL data loader section <stil-data-provider>`), but also in a tailor-made binary format that is fast and compact. This binary format can load simple particles (of type ``PARTICLE``, only contain a position) and also extended particles (of type ``PARTICLE_EXT``, which contain positions, but also proper motions, colors, magnitudes, etc.). This format is used, for instance, in the most recent versions of the SDSS catalogs.

The class in charge of loading and writing binary particle catalogs is the `BinaryPointDataProvider <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/gaiasky/data/group/BinaryPointDataProvider.html>`__.

The binary format loads much faster than regular VOTable or CSV files, and is described below.

- 1 single-precision integer (32-bit) -- number of particles in the file
- 1 byte (8-bit) -- boolean (1: true, 0: false) indicating whether to use extended particles or not
- For each particle:

  - 1 double-precision integer (64-bit) -- particle identifier
  - 1 single-precision integer (32-bit) -- namelen -> Length of name
  - namelen * char (16-bit * namelen) -- characters of the particle name. Each character is encoded with UTF-16
  - 1 double-precision float (64-bit) -- right ascension [deg]
  - 1 double-precision float (64-bit) -- declination in [deg]
  - 1 double-precision float (64-bit) -- distance [pc]
  - if extended particles:

    - 1 single-precision float (32-bit) -- :math:`\mu_{\alpha}\star` [mas/yr]
    - 1 single-precision float (32-bit) -- :math:`\mu_{\delta}` [mas/yr]
    - 1 single-precision float (32-bit) -- radial velocity [km/s]
    - 1 single-precision float (32-bit) -- apparent magnitude
    - 1 single-precision float (32-bit) -- packed color
    - 1 single-precision float (32-bit) -- particle pseudo-size

The packed color format uses 8 bits per channel in RGBA, and is encoded into a single-precision floating point number using `the libgdx Color class <https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/graphics/Color.java#L360>`__.
