.. _console:

.. |log| image:: img/ui/iconic-map-marker.png
   :class: filter-invert

.. role:: orange

.. raw:: html

   <div class="new">Since 3.6.4</div>

Console
*******

   .. hint:: Use :guilabel:`~` or :guilabel:`:` to open the console.

Since version 3.6.4, Gaia Sky includes a console feature that offers a text interface to directly call methods in the API from within the app.

.. figure:: img/ui/console/console-help.jpg
  :width: 85%                                  
  :align: center
  :target: _images/console-help.jpg

  The console displaying the available API calls.

From the console, you can call any of the :ref:`scripting API methods <scripting>`. Additionally, there are a few shortcuts that directly link to API calls. Get a listing of all the available methods and shortcuts directly in the console with

.. code:: console

   help 

You can list only the API calls with

.. code:: console

   help api

Or only the shortcuts with

.. code:: console

   help shortucts

In order to call a method, input the method name (or shortcut), followed by a white spaces and a list of parameters, also white space-separated. Within a single parameter there may be no spaces, unless it is a string. Strings containing spaces need to be delimited with double-quotes ``"sample string"``, otherwise the parsing will fail.

.. code:: console

   methodName parameter1 parameter2 parameter3

Each parameter may be of the following types:

- ``String`` --- either surrounded by double quotes or not. If the string contains spaces, it must be surrounded by double quotes. For instance, ``"This is a string"`` and ``StringWithoutSpaces`` are valid strings.
- ``double`` --- a double-precision floating point number. May be a simple decimal number (3.24) or in scientific notation (3.24E10).
- ``float`` --- a single-precision floating point number. May be a simple decimal number (3.24) or in scientific notation (3.24E10).
- ``int`` --- a single-precision integer value.
- ``long`` --- a double-precision integer value.
- ``boolean`` --- a boolean value, either ``true`` or ``false``. Do not use capital letters.
- ``type[]`` --- a string of any of the types above, in the format ``[val1,val2,val3,...]``. There may be no spaces between the values.

.. note:: Methods that use non-string object parameters (``Runnable``, ``FocusView``, ``Entity``, etc.) can't be used from the console. These methods are not listed in the ``help`` command and are not available.
