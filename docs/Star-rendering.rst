.. _star-rendering:

Star rendering
**************

This section provides a bird's eye view of the star rendering process implemented in Gaia Sky, with pointers to source files implementing the different aspects of it.

The star rendering process in Gaia Sky consists of two parts. First, we compute a pseudo-size for each star, and then we use all the pseudo-sizes in the star shaders to render the stars.

Pseudo-size determination
-------------------------

We determine the pseudo-size from each star based on the star's apparent magnitude. First, we get the apparent magnitude as seen from the Sun from whatever star catalog. Then, we correct it using extinction data (if available, see :ref:`the magnitude/color corrections section <mag-corrections>`).

The Java code that implements this (``STILDataProvider`` class, used to load external catalogs in CSV or VOTable into Gaia Sky) does not include magnitude/color corrections, the Rust code in the catalog generator program does:

- `Magnitude corrections - load.rs#L799 <https://codeberg.org/gaiasky/gaiasky-catgen/src/branch/master/src/load.rs#L799>`__.

Once we have the corrected apparent magnitude, we convert it to an absolute magnitude with the common formula 

.. math::
  M = m - 5(log_{10} d_{pc} - 1), 

where :math:`M` is the absolute magnitude and :math:`m` is the apparent one. Finally, we do a conversion from absolute magnitude to *pseudo*-size using the luminosity,

.. math::
  L = L_0 * 10^{-0.4*M_o},

where :math:`M_o` is the bolometric magnitude. Obviously, this is not physically accurate, as the bolometric magnitude should include the contributions of the radiation at all wavelengths, but we found it works quite well in practice for rendering stars. Then we apply a constant factor and a square root, but this is tailored to Gaia Sky's rendering and should probably be adapted to your own renderer. The routine we use is here:

- `Magnitude to pseudo-size routine - load.rs#L826 <https://codeberg.org/gaiasky/gaiasky-catgen/src/branch/master/src/load.rs#L826>`__.

Or in Java, look at method ``absoluteMagnitudeToPseudoSize(double)`` of ``STILDataProvider`` `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/core/src/gaiasky/data/group/STILDataProvider.java#L716>`__.

Star shader and rendering
-------------------------

That is only half of the picture though. That gets us the pseudo-size from the apparent magnitude.

On the rendering side, Gaia Sky has the option of rendering stars using billboards (quads implemented as two triangles sharing two vertices) or using native driver points (``GL_POINTS``). The code using ``GL_POINTS`` is faster but has some important drawbacks like points being drawn in screen space, which ignores effects like perspective distortion, so we focus here on the billboard quads.

We pass the pseudo-size :math:`p` into the shader and compute the solid angle :math:`\alpha` from this size and the current distance :math:`d` from the camera to the star:

.. math::
  \alpha = \text{atan}(p / d)

Since we are dealing with distant stars most of the time, we can probably get away with using the `small-angle formula <https://en.wikipedia.org/wiki/Small-angle_approximation>`__ (i.e. omit :math:`\text{atan}`). We found it does not have much of an impact on performance on relatively modern GPUs, and it gives us accurate angles when stars get closer. Then, we use the solid angle, together with the pseudo-size and some additional parameters (like the brightness power, which applies a power function to the solid angle to artificially widen the difference between bright and faint stars) to work out the quad size. This is implemented in various shaders. For example, have a look at:

- `Star shader code - star.group.quad.vertex.glsl#L83 <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/shader/star.group.quad.vertex.glsl#L83>`__

The whole star rendering process involves many parameters (i.e. see the :ref:`visual settings section <visual-settings>`) and is quite complex, but with the topics discussed in this section you should have a solid understanding of what is going on behind the scenes.
