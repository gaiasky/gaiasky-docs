.. _rest-server:

REST API
********


Gaia Sky provides a REST API feature that exposes the scripting API over the network via an HTTP server. This feature is used to :ref:`connect multiple instances <gaiasky-master-slave>` and to enable :ref:`the multi-projector setup in planetariums <planetarium-multi-projector>`.

   .. hint:: The REST API feature may permit remote code execution and open your machine to vulnerabilities. Only use the feature in a trusted environment!

.. contents::
  :backlinks: none

In order to enable the REST API over an HTTP server in Gaia Sky, you need to modify the :ref:`configuration file <configuration-file>`. The default location is ``~/.config/gaiasky/config.yaml`` in Linux and ``[User.Home]\.gaiasky\config.yaml`` in Windows and macOS. In that file, there is a property ``program::net::restPort`` (double colons indicate nesting) with the default value of -1. You can enable the REST server by setting this value to a positive integer number which will be the listening port of the server. For instance, we can start Gaia Sky with the REST server listening to the port 34487 with:

.. code:: yaml
    
    program:
        net:
            restPort: 34487

Then, start Gaia Sky normally. You should see a couple of lines in the logs starting with ``RESTServer`` informing you that the REST API server is ready. Then, open your browser and point it to ``http://localhost:34487/api``. You should get a JSON-formatted page documenting all the API calls available:

.. figure:: img/screenshots/firefox-rest.jpg
   :alt: Help page with REST API calls
   :align: center
   :target: ../_images/firefox-rest.jpg

   The help page showing all REST API calls in Firefox

Using the REST API
==================

The API allows for developing additional software that interfaces with Gaia Sky without the need for language-specific bindings or inter-process communication protocols. Calling methods from the scripting interface ``IScriptingInterface`` is enabled locally and remotely via HTTP.

The syntax of API commands is set to be close to the Java method interface, but does not cover it in all generality to permit simple usage. Particularly note that the REST server receives strings from the client and will try to convert them into correct types.

Commands require HTTP request parameters having the names for the formal parameters of the script interface methods to allow simple construction of HTTP requests based on the scripting interface source documentation. We use Java reflections with access to the formal parameter names. Accordingly, the code needs to be compiled with ``-parameters`` (otherwise parameters are named ``arg0``, ``arg1``, ...).

Both GET and POST requests are accepted. Although GET requests are not supposed to have side effects, we include them for easy usage with a browser.

Issue commands with a syntax like the following:

- ``http://localhost:PORT/api/setCameraUp?up=[1.,0.,0.]``
- ``http://localhost:PORT/api/getScreenWidth``
- ``http://localhost:PORT/api/goToObject?name=Jupiter&angle=32.9&focusWait=2``

Give booleans, integers, floats, doubles, strings as they are, vectors are comma-separated with square brackets around: ``true``, ``42``, ``3.1``, ``3.14877``, ``Super-string``, ``[1,2,3]``, ``[Do,what,they,told,ya]``. Note that you might need to escape or URL-encode characters in a browser for this (e.g. spaces or "=").

Response with return data is in JSON format, containing key/value pairs.
The ``"success"`` pair tells you about success/failure of the call, the ``"value"`` pair gives the return value. Void methods will contain a ``"null"`` return value.  The ``"text"`` pair can give additional information on the call.

The ``"cmd_syntax"`` entry you get from the ``help`` command (e.g. ``http://localhost:PORT/api/help``) gives a summary of permitted commands and their return type. Details on the meaning of the command and its parameters need to be found from the :ref:`scripting API documentation <scripting>`.

Debug
=====

To examine, what happens during an API call, set the default log level of ``SimpleLogger`` to
'info' or lower (in the build file ``core/build.gradle``).

Return values are given as JSON objects that contain key-value pairs:

- ``"success"`` indicates whether the API call was executed successful or not
- ``"text"`` may give additional text information
- ``"value"`` contains the return value or null if there is no return value

For testing with curl, a call like the following allows will deal with URL-encoding. The line below, when issued with a running Gaia Sky instance with the REST API server enabled listening to port 34487, will print the message "Hi, how are you?" in the Gaia Sky window:

.. code:: bash
    
    curl "http://localhost:34487/api/setHeadlineMessage" --data headline='Hi, how are you?'

You can clear it with:

.. code:: bash
    
    curl "http://localhost:34487/api/clearHeadlineMessage"
