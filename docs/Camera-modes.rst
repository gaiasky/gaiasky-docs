Camera settings
***************

The camera settings are accessed via the :ref:`camera pane <camera-pane>`. This section describes the two main settings that affect how the camera behaves: :ref:`camera modes <camera-modes>` and :ref:`camera behaviors <camera-behaviors>`.

.. contents::
  :backlinks: none

.. _camera-modes:

Camera modes
============

Gaia Sky offers four basic camera modes.

.. hint:: The 'Gaia scene' camera mode has been removed in Gaia Sky 3.2.2. The three 'Gaia FOV' modes have been removed after Gaia Sky 3.5.4-1.

Focus mode
----------

This is the default mode. In this mode the camera movement is locked to a focus object, which can be selected by double clicking or by using the find dialog (:guilabel:`Ctrl` + :guilabel:`F`).
There are two extra options available. These can be activated using the checkboxes at the bottom of the `Camera` panel in the GUI Controls window:

*  **Lock camera to object** -- the relative position of the camera with respect to the focus object is maintained. Otherwise, the camera position does not change.
*  **Lock orientation** -- the camera rotates with the object to keep the same perspective of it at all times.

.. raw:: html

   <div class="new new-prev">Since 3.1.7</div>

Object tracking
~~~~~~~~~~~~~~~

Usually, in focus mode, the direction of the camera points to the focus object. It is possible, however, to track a different object while still having our position linked to the focus object. To do so, **right-click** on the object to track and select '*Track object: Object name*' in the **context menu** that pops up. This will cause the camera direction to automatically follow the tracking object at all times. To disable tracking, right-click anywhere and select '*Remove tracking object*'


The description of the controls in focus mode can be found here:

*  :ref:`Keyboard controls in focus mode <keyboard-focus-free-mode>`
*  :ref:`Mouse controls in focus mode <mouse-focus-mode>`
*  :ref:`Gamepad controls <gamepad-actions>`

.. hint:: :guilabel:`Numpad 1` or :guilabel:`1` -- enter focus mode

Free mode
---------

This mode does not lock the camera to a focus object but it lets it roam free in space.

*  :ref:`Keyboard controls in free mode <keyboard-focus-free-mode>`
*  :ref:`Mouse controls in free mode <mouse-free-mode>`
*  :ref:`Gamepad controls <gamepad-actions>`

.. hint:: :guilabel:`Numpad 0` or :guilabel:`0` -- enter free mode

Game mode
---------

This mode maps the standard control system for most games (:guilabel:`w`:guilabel:`a`:guilabel:`s`:guilabel:`d` + Mouse look) in Gaia Sky. Additionally, 
it is possible to add gravity to objects, so that when the camera is closer to a planet than a certain threshold, 
gravity will pull it to the ground.Quit

.. hint:: :guilabel:`Numpad 2` or :guilabel:`2` -- enter game mode


.. _spacecraft-mode:

Spacecraft mode
---------------

In this mode you take control of a spacecraft. In the spacecraft mode, the ``GUI`` changes completely. The Options window disappears and
a new user interface is shown in its place at the bottom left of the screen.

*  **Attitude indicator** -- shown as a ball with the horizon and other marks. It represents the current orientation of the spacecraft with respect to the equatorial system.

  *  |pointer| -- indicate the direction the spacecraft is currently headed to.
  *  |greencross| -- indicate direction of the current velocity vector, if any.
  *  |redcross| -- indicate inverse direction of the current velocity vector, if any.

*  **Engine Power** -- current power of the engine. It is a multiplier in steps of powers of ten. Low engine power levels allow for Solar System or planetary travel, whereas high engine power levels are suitable for galactic and intergalactic exploration. Increase the power clicking on |power-up| and decrease it clicking on |power-down|.
*  |stabilise| -- stabilise the yaw, pitch and roll angles. If rotation is applied during the stabilisation, the stabilisation is canceled.
*  |stop| -- stop the spacecraft until its velocity with respect to the Sun is 0. If thrust is applied during the stopping, the stopping is canceled.
*  |exit| -- return to the focus mode.


.. |redcross| image:: img/sc/ai-antivel.png
   :class: filter-invert
.. |greencross| image:: img/sc/ai-vel.png
   :class: filter-invert
.. |pointer| image:: img/sc/ai-pointer.png
   :class: filter-invert
.. |stabilise| image:: img/sc/icon_stabilise.jpg
   :class: filter-invert
.. |stop| image:: img/sc/icon_stop.jpg
   :class: filter-invert
.. |exit| image:: img/sc/icon_exit.jpg
   :class: filter-invert
.. |power-up| image:: img/sc/sc-engine-power-up.png
   :class: filter-invert
.. |power-down| image:: img/sc/sc-engine-power-down.png
   :class: filter-invert

Additionally, it is possible to adjust three more parameters:

*  **Responsiveness** -- control how fast the spacecraft reacts to the user's yaw/pitch/roll commands. It could be seen as the power of the thrusters.
*  **Drag** -- control the friction force applied to all the forces acting on the spacecraft (engine force, yaw, pitch, and roll). Set it to zero for a real zero G simulation.
*  **Force velocity to heading direction** -- make the spacecraft to always move in the direction it is facing, instead of using the regular momentum-based motion. Even though physically inaccurate, this makes it much easier to control and arguably more fun to play with. 


*  :ref:`Keyboard controls in spacecraft mode <keyboard-spacecraft-mode>`
*  :ref:`Gamepad controls <gamepad-actions>`

.. hint:: :kbd:`NUMPAD_3` -- enter spacecraft mode

.. figure:: img/sc/sc-controls.png
  :alt: Spacecraft mode controls view, with the attitude indicator ball at the center, the control buttons at the bottom and the engine power to the left.
  :width: 50%
  :align: center

  Spacecraft mode controls view, with the attitude indicator ball at the center, the control buttons at the bottom and the engine power to the left.

          
.. _camera-behaviors:

Camera behaviors
================

.. raw:: html

   <div class="new new-prev">Since 1.5.0</div>

Since version ``1.5.0`` a new option is available in the user interface to control the behavior of the camera, the cinematic mode toggle. The cinematic mode is in fact the same exact behavior
the camera has had in Gaia Sky since the first release. If cinematic mode is not enabled, however, the camera adopts a new behavior which is much more responsive.

.. hint:: enable and disable the cinematic camera behavior with :guilabel:`ctrl` + :guilabel:`c`.

.. _cinematic-behaviour:

Cinematic behavior
------------------

This behavior makes the camera use **acceleration and momentum**, leading to **very smooth transitions** and movements. This is the ideal camera to 
use when recording camera paths or when showcasing to an audience.

.. _noncinematic-behaviour:

Non-cinematic behavior
----------------------

In this behavior the camera becomes much **more responsive** to the user's commands and inputs. There is no longer an acceleration factor, and momentum is very minimal. This is the
**default** behavior as of version ``1.5.0`` and probably better meets the expectations of new users.
 
