.. _controls:

.. role:: mouse

Controls
********

This section describes the controls of Gaia Sky.

.. contents::
  :backlinks: none

Keyboard controls
=================

To check the most up-to-date controls go to the ``Controls`` tab in the
preferences window. Here are the default keyboard controls depending on the
current camera mode. Learn more about camera modes in the :ref:`camera-modes` section.

Keyboard mappings
-----------------

The keyboard mappings are stored in an internal file called ``keyboard.mappings`` (`link <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/mappings/keyboard.mappings>`__).
If you want to edit the keyboard mappings, copy the file it into ``$GS_CONFIG/mappings/`` (if it is not yet there) and edit it. This overrides the default internal mappings file.
The file consists of a series of ``<ACTION>=<KEYS>`` entries. For example:

.. code::

    # Help
    action.help                                 = F1
    action.help                                 = H

    # Exit
    action.exit                                 = ESC

    # Home
    action.home                                 = HOME

    # Preferences
    action.preferences                          = P

    #action.playcamera                          = C

The available actions are the following:


- ``action.toggle/element.stars`` -- toggle stars
- ``action.toggle/element.planets`` -- toggle planets
- ``action.toggle/element.moons`` -- toggle moons
- ``action.toggle/element.satellites`` -- toggle satellites
- ``action.toggle/element.orbits`` -- toggle orbits
- ``action.toggle/element.labels`` -- toggle labels
- ``action.toggle/element.equatorial`` -- toggle equatorial grid
- ``action.toggle/element.ecliptic`` -- toggle ecliptic grid
- ``action.toggle/element.galactic`` -- toggle galactic grid
- ``action.toggle/element.clusters`` -- toggle star clusters
- ``action.toggle/element.asteroids`` -- toggle asteroids
- ``action.toggle/element.constellations`` -- toggle constellations
- ``action.toggle/element.boundaries`` -- toggle constellation boundaries
- ``action.toggle/element.meshes`` -- toggle meshes
- ``action.toggle/element.keyframes`` -- toggle keyframes
- ``action.toggle/element.recursivegrid`` -- toggle recursive grid

- ``action.toggle/element.stereomode`` -- toggle stereoscopic mode
- ``action.switchstereoprofile`` -- switch stereoscopic profile
- ``action.toggle/element.planetarium`` -- toggle planetarium mode
- ``action.toggle/element.planetarium.projection`` -- switch planetarium projection
- ``action.toggle/element.360`` -- toggle cubemap mode
- ``action.toggle/element.projection`` -- switch cubemap projection mode
- ``action.toggle/element.orthosphere`` -- toggle orthosphere mode
- ``action.toggle/element.orthosphere.profile`` -- switch orthosphere profile

- ``action.toggle/element.octreeparticlefade`` -- toggle particle smooth transitions in LOD datasets
- ``action.toggle/element.debugmode`` -- enable/disable debug information

- ``action.toggle/element.cleanmode`` -- toggle clean UI mode (remove the user interface)
- ``action.toggle/gui.minimap.title`` -- toggle minimap
- ``action.toggle/gui.mousecapture`` -- toggle mouse capture
- ``action.expandcollapse.pane/gui.time`` -- toggle time pane
- ``action.expandcollapse.pane/gui.camera`` -- toggle camera pane
- ``action.expandcollapse.pane/gui.visibility`` -- toggle visibility pane
- ``action.expandcollapse.pane/gui.lighting`` -- toggle visual settings pane
- ``action.expandcollapse.pane/gui.dataset.title`` -- toggle datasets pane
- ``action.expandcollapse.pane/gui.bookmarks`` -- toggle bookmarks pane
- ``action.screenshot`` -- capture and save screenshot
- ``action.screenshot.cubemap`` -- save 6 current cubemap faces to image files (only in panorama, planetarium and orthosphere modes)

- ``action.pauseresume`` -- start/stop time simulation
- ``action.dividetime`` -- reduce time warp (x0.5)
- ``action.doubletime`` -- increase time warp (x2)
- ``action.time.warp.reset`` -- reset time warp
- ``action.playcamera`` -- open a camera path file

- ``action.decfov`` -- decrease field of view angle
- ``action.incfov`` -- increase field of view angle
- ``action.toggle/camera.mode`` -- switch camera modes
- ``camera.full/camera.FREE_MODE`` -- enable free mode
- ``camera.full/camera.FOCUS_MODE`` -- enable focus mode
- ``camera.full/camera.GAME_MODE`` -- enable game mode
- ``camera.full/camera.SPACECRAFT_MODE`` -- enable spacecraft mode
- ``action.toggle/camera.cinematic`` -- toggle cinematic camera mode
- ``action.camera.speedup`` -- keep pressed to speed the camera up

- ``action.starpointsize.inc`` -- increase star point size
- ``action.starpointsize.dec`` -- decrease star point size
- ``action.starpointsize.reset`` -- reset star point size

- ``action.gotoobject`` -- immediately move to focus object
- ``action.home`` -- go to home object
- ``action.search`` -- open search dialog
- ``action.log`` -- show system log
- ``action.preferences`` -- show preferences dialog
- ``action.help`` -- open help dialog
- ``action.slave.configure`` -- show slave configuration dialog
- ``action.loadcatalog`` -- load a dataset
- ``action.upscale`` -- debug upscale filter
- ``action.keyframe`` -- add new keyframe at the end

- ``action.controller.gui.in`` -- show/hide controller UI
- ``action.toggle/element.controls`` -- expand/collapse UI controls
- ``action.ui.reload`` -- reload user interface
- ``action.resettime`` -- reset simulation time to current
- ``action.toggle/element.frameoutput`` -- toggle frame output
- ``action.exit`` -- quit Gaia Sky
- ``action.togglefs`` -- toggle full screen mode

Find the current keyboard mappings associations in the controls tab of the preferences window within Gaia Sky.

.. figure:: img/ui/prefs/controls.jpg
  :align: center

  The controls settings in Gaia Sky

.. _keyboard-focus-free-mode:

Free/focus mode controls
------------------------

These are the default keyboard controls that apply to the focus, free and game camera modes.

+--------------------------------------------------------------+---------------------------------------------------+
| Key(s)                                                       | Action                                            |
+==============================================================+===================================================+
| :guilabel:`↑`                                                | camera forward                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↓`                                                | camera backward                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`→`                                                | rotate/yaw right                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`←`                                                | rotate/yaw left                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`g`                             | instantly move to focus object                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Home`                                             | back to Earth (or any other home object)          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Tab`                                              | toggle minimap                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`r`                             | reset time to current                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 0` or :guilabel:`0`                           | free camera                                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 1` or :guilabel:`1`                           | focus camera                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 2` or :guilabel:`2`                           | game mode                                         |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Num 3` or :guilabel:`3`                           | spacecraft mode                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`o`                             | load new dataset                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`m`                             | toggle camera mode                                |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`c`                             | toggle cinematic camera behavior                  |
+--------------------------------------------------------------+---------------------------------------------------+
| ``hold down`` :guilabel:`z`                                  | multiply camera speed                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`w`                             | new keyframe                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`k`                             | panorama mode                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Space`                                            | pause/resume time                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F1`                                               | help dialog                                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F5`                                               | take screenshot                                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F6`                                               | start/stop frame output mode                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F7`                                               | save cubemap faces as image files                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`F11`                                              | toggle fullscreen/windowed mode                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`f` or :guilabel:`f`            | search dialog                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Esc` or :guilabel:`q`                             | quit application                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`p`                                                | open preferences dialog                           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`h`                                                | open help dialog                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`r`                                                | run script dialog                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`t`                                                | toggle time pane                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`c`                                                | toggle camera pane                                |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`v`                                                | toggle visibility pane                            |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`l`                                                | toggle visual settings pane                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`d`                                                | toggle datasets pane                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`b`                                                | toggle bookmakrs pane                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Alt` + :guilabel:`c`                              | run camera path file dialog                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`,`                                                | halve time warp (hold for smooth decrease)        |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`.`                                                | double time warp (hold for smooth increase)       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`.`                             | reset time warp to 1                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`b`                            | toggle constellation boundaries                   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`c`                            | toggle constellation lines                        |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`e`                            | toggle ecliptic grid                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`g`                            | toggle galactic grid                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`l`                            | toggle labels                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`m`                            | toggle moons                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`o`                            | toggle orbits                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`p`                            | toggle planets                                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`q`                            | toggle equatorial grid                            |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`s`                            | toggle stars                                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`t`                            | toggle satellites                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`v`                            | toggle star clusters                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`h`                            | toggle meshes                                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`r`                            | toggle recursive grid                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`k`                            | toggle keyframes                                  |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Shift` + :guilabel:`u`                            | expand/collapse control panel                     |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`u`                             | toggle UI completely (hide/show user interface)   |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`d`                             | toggle debug info                                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`s`                             | toggle stereoscopic mode                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`s`         | switch between stereoscopic profiles              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`k`                             | toggle 360 panorama mode                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`k`         | switch between 360 projections                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`p`                             | toggle planetarium mode                           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`p`         | switch planetarium projections                    |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`j`                             | toggle orthosphere mode                           |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`Ctrl` + :guilabel:`Shift` + :guilabel:`j`         | switch between orthosphere profiles               |
+--------------------------------------------------------------+---------------------------------------------------+

.. _keyboard-spacecraft-mode:

Spacecraft mode controls
------------------------

These controls apply only to the spacecraft mode.

+--------------------------------------------------------------+---------------------------------------------------+
| Key(s)                                                       | Action                                            |
+==============================================================+===================================================+
| :guilabel:`w`                                                | apply forward thrust                              |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`s`                                                | apply backward thrust                             |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`a`                                                | roll left                                         |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`d`                                                | roll right                                        |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`k`                                                | stop spaceship automatically                      |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`l`                                                | stabilize spaceship automatically                 |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↑`                                                | pitch up                                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`↓`                                                | pitch down                                        |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`←`                                                | yaw left                                          |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`→`                                                | yaw right                                         |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`PgUp`                                             | increase engine power (x10)                       |
+--------------------------------------------------------------+---------------------------------------------------+
| :guilabel:`PgDown`                                           | decrease engine power (x0.1)                      |
+--------------------------------------------------------------+---------------------------------------------------+

.. _mouse-controls:

Mouse controls
==============

Here are the default mouse controls for the focus and free :ref:`camera-modes`. The other modes do not have mouse controls.

.. _mouse-focus-mode:

Focus mode
----------

+-----------------------------------------------+-----------------------------------------------------------------+
| Mouse + keys                                  | Action                                                          |
+===============================================+=================================================================+
| :mouse:`L-MOUSE DOUBLE-CLICK`                 | select focus object                                             |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE CLICK`                        | stop all rotation and translation movement                      |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE` + `DRAG`                     | apply rotation around focus                                     |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE` + :guilabel:`Shift` + `DRAG` | camera roll                                                     |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`R-MOUSE` + `DRAG`                     | pan view freely from focus                                      |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`M-MOUSE` + `DRAG` or :mouse:`WHEEL`   | move towards/away from focus                                    |
+-----------------------------------------------+-----------------------------------------------------------------+

.. _mouse-free-mode:

Free mode
---------

+-----------------------------------------------+-----------------------------------------------------------------+
| Mouse + keys                                  | Action                                                          |
+===============================================+=================================================================+
| :mouse:`L-MOUSE DOUBLE-CLICK`                 | select object as focus (changes to focus mode)                  |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE CLICK`                        | stop all rotation and translation movement                      |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE` + `DRAG`                     | pan view                                                        |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`L-MOUSE` + :guilabel:`Shift` + `DRAG` | camera roll                                                     |
+-----------------------------------------------+-----------------------------------------------------------------+
| :mouse:`M-MOUSE` + `DRAG` or :mouse:`WHEEL`   | forward/backward movement                                       |
+-----------------------------------------------+-----------------------------------------------------------------+

.. _mouse-game-mode:

Game mode
---------

Use the mouse to look around and :guilabel:`wasd` to move.

Gamepad controls
================

.. raw:: html

   <div class="new new-prev">Since 2.3.0</div>

Gaia Sky supports Game controllers through `SDL <https://www.libsdl.org>`__. This means that most controllers should *just work* out-of-the-box. The default controller mappings file, ``SDL_Controller.controller``, should always be used initially. Should this file not work for your controller, you can create your custom mappings easily and interactively by going to the preferences window > controls and clicking on the "Configure" button next to your controller. Then, follow screen instructions.

.. figure:: img/ui/prefs/controls-gamepad.jpg
  :align: center

  Configuring gamepad controls in Gaia Sky

User mappings files (see `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/mappings/SDL_Controller.controller>`__)
can be added manually to ``$GS_CONFIG/mappings`` (see :ref:`folders <folders>`) folder, or set up automatically from within Gaia Sky. The controller mappings file contains the axis or button numbers for each input type. Below is an example of one such file.

.. code:: 

        #Controller mappings definition file for Wireless Steam Controller
        axis.dpad.h=-1
        axis.dpad.v=1
        axis.lstick.h=0
        axis.lstick.h.sensitivity=1.0
        axis.lstick.v=1
        axis.lstick.v.sensitivity=1.0
        axis.lt=-1
        axis.lt.sensitivity=1.0
        axis.rstick.h=2
        axis.rstick.h.sensitivity=1.0
        axis.rstick.v=3
        axis.rstick.v.sensitivity=1.0
        axis.rt=-1
        axis.rt.sensitivity=-1.0
        axis.value.pow=4.0
        button.a=2
        button.b=3
        button.dpad.d=18
        button.dpad.l=19
        button.dpad.r=20
        button.dpad.u=17
        button.lb=6
        button.lstick=13
        button.lt=-1
        button.rb=7
        button.rstick=-1
        button.rt=-1
        button.select=10
        button.start=11
        button.x=4
        button.y=5



.. _gamepad-actions:

Default camera mappings
-----------------------

.. figure:: img/controller/controller-annotated.png
  :alt: Gampead axes and buttons
  :align: center

  Gamepad annotated with axes and buttons


The following table lists the actions assigned to each of the gamepad axes and buttons.

.. list-table::
   :header-rows: 1
   :widths: 30 70

   * - Button/axis
     - Action
   * - |rs-left|/|rs-right|
     - rotate around horizontally (focus mode), yaw (free mode)
   * - |rs-up|/|rs-down|
     - rotate around vertycally (focus mode), pitch (free mode)
   * - |ls-left|/|ls-right|
     - roll
   * - |ls-up|/|ls-down|
     - forward/backward
   * - |rt| (right trigger)
     - roll right
   * - |lt| (left trigger)
     - roll left
   * - |start|
     - preferences
   * - |a|
     - toggle labels
   * - |b|
     - toggle asteroids
   * - |x|
     - toggle minimap
   * - |y|
     - toggle orbits
   * - |dpad-up|
     - hold to speed up time
   * - |dpad-down|
     - hold to slow down time
   * - |dpad-right|
     - start time
   * - |dpad-left|
     - stop time
   * - |rs| (click)
     - stop time

Spacecraft camera mappings
--------------------------

In spacecraft mode, the actions mapped to the different gamepad axes and buttons are different. They are listed in the table below.

.. list-table::
   :header-rows: 1
   :widths: 30 70

   * - Button/axis
     - Action
   * - |rs-left|/|rs-right|
     - spacecraft yaw
   * - |rs-up|/|rs-down|
     - spacecraft pitch
   * - |ls-left|/|ls-right|
     - spacecraft roll
   * - |ls-up|/|ls-down|
     - thrust forward/backward
   * - |rb| (right bumper)
     - spacecraft roll right
   * - |lb| (left bumper)
     - spacecraft roll left
   * - |rt| (right trigger)
     - thrust forward
   * - |lt| (left trigger)
     - thrust backward
   * - |a|
     - toggle labels
   * - |b|
     - toggle orbits
   * - |x|
     - stop spacecraft
   * - |y|
     - level spacecraft
   * - |dpad-up|
     - increase engine power (x10)
   * - |dpad-down|
     - decrease engine power (x0.1)



.. raw:: html

   <div class="new new-prev">Since 3.0.0</div>

Gamepad UI
----------

The gamepad UI allows access to some basic actions and settings directly using a gamepad. To open it, press |start|.

.. figure:: img/ui/controller-ui.jpg
  :align: center

  The gamepad UI

There are seven tabs at the top that can be navigated with |lb| and |rb|. The tabs are the following:

- **Search** -- provides a virtual keyboard to search for objects.
- **Bookmarks** -- access the system bookmarks (limited to 4 nested folder levels).
- **Camera** -- camera parameters like the mode or the field of view.
- **Time** -- controls to start and stop time, as well as to set the time warp factor.
- **Types** -- visibility of elements in Gaia Sky.
- **Controls** -- gamepad settings and mappings.
- **Graphics** -- graphics options like post-processing effect parameters.
- **System** -- system-wide settings. Also a button to quit Gaia Sky.


Close the gamepad UI with |b| or |start|.


.. raw:: html

   <div class="new">Since 3.3.1</div>

GUI navigation
==============

Gaia Sky supports the navigation of its GUI windows using the gamepad and keyboard mappings, additionally to the usual mouse clicks. Below are the most common actions and how to achieve them in a keyboard- or gamepad- centric workflow.


+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Action                                       | Keyboard                                                            | Gamepad                                  |
+==============================================+=====================================================================+==========================================+
| Action (click focused button)                | :guilabel:`Enter`                                                   | |a|                                      |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move focus up                                | :guilabel:`↑`                                                       | |dpad-up|/|ls-up|                        |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move focus down                              | :guilabel:`↓`                                                       | |dpad-down|/|ls-down|                    |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move focus right                             | :guilabel:`→`                                                       | |dpad-right|/|ls-right|                  |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move focus left                              | :guilabel:`←`                                                       | |dpad-left|/|ls-left|                    |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move slider (when focused)                   | :guilabel:`←`/:guilabel:`→`/:guilabel:`home`/:guilabel:`end`        | |rs-left|/|rs-right|                     |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Move select box selection (when focused)     | :guilabel:`←`/:guilabel:`→`/:guilabel:`home`/:guilabel:`end`        | |rs-left|/|rs-right|                     |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Check check box (when focused)               | :guilabel:`Enter`                                                   | |a|                                      |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Cycle dialog bottom buttons                  | :guilabel:`Alt`                                                     | |select|                                 |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Close current dialog (with accept action)    | /                                                                   | |start|                                  |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Close current dialog (with cancel action)    | :guilabel:`Esc`                                                     | /                                        |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Tab right                                    | :guilabel:`Tab`                                                     | |rb|                                     |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+
| Tab left                                     | :guilabel:`Shift` + :guilabel:`Tab`                                 | |lb|                                     |
+----------------------------------------------+---------------------------------------------------------------------+------------------------------------------+


.. |a| image:: img/gamepad/a.png
   :width: 30
.. |b| image:: img/gamepad/b.png
   :width: 30
.. |x| image:: img/gamepad/x.png
   :width: 30
.. |y| image:: img/gamepad/y.png
   :width: 30

.. |start| image:: img/gamepad/start.png
   :width: 30
.. |select| image:: img/gamepad/select.png
   :width: 30

.. |lb| image:: img/gamepad/lb.png
   :width: 30
.. |rb| image:: img/gamepad/rb.png
   :width: 30
.. |lt| image:: img/gamepad/lt.png
   :width: 30
.. |rt| image:: img/gamepad/rt.png
   :width: 30

.. |dpad| image:: img/gamepad/dpad.png
   :width: 30
.. |dpad-up| image:: img/gamepad/dpad-up.png
   :width: 30
.. |dpad-down| image:: img/gamepad/dpad-down.png
   :width: 30
.. |dpad-right| image:: img/gamepad/dpad-right.png
   :width: 30
.. |dpad-left| image:: img/gamepad/dpad-left.png
   :width: 30

.. |ls| image:: img/gamepad/ls.png
   :width: 30
.. |ls-up| image:: img/gamepad/ls-up.png
   :width: 30
.. |ls-down| image:: img/gamepad/ls-down.png
   :width: 30
.. |ls-right| image:: img/gamepad/ls-right.png
   :width: 30
.. |ls-left| image:: img/gamepad/ls-left.png
   :width: 30

.. |rs| image:: img/gamepad/rs.png
   :width: 30
.. |rs-up| image:: img/gamepad/rs-up.png
   :width: 30
.. |rs-down| image:: img/gamepad/rs-down.png
   :width: 30
.. |rs-right| image:: img/gamepad/rs-right.png
   :width: 30
.. |rs-left| image:: img/gamepad/rs-left.png
   :width: 30
