.. _cubemaps:

.. raw:: html

   <div class="new new-prev">Since 3.2.0</div>

Cubemaps
********

Gaia Sky supports `cubemaps <https://learnopengl.com/Advanced-OpenGL/Cubemaps>`__, in addition to regular equirectangular (spherically projected) images, to texture planets, moons and other spherical or semi-spherical objects.

The use of cubemaps instead of plain textures helps eliminate the artifacts happening at the poles with UV sphere models. Other possible solutions are using icospheres or octahedronsphers, but in these seams may appear due to the uneven texture coordinates. The image below illustrates this iusse.

.. figure:: img/cubemaps/tex-vs-cubemap-s.jpg
  :alt: Texture versus cubemap
  :align: center

  Detail of the north pole region of the Earth, using a regular texture (left) and using a cubemap (right). Note the artifacts on the left image.

Cubemaps are supported for the diffuse, specular, normal, emissive, metallic, roughness and height channels. All these can be applied to regular models. Additionally, the diffuse cubemap can be applied to the cloud layer. The keys are the following:

- ``"diffuseCubemap"``
- ``"specularCubemap"``
- ``"normalCubemap"``
- ``"emissiveCubemap"``
- ``"metallicCubemap"``
- ``"roughnessCubemap"``
- ``"heightCubemap"``

Cubemaps are composed of six individual textures for the up, down, right, left, front and back directions. They can be easily generated from equirectangular images by using this `python converter <https://codeberg.org/langurmonkey/py360convert>`__. To generate a cubemap from an equirectangular texture, clone the repository and use the `equitocubemap` script:

.. code:: bash

    equitocubemap IMAGE CUBEMAP_SIDE_RES OUTPUT_LOCATION


The six cubemap image will be saved in ``OUTPUT_LOCATION`` with the prefixes ``_ft.jpg``, ``_bk.jpg``, ``_up.jpg``, ``_dn.jpg``, ``_rt.jpg`` and ``_lf.jpg``. PNG is also supported.

In Gaia Sky, you need to point the ``"diffuseCubemap"`` property to the location of these six cubemap sides. Gaia Sky will take the appropriate image for each cubemap side using the file name suffixes. The following suffixes are recognized by Gaia Sky:

.. csv-table:: File name suffixes for each cubemap side.
   :header: "Side", "Suffixes"
   :widths: 20, 25

    "back", "``bk``, ``back``, ``b``"
    "front", "``ft``, ``front``, ``f``"
    "up",    "``up``, ``top``, ``u``, ``t``"
    "down",  "``dn``, ``bottom``, ``d``"
    "right", "``rt``, ``right``, ``r``"
    "left", "``lf``, ``left``, ``l``"

