.. _proxy-configuration:

.. raw:: html

   <div class="new">Since 3.2.0</div>

Proxy configuration
*******************

If you need to configure Gaia Sky to use an HTTP, HTTPS, FTP or SOCKS proxy, you need to set it up at the Java virtual machine (JVM) level. The official documentation can be found `here <https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/net/doc-files/net-properties.html>`__.

To configure a proxy, we need to pass some arguments to the JVM. Even though you can directly configure the proxy using JVM arguments, Gaia Sky offers an easier way to set this up using the :ref:`configuration file <configuration-file>`. Using the configuration file has the advantage that it works the same way across all operating systems and packages.

.. note:: If your proxy requires authentication, please use the direct configuration below. Otherwise Java just ignores the ``[protocol].proxy[User|Password]`` properties, and the direct method ensures the authentication tokens are set up correctly.

Use system proxy
----------------

The easiest way is to instruct Gaia Sky to use the proxy configured at the operating system level. To do so, open your ``config.yaml`` file (if you don't know where to find it, see :ref:`this <configuration-file>`) you need to set the ``proxy::useSystemProxies`` property to ``true`` (``::`` indicates nesting) in your configuration file:

.. code:: yaml

    proxy:
        useSystemProxies: true

If not set, this setting defaults to ``false``.

Direct configuration
--------------------

Here you can enter the parameters of your proxy directly. The properties to set depend on the protocol.

HTTP
~~~~

You can set the host, the port, the user credentials and the list of hosts that can bypass the proxy:

.. code:: yaml

    proxy:
        http:
            host: a.b.c.d
            port: 8080
            username: myname
            password: secret
            nonProxyHosts: a.b.c.d|e.f.g.*|localhost

- **host** -- the hostname, or address, of the proxy server.
- **port** -- the port number of the proxy server. Defaults to 80.
- **username** -- the username, if you need authentication.
- **password** -- the password, if you need authentication.
- **nonProxyHosts** -- the hosts that should be accessed without going through the proxy. The value of this property is a list of hosts, separated by the '|' character. In addition, the wildcard character '*' can be used for pattern matching.



HTTPS
~~~~~

You can set the host, the protocol, the user credentials and the list of hosts that can bypass the proxy:

.. code:: yaml

    proxy:
        https:
            host: a.b.c.d
            port: 8080
            username: myname
            password: secret
            nonProxyHosts: a.b.c.d|e.f.g.*|localhost

- **host** -- the hostname, or address, of the proxy server.
- **port** -- the port number of the proxy server. Defaults to 80.
- **username** -- the username, if you need authentication.
- **password** -- the password, if you need authentication.
- **nonProxyHosts** -- the hosts that should be accessed without going through the proxy. The value of this property is a list of hosts, separated by the '|' character. In addition, the wildcard character '*' can be used for pattern matching.

SOCKS
~~~~~

You can set the host, the port, the username, the password and the SOCKS version:

.. code:: yaml

    proxy:
        socks:
            host: a.b.c.d
            port: 8080
            version: 5
            username: myname
            password: secret

- **host** -- the hostname, or address, of the proxy server.
- **port** -- the port number of the proxy server. Defaults to 80.
- **version** -- the SOCKS protocol version. Defaults to 5, but can also be set to 4.
- **username** -- the username, if you need authentication.
- **password** -- the password, if you need authentication.

FTP
~~~

You can set the host, the protocol, the user credentials and the list of hosts that can bypass the proxy:

.. code:: yaml

    proxy:
        ftp:
            host: a.b.c.d
            port: 8080
            username: myname
            password: secret
            nonProxyHosts: a.b.c.d|e.f.g.*|localhost

- **host** -- the hostname, or address, of the proxy server.
- **port** -- the port number of the proxy server. Defaults to 80.
- **username** -- the username, if you need authentication.
- **password** -- the password, if you need authentication.
- **nonProxyHosts** -- the hosts that should be accessed without going through the proxy. The value of this property is a list of hosts, separated by the '|' character. In addition, the wildcard character '*' can be used for pattern matching.
