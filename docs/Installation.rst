.. _installation:

Installation and running
************************

In the sections below is the information on the minimum hardware requirements and on how to install the software.

.. contents::
  :backlinks: none

System requirements
===================

Here are the minimum requirements to run this software:

+------------------------+----------------------------------------------------------------------------------+
| **Operating system**   | Linux / Windows 10+ / macOS                                                      |
+========================+==================================================================================+
| **Archictecture**      | x86_64, ARM (only Apple silicon with compat. mode)                               |
+------------------------+----------------------------------------------------------------------------------+
| **CPU**                | Intel Core i5 3rd Gen. 4+ cores recommended                                      |
+------------------------+----------------------------------------------------------------------------------+
| **GPU**                | Support for OpenGL 3.3 (4.2 recommended), 1 GB VRAM                              |
+------------------------+----------------------------------------------------------------------------------+
| **Memory**             | 4+ GB RAM (depends on loaded datasets)                                           |
+------------------------+----------------------------------------------------------------------------------+
| **Hard drive**         | 1+ GB of free disk space (depends on downloaded datasets)                        |
+------------------------+----------------------------------------------------------------------------------+

Download
========

Gaia Sky packages are available for **Linux**, **macOS** and **Windows**. You can either download the `Gaia Sky build <https://gaiasky.space/downloads>`__ for your operating system (recommended) or browse and `build the source code <https://codeberg.org/gaiasky/gaiasky>`_.

*  `Gaia Sky downloads page <https://gaiasky.space/downloads>`__

Installation procedure
======================

Depending on your system and your personal preferences the installation procedure may vary. This section describes the installation and running process for the different operating systems and packages.

  
Linux
-----

We provide 4 distro-agnostic packages:

- :ref:`Flatpak <flatpak>`.
- :ref:`AppImage <appimage>`.
- :ref:`Unix installer <unix-installer>`.
- :ref:`TAR.GZ package <tar-gz>`.

We also offer 3 distro-specific packages: 

- :ref:`DEB <deb>` -- **Debian** and derivatives.
- :ref:`RPM <rpm>` -- **RedHat** and derivatives.
- :ref:`AUR <aur>` -- **Arch Linux** and derivatives.

.. _flatpak:

Flatpak
^^^^^^^

Install the `Flatpak package <https://flathub.org/apps/space.gaiasky.GaiaSky>`__ with the following:

.. code:: bash

   flatpak install flathub space.gaiasky.GaiaSky

Then, run with:

.. code:: bash

   flatpak run space.gaiasky.GaiaSky

.. _appimage:

AppImage
^^^^^^^^

The `AppImage <https://appimage.org>`__ does not need installation. `Download the package <https://gaiasky.space/downloads>`__, give it execute permissions if necessary, and run it.

.. code:: bash

   wget https://gaia.ari.uni-heidelberg.de/gaiasky/releases/latest/gaiasky_$VERSION_x86_64.appimage
   chmod +x gaiasky_$VERSION_x86_64.appimage
   ./gaiasky_$VERSION_x86_64.appimage

.. _unix-installer:

Unix installer
^^^^^^^^^^^^^^

`Download the package <https://gaiasky.space/downloads>`__, give it execute permissions and run it to start the installation process. Then follow the on-screen instructions:

.. code:: bash

   chmod +x gaiasky_linux_$VERSION.sh
   ./gaiasky_linux_$VERSION.sh

Once installed, you can simply run the ``gaiasky`` command, or use your favourite launcher to find and run it.

.. _deb:

DEB package
^^^^^^^^^^^

This is the package for Debian-based distros (**Debian**, **Ubuntu**, **Mint**, etc.). `Download <https://gaiasky.space/downloads>`__ the ``gaiasky_$VERSION.deb`` file and run the following command. You need root privileges to install a DEB package in your system.

.. code:: bash

    dpkg -i gaiasky_$VERSION.deb

This installs the application in the ``/opt/gaiasky/`` folder and creates the necessary shortcuts and ``.desktop`` files.

Once installed, you can simply run the ``gaiasky`` command, or use your favourite launcher to find and run it.

In order to **uninstall**, just type:

.. code:: bash

    apt remove gaiasky

.. _rpm:

RPM package
^^^^^^^^^^^

This is the package for RPM-based distributions (**Red Hat**, **Fedora**, **Mandriva**, **SUSE**, **CentOS**, etc.) `Download <https://gaiasky.space/downloads>`__ the ``gaiasky_linux_$VERSION.rpm`` file and run the following command. You need root privileges to install an RPM package in your system.

.. code:: bash

    rpm --install gaiasky_linux_$VERSION.rpm

This installs the application in the ``/opt/gaiasky/`` folder and creates the necessary shortcuts.

Once installed, you can simply run the ``gaiasky`` command, or use your favourite launcher to find and run it.

In order to **uninstall**, just type:

.. code:: bash

    yum remove gaiasky-x86

.. _aur:

AUR package
^^^^^^^^^^^

We also offer an Arch User Repository (AUR) package for **Arch Linux** and derivatives. Install one of `gaiasky <https://aur.archlinux.org/packages/gaiasky/>`__,  `gaiasky-git <https://aur.archlinux.org/packages/gaiasky-git/>`__ or  `gaiasky-appimage <https://aur.archlinux.org/packages/gaiasky-appimage/>`__. For example, if you use `paru <https://github.com/Morganamilo/paru>`__:

.. code:: bash

    paru -S gaiasky

Once installed, you can simply run the ``gaiasky`` command, or use your favourite launcher to find and run it.


Windows
-------

We offer a Windows installer for 64-bit systems, ``gaiasky_windows-x64_$VERSION.exe``.

To install the Gaia Sky, just double click on the installer and then follow the on-screen instructions. You need to choose the directory where the application is to be installed.

In windows, this means clicking on ``Start`` and then browsing the start menu folder ``Gaia Sky``. You can run the executable(s) for Gaia Sky and Gaia Sky VR from there. You can also navigate to the installation folder and run the ``gaiasky.cmd`` file from a command prompt or PowerShell.

In order to **uninstall** the application you can use the Windows Control Panel or you can use the provided uninstaller in the Gaia Sky folder.


macOS
-----

For macOS we provide a ``gaiasky_macos_$VERSION.dmg`` file. To install, double-click on it to mount it and then drag-and-drop the **Gaia Sky.app** application to your ``/Applications`` directory in Finder. Once copied, it is safe to unmount the ``dmg`` volume.

To run it, double click on the **Gaia Sky.app** launcher in your applications directory.

Our ``dmg`` package is not signed by Apple, so it will be detected as coming from an 'unidentified developer'. You can still install it by following the procedure described `in this page <https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac>`__.


.. _tar-gz:

TAR.GZ
------

`Download the package <https://gaiasky.space/downloads>`__, and extract it wherever. Then, use either the ``gaiasky`` or ``gaiasky.cmd`` script to start the program. On a Unix system, do:

.. code:: bash

   tar -xzvf gaiasky-$VERSION.tar.gz -C target/directory/
   cd target/directory/gaiasky-$VERSION
   ./gaiasky


.. _running-from-source:

Run from source
===============

Requirements
------------

If you want to compile the source code, you need the following:

-  Java Development Kit (JDK, version 17 or above should suffice, we recommend using the latest LTS available).
-  `Git <https://git-scm.com>`__.

Please, be aware that only tags are guaranteed to work (`here <https://codeberg.org/gaiasky/gaiasky/tags>`__). The master branch holds the development version and the configuration files are possibly messed up and not ready to work out-of-the-box. So remember to use a tag version if you want to run it right away from source.

First, clone the repository:

.. code:: bash

  git clone https://codeberg.org/gaiasky/gaiasky.git

Getting the catalog data
------------------------

.. hint:: As of version ``2.1.0``, Gaia Sky provides a self-contained download manager to get all the data packs available.  

The `Base data pack` (key: ``default-data``) is necessary for Gaia Sky to run, and contains the Solar System, the Milky Way model, etc. Catalog files are optional but recommended if you want to see any stars at all. You can bring up the download manager at any time by clicking on the button :guilabel:`Dataset manager` in the data tab of the preferences window. More information on the download manager can be found in :ref:`download-manager`.

You can also download the data packs manually `here <https://gaia.ari.uni-heidelberg.de/gaiasky/repository>`_.

Compiling and running
---------------------

To compile the code and run Gaia Sky run the following.

.. code:: console

  ./gradlew core:run
    
If you want to pass CLI arguments via gradle, just use the gradle :samp:`--args` argument (:command:`gradlew core:run --args='-vr'`).

.. tip:: Gaia Sky checks that your Java version is compatible with it when you run the build. Skip this check by setting the ``GS_JAVA_VERSION_CHECK`` environment variable to `false` in the context of gradle:

.. code:: bash
    
  export GS_JAVA_VERSION_CHECK=false


In order to pull the latest changes from the remote git repository:

.. code:: bash

	git pull

On Windows, you need to open the Command Prompt or PowerShell and run:

.. code:: bash

   .\gradlew.bat core:run


CLI arguments
=============

Gaia Sky offers a few command line arguments. Run :command:`gaiasky -h` for more information.

.. code:: bash

  gaiasky -h

  Usage: gaiasky [options]
  Options:
    -h, --help
      Show program options and usage information.
    -v, --version
      List Gaia Sky version and relevant information.
      Default: false
    -i, --asciiart
      Add nice ascii art to --version information.
      Default: false
    -s, --skip-welcome
      Skip the welcome screen if possible (base-data package must be present).
      Default: false
    -p, --properties
      Specify the location of the properties file.
    -a, --assets
      Specify the location of the assets folder. If not present, the default 
      assets location (in the installation folder) is used.
    -vr, --openvr
      Launch in Virtual Reality mode. Gaia Sky will attempt to create a VR 
      context through OpenVR.
      Default: false
    -e, --externalview
      Create a window with a view of the scene and no UI.
      Default: false
    -n, --noscript
      Do not start the scripting server. Useful to run more than one Gaia Sky 
      instance at once in the same machine.
      Default: false
    -d, --debug
      Launch in debug mode. Prints out debug information from Gaia Sky to the 
      logs. 
      Default: false
    -g, --gpudebug
      Activate OpenGL debug mode. Prints out debug information from OpenGL to 
      the standard output.
      Default: false
    -l, --headless
      Use headless (windowless) mode, for servers.
      Default: false
    --safemode
      Activate safe graphics mode. This forces the creation of an OpenGL 3.2 
      context, and disables float buffers and tessellation.
      Default: false
    --nosafemode
      Force deactivation of safe graphics mode. Warning: this bypasses 
      internal checks and may break things! Useful to get rid of safe graphics 
      mode in the settings.
      Default: false
    --hdpimode
      The HDPI mode to use. Defines how HiDPI monitors are handled. Operating 
      systems may have a per-monitor HiDPI scale setting. The operating system 
      may report window width/height and mouse coordinates in a logical 
      coordinate system at a lower resolution than the actual physical 
      resolution. This setting allows you to specify whether you want to work 
      in logical or raw pixel units.
      Default: Pixels
      Possible Values: [Logical, Pixels]


Packaging the software
======================

Gaia Sky can be exported to be run as a standalone app. Right now, doing so is only supported from Linux. You need the utility :command:`help2man` in your path to generate the man pages. Remember to restart the gradle daemon after installing it. Then run:

.. code:: bash

	gradlew core:dist
	
This creates a new directory ``releases/gaiasky-$VERSION`` with the exported application. Run scripts are provided with the name :command:`gaiasky` (Linux, macOS) and :command:`gaiasky.cmd` (Windows).

Also, to export Gaia Sky into a ``tar.gz`` archive file, run the following:

.. code:: bash

    gradlew core:createTar

In order to produce the desktop installers for the various systems you
need a licensed version of Install4j. Additionally, you need a certificate for signing the Windows packages in ``$GS/assets/cert/cert.pfx``. Then, just run:

.. code:: bash

    gradlew core:pack -PwinKeystorePassword=$PASSWORD

Where $PASSWORD is the password of the certificate. This command produces the different OS packages (EXE, DMG, DEB, RPM, etc.) of Gaia Sky and stores them in the ``releases/packages-$VERSION`` directory.


