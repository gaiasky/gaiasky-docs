.. _reference-system:
.. _internal-reference-system:

Internal reference system
*************************

The internal cartesian reference system is a right-handed equatorial system with the particularity that the axes labels are unorthodox. Usually, :math:`X` points to the fundamental direction (:math:`\alpha=0, \delta=0`), :math:`Z` points "up" and :math:`XY` is the fundamental plane (:math:`\delta=0`), with :math:`Y = Z \times X`.

In our case, it is :math:`Z` which points to the fundamental direction (:math:`\alpha=0, \delta=0`), :math:`Y` points up and :math:`XZ` is the fundamental plane (:math:`\delta=0`), with :math:`X = Y \times Z`. In order to convert from common equatorial cartesian coordinates :math:`(XYZ)` to Gaia Sky coordinates :math:`(X'Y'Z')`, you just need to swap the axes:

- :math:`X' = Y`
- :math:`Y' = Z`
- :math:`Z' = X`

Or, what is the same, :math:`(X'Y'Z') = (YZX)`, and :math:`(XYZ) = (Z'X'Y')`. 

Description
-----------

So, in Gaia Sky :math:`XZ` is the equatorial plane (:math:`\delta=0`). :math:`Z` points towards the 
vernal equinox point (:math:`\alpha=0, \delta=0`). :math:`Y` 
points towards the north celestial pole (:math:`\delta=+90^\circ`). :math:`X` 
is perpendicular to both :math:`Z` and :math:`Y` and points to :math:`\alpha=+90^\circ` so that :math:`X=Y \times Z`.

.. figure:: img/refsys.png
   :alt: Gaia Sky reference system
   :align: center
   :class: filter-invert

   Gaia Sky reference system

All the positions and orientations of the entities in the scene are at
some point converted to this reference system for representation. The
same happens with the orientation sensor data in mobile devices.


.. _internal-units:

Internal units
--------------

Internally, the objects in Gaia Sky are positioned using Internal Units.
The default Internal Units (:math:`iu`) are defined as follows:

- :math:`1 iu = 1 * 10^{9} m`

When running in Virtual Reality mode, and only for the duration of the session, the Internal Units are scaled as follows:

- :math:`1 iu = 1 * 10^{5} m`


