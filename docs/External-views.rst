.. _external-views:
.. _external-view:

.. raw:: html

   <div class="new new-prev">Since 2.2.4</div>

External view
*************

Gaia Sky offers a mode to create an additional window with an **external view** of 
the current scene and no user interface controls. This may be useful when presenting
or in order to project the view to an external screen or dome. We call the original window, which contains the UI and controls, **main view**, and the new window with only the scene, **external view**.

In order to create the view, just use the ``-e`` or ``--externalview`` flags when launching
Gaia Sky.

.. code:: console

    $  gaiasky -e


The external view contains a copy of the same frame buffer rendered in the main view. The
scene is not re-rendered (for now), so increasing the size of the external window
won't increase its base resolution. The original aspect ratio is maintained in the
external view to avoid stretching the image, but a 'fill' policy is used in the rendering.


.. hint:: Enable the external view at launch with the flag ``-e`` or ``--externalview``.


.. figure:: img/external-view.jpg
  :alt: Gaia Sky external view
  :align: center
  :target: _images/external-view.jpg

  The external view in Gaia Sky. This configuration has two displays: a 1080p monitor (top-left) and a 4K monitor (bottom-right). The main view is in the 1080p screen, while the external view is in the 4K monitor. The rendering uses a back-buffer scale of 3 to account for the much larger external view window compared to the main view one.


Handling resolution
===================

As we said, the external view only displays **a copy of the frame buffer rendered in the main view**. This means that the resolution of the external view is the same as that of the main view, **regardless of the window size**. When rendering to a projector or dome, this is not ideal. In such a case, it is advised to use the **back-buffer scale** setting (see :ref:`back-buffer scale <back-buffer-scale>` for more info) to multiply the resolution of the rendering buffer. This scales up the frame buffer used for rendering by a given factor.

If you have the *main view* in a fullHD monitor, and need to render to a projector with a 4K resolution, you need to set the back-buffer scale setting to 2. The back-buffer scale setting applies to the width and height of the frame buffer, not the pixel count, so a setting of 2 applied to 1920x1080 (roughly 2MP) results in a 3840x2160 frame buffer (roughly 8MP). So the pixel count is not lineal, but follows a square law (perfectly, in the case of aspect ratio 1:1).

Additionally, the size of the external view window can be manually edited in the :ref:`external view settings <external-view-settings>`.

