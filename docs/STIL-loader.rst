.. _stil-data-provider:

STIL data loader
================

Gaia Sky supports the loading of catalog data in **VOTable**, **FITS**, **CSV** and **ASCII**. using the `STIL library <http://www.star.bristol.ac.uk/~mbt/stil/>`__. To ensure the catalogs are loaded correctly, some preparation might be needed in the form of UCDs and/or column names and units. The following sections describe the expected UCDs and column names for the different data types and units.

.. contents::
  :backlinks: none

The class in charge of loading data using STIL is the `STILDataProvider <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/gaiasky/data/group/STILDataProvider.html>`__.

.. note:: In all cases, UCDs take precedence over column names. That is, if a UCD is present for a given column, the column name is ignored. This means that if the UCD is incorrect, the column data won't be recognized and used even if the column name is correct.


Object IDs
~~~~~~~~~~

Columns with the UCD ``meta.id`` are recognized as generic identifiers. Otherwise, the actual matching is done by column name. The following are recognized:

- ``id`` -- generic ID
- ``hip`` -- HIP number
- ``source_id`` -- Gaia source ID

Object names
~~~~~~~~~~~~

Names are taken from the columns ``name``, ``proper``, ``proper_name``, ``common_name`` and ``designation``. 

.. raw:: html

   <div class="new new-prev">Since 3.0.2</div>

Also, the regular expression ``"(name|NAME|refname|REFNAME)((_|-)[\\w\\d]+)?"`` is matched against column names to find names. This matches anything which starts with  ``name`` or ``NAME`` or ``refname`` plus an optional suffix starting with a hyphen or an underscore.

The loader supports multiple names in a single value. The connecting character used is ``|``, so that if multiple names are to be loaded, they must be in a column with one of the above names and the format ``name-1|name-2|...|name-n``.

Positions
~~~~~~~~~

For the **positional data**, Gaia Sky will look for spherical and cartesian coordinates. In the case of spherical coordinates, the following are UCDs supported: 

-  Equatorial: ``pos.eq.ra``, ``pos.eq.dec``
-  Galactic: ``pos.galactic.lon``, ``pos.galactic.lat``
-  Ecliptic: ``pos.ecliptic.lon``, ``pos.ecliptic.lat``

The units should be specified as column metadata. If units are not there, Gaia Sky will use degrees for coordinate angles (ra, dec, lat, lon, etc.), mas for parallaxes and parsecs for distances.

If UCDs are not possible (i.e. CSV format), the sky positions should be given in the equatorial system and have the following column names and units:

- Right ascension: ``ra``, ``right_ascension``, ``alpha`` in degrees
- Declination: ``dec``, ``de``, ``declination``, ``delta`` in degrees

To work out the distance, it looks for the UCDs ``pos.parallax`` and ``pos.distance``. If either of those are found, they are used. If no UCDs are to be found, the column names ``plx``, ``parallax``, ``pllx`` and ``par`` are accepted. If there are no parallaxes, the default parallax of 0.04 mas is used. As previously mentioned, parallaxes are in mas by default, and distances are in parsecs, unless stated otherwise in column unit metadata. 

With respect to cartesian coordinates, it recognizes the UCDs ``pos.cartesian.x|y|z``, and they are interpreted in the equatorial system by default.

Proper motions and radial velocities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Proper motions** are supported using only the UCDs ``pm.eq.ra`` and ``pm.eq.dec``. Otherwise, the following
column names are checked, assuming the units to be in ``mas/yr``.

-  RA: ``pmra``, ``pmalpha``, ``pm_ra``
-  DEC: ``pmdec``, ``pmdelta``, ``pm_dec``, ``pm_de``

**Radial velocities** are supported through the UCD ``dopplerVeloc`` and through the column
names ``radvel`` and ``radial_velocity``.

Magnitudes
~~~~~~~~~~

**Magnitudes** are supported using the ``phot.mag`` or ``phot.mag;stat.mean`` UCDs. Otherwise, they are
discovered using the column names ``mag``, ``bmag``, ``gmag``, ``phot_g_mean_mag``. If no magnitudes are found, the default value of 15 is used.

Apparent magnitudes are converted to absolute magnitudes with:

.. math::

    M = m - 5 log_{10}(d_{pc}) + 5

where :math:`M` is the absolute magnitude and :math:`m` is the apparent magnitude. :math:`d_{pc}` is the distance to the star in parsecs.

The absolute magnitude is then converted to a pseudo-size with an algorithm that converts first to a luminosity, and then adjusts the size with an experimental calibration.
    


Colors
~~~~~~

**Colors** are discovered using the ``phot.color`` UCD. If not present, the column names ``b_v``, ``v_i``,
``bp_rp``, ``bp_g`` and ``g_rp`` are used, if present. If no color is discovered at all, the default value of 0.656 is used as the color index.

The conversion from color index to RGB is done by converting the XP (BP-RP) color index to :math:`T_{eff}`, and then the :math:`T_{eff}` to RGB, using the ``xp_to_teff()`` and ``teff_to_rgb()`` `methods implemented here <https://codeberg.org/gaiasky/gaiasky-catgen/src/branch/master/src/color.rs#L13>`__.

Variability
~~~~~~~~~~~

Variable stars are loaded if light curves (magnitude vs time) and periods are found in the column list. The magnitude list, time list and period are looked up using their column names:

- **Magnitude list**: A list of [mag] is expected under ``g_transit_mag``, ``g_mag_list``, ``g_mag_series``
- **Time list**: A list of Julian dates (offset from J2010, i.e. :math:`t=JD-2455197.5`) under ``g_transit_time``, ``time_list``, ``time_series``
- **Period**: A period in Julian days under ``pf``, ``period``

Only variable stars with a period will be loaded. The rest will be skipped.


Other columns
~~~~~~~~~~~~~

All the columns which do not fit in the aforementioned categories are loaded as extra attributes. These attributes can be used for filtering and color mapping the dataset.

Right now, additional physical quantities (mass, flux, effective temperature (:math:`T_{eff}`), radius, etc.) fall into the 'other columns' category and are also loaded as extra attributes.
