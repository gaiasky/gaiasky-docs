Frames and screenshots
**********************

Gaia Sky includes some utilities to save frames and screenshots to disk.

.. _frame-output:

Frame ouptut
============

.. hint:: Enable and disable the frame ouptut system with :guilabel:`F6`.

Gaia Sky has an in-built method to save every frame to an image file. The purpose of this is to produce high quality videos from the still frames. Of course, you can also produce videos by capturing the window with OBS or any other screen recorder.

To configure the frame output system (mode, image format, quality, etc.), check out the :ref:`frame output configuration <frame-output-config>` section.

Once enabled with :guilabel:`F6`, Gaia Sky starts saving every frame to an image file in the ``$GS_DATA/frames`` (see :ref:`folders <folders>`) directory. The system saves every frame until :guilabel:`F6` is hit again.


Frame ouptut modes
------------------

There are two frame output modes:

-  **Simple mode** -- save the current screen buffer directly to a file. This means that everything that's on the Gaia Sky window will be in the saved image, including the user interface elements.
-  **Advanced mode** - render the current scene to an off-screen buffer with an arbitrary resolution. The resolution can be configured in the preferences window, :guilabel:`Frame output` tab. The advanced mode does **NOT** render user interface elements or any additional objects that are not part of the scene.

.. _screenshots:

Screenshots
===========

Gaia Sky has an in-built screenshot capturing feature. To take a screenshot press :guilabel:`F5` any time during the execution of the program. By default, screenshots are saved in the ``$GS_DATA/screenshots`` (see :ref:`folders <folders>`) folder. The screenshots are in the format defined in the :ref:`screenshot settings <screenshots-configuration>`.

.. hint:: Take a screenshot with :guilabel:`F5`.

Screenshot modes
----------------

The same two modes available to the frame output system are also available to screenshots.

-  **Simple mode** -- save the current screen buffer directly to a file. This means that everything that's on the Gaia Sky window will be in the saved image, including the user interface elements.
-  **Advanced mode** - render the current scene to an off-screen buffer with an arbitrary resolution. The resolution can be configured in the preferences window, :guilabel:`Screenshots` tab. The advanced mode does **NOT** render user interface elements or any additional objects that are not part of the scene.
