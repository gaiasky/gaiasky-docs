.. _eclipses:

Eclipse representation
**********************

Gaia Sky can represent eclipse events between bodies. You can enable and configure eclipses in the preferences window (see the :ref:`scene settings page <scene-settings>`). 

By default, we provide a series of bookmarks to Solar eclipse events (see the :ref:`bookmarks documentation <bookmarks>`) in the 'Eclipses' folder in the bookmarks pane. Try clicking on any of those, and you should be immediately transported to the time and place of an eclipse.

If highlighting is enabled the penumbra region is highlighted with a yellow line, and the umbra region is highlighted with a red line.


.. figure:: img/screenshots/eclipse-earth-1999.jpg
  :align: center

  The Eclipse of August 11, 1999 in Gaia Sky.
