.. _orthosphere-mode:

Orthosphere view mode
*********************

.. hint:: Use the button |orthosphere-icon| in the camera pane, or :guilabel:`ctrl` + :guilabel:`j` to enter and exit the orthosphere view mode.

.. raw:: html

   <div class="new new-prev">Since 3.3.0</div>


The orthosphere view mode blends the two hemispheres of the orthographic projection in :ref:`panorama mode <panorama-mode>` on top of each other to simulate a full celestial sphere. There are two profiles:

- **Orthosphere** -- the base mode, in which both hemispheres are blended on top of each other. Optionally, you can fill up the sphere with a material with a certain :ref:`refraction index <refraction-index>`. To do so, open the preferences dialog, go to the Graphics configuration tab and scroll down to the experimental section. You will find a "Refraction index" slider to modify it.
- **Orthosphere cross-eye** -- a stereoscopic (3D) cross-eye mode which lays the images for each eye side-by-side.


.. hint:: :guilabel:`ctrl` + :guilabel:`shift` + :guilabel:`j` -- Cycle between the different profiles.

.. hint:: :guilabel:`F7` -- Save the faces of the current cubemap to image files in the screenshots directory.

.. |orthosphere-icon| image:: img/ui/orthosphere-icon.png
   :class: filter-invert
