.. _gaiasky-vr:

Gaia Sky VR
===========

.. note:: Gaia Sky VR is **beta** software. It works reasonably well, but you may encounter hiccups here and there..

.. raw:: html

   <div class="new">Since 3.5.0</div>

`Gaia Sky VR <https://gaiasky.space>`__ is the VR
version of Gaia Sky. It runs on multiple headsets and operating systems
using the `OpenXR API <https://www.khronos.org/OpenXR/>`__.

.. contents::
  :backlinks: none

Our tests have been carried out with the
**Oculus Rift CV1** headset on Windows and the **Valve Index** on Windows
and Linux. We also successfully tested it with the **HP Reverb G2**. 
Due to the system-agnostic nature of OpenXR, other VR HMD systems
and controllers supporting OpenXR should also work fine.

.. note:: Gaia level-of-detail star catalogs don't work very well in VR and may cause performance issues.
   We recommend using static star catalogs like DR3-tiny, DR3-weeny, Hipparcos or GCNS5.

Currently, the regular installation of Gaia Sky also includes the VR version.

System requirements
-------------------

The minimum system requirements for running Gaia Sky VR are roughly the following:

+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **VR System**          | `OpenXR <https://www.khronos.org/OpenXR>`__-compatible VR system (HMD, VR controllers, trackers)                                |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **Operating system**   | Windows 10+ / Linux                                                                                                             |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **CPU**                | Intel Core i5 4rd Generation or similar (4+ core)                                                                               |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **GPU**                | VR-capable GPU (GTX 970+ strongly recommended)                                                                                  |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **Memory**             | 8+ GB RAM                                                                                                                       |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+
| **Hard drive**         | 1 GB of free disk space (depends on datasets)                                                                                   |
+------------------------+---------------------------------------------------------------------------------------------------------------------------------+

Set-up
------

Essentially, you need an OpenXR runtime installed system-wide.

1. **Install runtime** --- Follow the provided vendor instructions and install the software PC application for your VR headset. This application provides the OpenXR runtime. This is the Oculus/Meta PC app for Meta headsets, or SteamVR for the HTC Vive/Pro and the Valve Index, for example.

2. **Set active OpenXR runtime** --- Set the runtime as the **active OpenXR runtime**. This typically is in the settings dialog of the vendor software. This step enables your particular OpenXR runtime to be discoverable by OpenXR-enabled applications like Gaia Sky VR.

3. **Run Gaia Sky VR** --- Launch Gaia Sky VR and it should connect to your active OpenXR runtime automatically. Refer to the following sub-sections to learn how to launch Gaia Sky VR for your system.


Windows
~~~~~~~
The easiest way to get it running in Windows is to install the latest 
version of Gaia Sky and directly run the executable :command:`gaiaskyvr.exe` file. You should also have
a start menu entry called 'Gaia Sky VR', if you chose to create it during
the installation.

Linux
~~~~~
Download and install Gaia Sky, and then run:

.. code:: console
    
    $  gaiasky -vr


Downloading datasets
--------------------

See the :ref:`download-manager` section.

Controls
--------
OpenXR defines a `system-agnostic input scheme <https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#input>`__ where the application defines actions which can be bound
to different input device hardware. We offer a set of comprehensive bindings
for the Oculus Rift CV1, the HTC Vive, the Valve Index and some others. However,
if your headset is not supported you can bind the actions to your controller input
yourself in your runtime. Please consult the documentation of your OpenXR runtime
to learn how to do so.

The default controls are the following:

.. figure:: img/controller/controller-info.png
  :align: center

  The default controls for Gaia Sky VR with the Oculus Touch controllers

Caveats
-------

Gaia Sky VR has been tested with a very small sample of VR systems. Only the Oculus Rift CV1 and the Valve Index are currently well tested. Please, do not expect everything to work flawlessly with other systems and/or headsets.

Common problems
---------------

-  Make sure your runtime is set as the active OpenXR runtime.
-  If you experience low frame rates try using a small and static star
   catalog like DR3-weeny or Hipparcos instead of a Gaia DR3 LOD one.
-  If you are using an Nvidia Optimus-powered laptop, make sure that the
   ``java.exe`` you are using to run Gaia Sky VR is `set up properly in
   the Nvidia Control
   Panel <https://www.pcgamer.com/nvidia-control-panel-a-beginners-guide/>`__
   to use the discrete GPU.

