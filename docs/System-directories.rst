.. _folders:

System Directories
******************

In this documentation we refer to a few different directories that Gaia Sky uses to store data and configuration settings:  ``$GS_DATA``,  ``$GS_CONFIG``, and ``$GS_CACHE``.

- ``$GS_DATA`` --- contains some essential files and directories for Gaia Sky to run properly. For example:

    - ``$GS_DATA/camera`` --- storage point for :ref:`camera path <camera-paths>` and :ref:`keyframe <keyframe-system>` files.
    - ``$GS_DATA/frames`` --- default save location for the :ref:`frame output mode <capture-videos>`.
    - ``$GS_DATA/screenshots`` --- default save location for :ref:`screenshots <screenshots>`.
    - ``$GS_DATA/crashreports`` --- whenever Gaia Sky crashes, a crash report is stored at this location.
    - ``$GS_DATA/log`` --- contains the full Gaia Sky log of the last session. Only the last session's log is kept.
    - ``$GS_DATA/data`` --- also referred to as simply ``$data``, this is the default dataset save location. All datasets are stored in this location by default (can be changed from the dataset manager).

- ``$GS_CONFIG`` --- contains the :ref:`configuration files <configuration-file>`, the :ref:`bookmarks <bookmarks>`, and the :ref:`keyboard mappings file <controls>`.
- ``$GS_CACHE`` --- contains cached files, like Wikipedia images. 
   


The locations of ``$GS_DATA``, ``$GS_CONFIG`` and ``$GS_CACHE`` depend on the operating system:

- **Linux** --- as of Gaia Sky 2.2.0, the Linux release of Gaia Sky uses the `XDG base directory specification <https://specifications.freedesktop.org/basedir-spec/latest/>`__.

    -  ``$GS_DATA`` = ``~/.local/share/gaiasky/``
    -  ``$GS_CONFIG`` = ``~/.config/gaiasky/``
    -  ``$GS_CACHE`` = ``~/.cache/gaiasky/``


- **Windows and macOS** --- the ``.gaiasky`` directory in the user home directory for both locations, so:

    -  ``$GS_DATA`` = ``$GS_CONFIG`` = ``[User.Home]/.gaiasky/``

      - ``[User.Home]`` on Windows is typically in ``C:\Users\[username]``.
      - ``[User.Home]`` on macOS is typically in ``/Users/[username]``.

    -  ``$GS_CONFIG`` = ``$GS_DATA``
    -  ``$GS_CACHE`` = ``$GS_DATA/cache``

Datasets location
=================

By default, Gaia Sky stores the downloaded datasets in the ``$GS_DATA/data`` directory. The location where the datasets are saved is referred to as ``$data``. The actual location of ``$data`` is stored in the configuration file (key ``data::location``) and can be changed in the dataset manager window at startup. 

- ``$data`` = ``$GS_DATA/data``


.. _logs:

Logs and crash reports
======================

For every Gaia Sky session a system log is stored in the directory ``$GS_DATA/log``. Logs are overwritten with each new session, so only the last log is effectively available at any given time.

- ``$GS_DATA/log/gaiasky_log_lastsession.txt`` --- full log of the last Gaia Sky session.

Crash reports are stored in ``$GS_DATA/crashreports`` whenever Gaia Sky crashes. If that happens, please, create a new issue in https://codeberg.org/gaiasky/gaiasky/issues, and attach the crash report. Additionally, also attach the session log.

- ``$GS_DATA/crashreports/gaiasky_crash_[$DATE].txt`` --- crash reports.
