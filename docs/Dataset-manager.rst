.. _download-manager:
.. _dataset-manager:

Dataset manager
===============

When you start Gaia Sky, you are met with the welcome screen, which contains some information and buttons to start Gaia Sky, launch the dataset manager, open the preferences window, open the help window, and exit Gaia Sky.

The **dataset manager** provides an integrated way of downloading and enabling/disabling datasets. Enabled datasets are loaded when Gaia Sky starts up. All downloads are performed over a secure, encrypted HTTPS connection, and data consistency is checked once the download has finished with `sha256` checksums.

.. contents::
  :backlinks: none
  
.. raw:: html

   <div class="new">Since 3.2.0</div>

Welcome screen
--------------

.. figure:: img/welcome/welcome-screen.jpg
  :alt: The welcome screen in Gaia Sky
  :align: center
  :target: _images/2023_welcome.jpg

  The welcome screen in Gaia Sky.

Gaia Sky greets the user with a welcome screen which lets her start Gaia Sky, manage the datasets or exit.

.. raw:: html

   <div class="new">Since 3.2.0</div>

Dataset manager
---------------

The dataset manager provides a hassle-free way of downloading, updating and enabling/disabling your datasets.

Data location
~~~~~~~~~~~~~

All datasets are installed in the data location. Check out :ref:`the directories section <folders>` for the defaults. The install location can be changed by clicking on the button next to :guilabel:`Data location`, at the bottom of the window. When changing the data location no data files are actually moved. If you want to migrate your data files to a different location, you must first do so by hand, and then point Gaia Sky to the new directory.

Available datasets
~~~~~~~~~~~~~~~~~~

.. figure:: img/welcome/dataset-manager.jpg
  :alt: The dataset manager of Gaia Sky
  :align: center
  :target: _images/dataset-manager.jpg

  The available datasets view in the dataset manager.

At the top of the window you can choose to view the datasets available for download, using the :guilabel:`Available for download` tab, and the installed datasets, using the :guilabel:`Installed` tab.

Each of these two views consists of a two-pane layout. The **left pane** displays a list of the installed or available datasets with some very basic information for each. Once the user clicks on one of these datasets, the **right pane** displays extensive information about the dataset and its files.

The :guilabel:`Available for download` tab lists all server datasets that can be downloaded and installed locally. In order to display a dataset in Gaia Sky, it must first be installed locally. To install a dataset, use the install button |arrow-bottom| or right-click on the dataset entry in the left pane and select :guilabel:`Install` in the context menu.

Multiple datasets can be downloading at the same time without problems. The download process can be canceled at any time by clicking on the :guilabel:`Cancel download` button in the **right pane**. Canceled downloads can be resumed any time without losing progress, as the ``.part`` files are kept in the file system.

.. |arrow-bottom| image:: img/ui/iconic-arrow-circle-bottom.png
   :class: filter-invert

Installed datasets
~~~~~~~~~~~~~~~~~~

.. |binicon| image:: img/ui/bin-icon.png
   :class: filter-invert

.. figure:: img/welcome/catalog-selection.jpg
  :alt: The dataset selection window of Gaia Sky
  :align: center
  :target: _images/catalog-selection.jpg

  The installed datasets view in the dataset manager.

The installed datasets view displays the datasets found in the currently selected data directory. From this view, you can **enable** and **disable** datasets by either using the checkbox next to the dataset name, or by right-clicking and selecting :guilabel:`Enable` or :guilabel:`Disable` in the context menu. Only datasets that are enabled are loaded into Gaia Sky.

Some datasets are always enabled. This is the case for all texture packs (whose usage depends on the graphics quality setting) and for the base data pack.

From this view you can also remove datasets. To do so, bring up the context menu by right-clicking on the dataset entry in the datasets list (left pane) and select |binicon| :guilabel:`Remove`. Removing a dataset actually deletes all of its files on disk, so a confirmation dialog is displayed.
