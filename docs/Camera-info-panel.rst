.. _camera-info-panel:
.. _focus-panel:

.. role:: green

Camera info panel
*****************

The **camera info panel**, also known as **focus panel**, is anchored to the bottom-right of the main window. 

.. contents::
  :backlinks: none

.. figure:: img/ui/focuspanel/focuspanel.jpg
  :alt: The focus panel in Gaia Sky
  :align: center

  The camera info panel in Gaia Sky


Whenever the camera is in :ref:`focus mode <camera-modes>`, information about the current focus is displayed here. Additionally, the current location of the mouse pointer and the speed and coordinates of the camera in the :ref:`internal reference system <reference-system>` are also shown at the bottom.

The camera info panel contains three blocks when the camera is in focus mode. When it is in free mode, only the two bottom blocks are available:

- :green:`Focus` information, at the top. Only shown when the camera is in focus mode.
- :green:`Mouse pointer` information, in the middle.
- :green:`Camera` information, to the bottom.

Focus pane
----------

The top line of the focus pane contains the name and type of the current focus object (in your theme accent color, green in the screenshot above) plus some icons:

- |eye| toggle the visibility of this object on and off.
- |tag| toggle the 'always show label' flag for this object, so that its label is always shown regardless of the object's solid angle.
- |star| add this object to the :ref:`bookmarks <bookmarks>`.
- |go-to| moves the camera to the object with a smooth transition.
- |land-on| land on the object.
- |land-at| open the window to choose a location to land on, and execute the landing.
- |procedural| only for planets and small bodies, opens the procedural generation window.


.. |eye| image:: img/focuspanel/eye-icon.png
   :class: filter-invert
.. |tag| image:: img/ui/iconic-tag.png
   :class: filter-invert
.. |star| image:: img/focuspanel/iconic-star.png
   :class: filter-invert
.. |go-to| image:: img/focuspanel/go-to.png
   :class: filter-invert
.. |land-at| image:: img/focuspanel/land-at.png
   :class: filter-invert
.. |land-on| image:: img/focuspanel/land-on.png
   :class: filter-invert
.. |procedural| image:: img/ui/iconic-fork.png
   :class: filter-invert

The information items contained in the focus pane are updated in real time, and are the following:

- :green:`Object type`.
- **ID** -- object ID.
- **Names** -- object names, as a list.
- :math:`\alpha` -- right ascension (:math:`\alpha`), in degrees.
- :math:`\delta` -- declination (:math:`\delta`), in degrees.
- :math:`\mu_{\alpha\star}` -- proper motion in alpha (:math:`\mu_{\alpha\star}`), in mas/s.
- :math:`\mu_{\delta}` -- proper motion in delta (:math:`\mu_{\delta}`), in mas/s.
- **Rad vel** -- radial velocity, in km/s.
- **App mag (E)** -- apparent magnitude as seen from Earth.
- **App mag (C)** -- apparent magnitude as seen from the current camera position.
- **Abs mag** -- absolute magnitude.
- **Angle** -- current solid angle. For stars, this involves a lot of guess-work. See the :ref:`star rendering section <star-rendering>` for more information.
- **Dist/sol** -- distance from the focus object to the Sun.
- **Dist/cam** -- distance from the focus object to the camera.
- **Radius** -- radius of the object, in km.
- :guilabel:`+ Info` button -- lists all the local data on the object, and offers a preview of the Wikipedia article for this object, if it exists. If the object belongs to a VOTable catalog and has additional columns, those are displayed here as well.
- :guilabel:`Archive` button -- provides the archive data for the given star. Only works for Gaia and Hipparcos stars.
- `Simbad <https://simbad.u-strasbg.fr>`__ link, opens in a browser with the object information in the Simbad database.

.. figure:: img/ui/camera-info-pane-focusmode.jpg
  :width: 35%                                  
  :align: center
  :target: ../_images/camera-info-pane-focusmode.jpg

  The camera info panel when the camera is in focus mode. In this state, it is also referred to as focus info pane, and it displays information on the focus (top), the mouse pointer (middle), and the camera position and state (bottom).


Mouse pointer
-------------

This section contains the current location of the mouse pointer in the equatorial reference system, as sky-projected coordinates. Additionally, when the pointer is over a planet or moon, we already get the longitude and latitude values.

- :math:`\alpha` **/** :math:`\delta` (pointer) -- the current location of the mouse pointer in the equatorial reference system (sky coordinates).
- **Lat\Lon** -- the latitude and longitude of the mouse pointer on the surface of a planet or moon. Only updated when the mouse pointer is on a planet or moon.
- :math:`\alpha` **/** :math:`\delta` (view) -- the current location of the center of the view in the equatorial reference system (sky coordinates).

Camera
------

This section contains the current speed of the camera in Km/h, plus the distance from the camera to the Sun and the current location of the camera in the :ref:`internal reference system <reference-system>` (equatorial cartesian coordinates).

- **Tracking** -- the name of the object the camera is currently tracking, if any.
- **Velocity** -- current camera velocity.
- **Dist/Sol** -- distance from the camera to the Sun.

.. figure:: img/ui/camera-info-pane-freemode.jpg
  :width: 35%                                  
  :align: center
  :target: ../_images/camera-info-pane-freemode.jpg

  The camera info panel when the camera is in free mode only provides information on the status, velocity and location of the camera, as well as the mouse pointer.

