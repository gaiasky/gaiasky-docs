.. _locationlog:

.. |log| image:: img/ui/iconic-map-marker.png
   :class: filter-invert

.. role:: orange

.. raw:: html

   <div class="new new-prev">Since 3.1.0</div>

Location log
************

Gaia Sky provides a small location log feature that keeps track of the visited locations and objects during a session. Currently, the location log is limited to 200 entries. Old entries are deleted as new ones come in.

.. figure:: img/ui/pane-location-log.jpg
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-location-log.jpg

  The location log pane keeps track of the objects you have visited.

The **location log pane** can be found anchored to the right in the main window. Expand and collapse it by clicking on the map marker |log| button. 

Every entry in the location log displays the **time since the visit** to the object (in :orange:`orange`, hover over it to get the absolute time), and has two actions available:


- |go-to| re-visits the location with the same camera and time setup as when it was first added: this sets the camera position, direction and up vectors to match exactly the ones at the time of the visit, and set the simulation time as well
- |land-on| instantly go to the object of this location

.. |go-to| image:: img/focuspanel/go-to.png
   :class: filter-invert
.. |land-on| image:: img/focuspanel/land-on.png
   :class: filter-invert

