.. _visibility:

Object visiblity
****************

Gaia Sky offers two different ways to control object visiblity: :ref:`type visibility <type-visibility>` and :ref:`per-object visibility <individual-visibility>`. The main difference is that, while type visibility acts on all objects of a given component type (think stars, labels, grids, planets, etc.), per-object visibility is capable of hiding and showing individual objects.

You can toggle **object types** on and off by clicking on their icon in the ``Type visibility`` pane in the control panel, as described :ref:`here <type-visibility>`.

You can also hide and show **individual objects** by using the eye icon in the focus panel (when in focus mode) or by using the dedicated window, as described in the :ref:`individual visibility section <individual-visibility>`.


