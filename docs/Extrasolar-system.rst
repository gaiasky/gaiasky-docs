.. _extrasolar-system:

Defining an extrasolar system
*****************************

In this little example we will define a made-up extrasolar system with two stars orbiting the common barycenter and four planets doing the same. We call the stars *Exonia A* and *Exonia B*, and the planets *Exonia c*, *d*, *e* and *f*. Additionally, we'll give *Exonia c* a small moon called *Exonia c1*. The system we'll create in this short tutorial can be downloaded directly from the download manager in Gaia Sky (with Gaia Sky 3.2.0+) or manually `here <https://gaia.ari.uni-heidelberg.de/gaiasky/repository/systems/exonia/>`__.

.. contents::
  :backlinks: none

Initial set up
==============

First, we need to create the JSON file where we define the objects. Go to the ``$data`` directory (see :ref:`folders`) and create the directory and file ``system-exonia/dataset.json``:


.. code:: bash

   cd $data
   mkdir system-exonia && cd system-exonia
   echo "{'objects':[]}" > dataset.json


We have created a file with no objects. Since we have added the ``dataset.json`` file, Gaia Sky will recognize the dataset (now it is still empty, but we'll get to that), and you'll be able to select it in the dataset manager at startup.

The dataset descriptor file contains some metadata about the new catalog. Let's have a look:

.. code:: json

    {
        "key" : "system-exonia",
        "name" : "Exonia extrasolar system",
        "type" : "system",
        "version" : 2,
        "description" : "A made-up, partially procedurally
                        generated extra-solar system with
                        two stars, three planets and a moon.",
        "size" : 2600000,
        "nobjects" : 7,
        "data" : [
            {
                "loader": "gaiasky.data.JsonLoader",
                "files": [ "$data/system-exonia/system-exonia.json" ]
            }
        ]
    }

As we can see, it has the catalog name, its version, its type, the description, the size, the number of objects and a pointer to the actual data file. All JSON files in Gaia Sky are loaded with the ``JsonLoader`` class, so that part is more or less constant.

Defining the objects
====================

Now we are ready to start adding our objects. First, since all the objects orbit the barycenter of the system, we need an object to represent its position. This object must not be visible, but it must allow us to represent a position in space, and it must be the parent of all other objects in the system. We can do that with an object of the archetype ``Invisible``. 

.. code:: json

    {
        "objects" : [
            {
                "names" : ["Exonia Center"],
                "componentType" : ["Others"],
                "size" : 6.0e4,

                "parent" : "Universe",
                "archetype" : "Invisible",
                
                "coordinates" : {
                    "impl" : "gaiasky.util.coord.StaticCoordinates",
                    "positionPc" : [20.0, 90.0, 10.0]
                }
            }
        ]
    }

This object has the name *Exonia Center*. We will use this name in the ``"parent"`` attribute when we crate the rest of the objects.
Since the barycenter of this system will not move, we use ``StaticCoordinates``. Here he have used the property ``"positionPc"``, so we need to enter the position in parsecs in the :ref:`internal reference system <reference-system>`. We could have used ``"positionKm"`` to use kilometers, or simply ``"position"`` to use :ref:`internal units <internal-units>`.

The internal reference system is based on the equatorial system, so we need equatorial Carteisan coordinates. However, we can also use ecliptic or galactic cartesian coordinates by specifying a transformation:

.. code:: json

    {                
        "coordinates" : {
           "impl" : "gaiasky.util.coord.StaticCoordinates",
           "positionPc" : [20.0, 90.0, 10.0],
           "transformName" : "galacticToEquatorial"
        }
    }

Here, the properties ``"transformName"`` and ``"transformFunction"`` can be used interchangeably to specify the transformation.

Finally, we also accept spherical equatorial, galactic and ecliptic coordinates. They are set using ``"positionEquatorial"`` (alpha, delta, distance), ``"positionEcliptic"`` (l, b, distance) and ``"positionGalactic"`` (gal_lon, gal_lat, distance). The angles are in degrees and the distances are in parsecs. If you choose to use spherical coordinates, please do not specify a transformation!

Stars
-----

Catalog stars in Gaia Sky are not single objects, so we can't just use them. If we need a star to do fancy things (e.g., move around in an orbit, have children exoplanets, etc.), we need to define a model object using the archetype ``Star``. In our case, for each star we need an *orbit* object (as we want the star to move in an elliptical orbit) and a *star* object. The orbit is in charge of constructing the trajectory data and rendering it, while the star is the model object representing the star itself. We have two stars in the system, *Exonia A* and *Exonia B*. More information on the orbit format can be found :ref:`here <orbit-format>`. Let's see how we define them and then we go over the attributes:

.. code:: json

    {
        "name" : "Exonia A orbit",
        "color" : [1.0, 0.0, 1.0, 0.8],
        "componentType": [ "Orbits", "Stars" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod": true,	
        "orbit" 		: {
            "period" : 130.7655287755297, 
            "epoch" : 2455198.0,
            "semimajoraxis" : 77112085.246326,
            "eccentricity" : 0.28862,
            "inclination" : 7.134,
            "ascendingnode" : 3.91,
            "argofpericenter" : 0.84,
            "meananomaly" : 307.80
        }
    },
    {
        "names" : ["Exonia A"],
        "color" : [1.0, 0.9213, 0.8818, 1.0],
        "colorbv" : 0.656,
        "componentType" : "Stars",

        "absmag" : 2.85,
        "appmag" : 8.73,

        "parent" : "Exonia Center", 
        "archetype" : "Star",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia A orbit"
        }
    },
    {
        "name" : "Exonia B orbit",
        "color" : [0.0, 1.0, 1.0, 0.8],
        "componentType": [ "Orbits", "Stars" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod" : true,
        "orbit" 		: {
            "period" : 120.7655287755297,
            "epoch" : 2455198.0,
            "semimajoraxis" : 71112085.246326,
            "eccentricity" : 0.4,
            "inclination" : 0.93415027853740,
            "ascendingnode" : 130.74959784132,
            "argofpericenter" : 140.31984971,
            "meananomaly" : 0.40983227102735
        }
    },
    {
        "names" : ["Exonia B"],
        "color" : [0.9, 0.8213, 0.7818, 1.0],
        "colorbv" : 0.656,
        "componentType" : "Stars",

        "absmag" : 4.85,
        "appmag" : 10.73,

        "parent" : "Exonia Center", 
        "archetype" : "Star",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia B orbit"
        }
    }


Note that we have two orbits and two stars. Orbits are objects of archetype ``Orbit``. Each orbit is essentially a vessel for the `orbital elements <https://en.wikipedia.org/wiki/Orbital_elements>`__ (period, semi-major axis, eccentricity, etc.). More information on the format and units can be found :ref:`here <orbit-format>`. Since our :ref:`reference-system` is an equatorial system, we add the ``"galacticToEquatorial"`` transformation so that our reference plane is actually the galactic plane instead of Earth's equatorial plane.

Single stars are objects of archetype ``Star``. Note that the parent of all objects is the invisible object *Exonia Center*. Additionally, the coordinates of the stars are provided by the respective orbit objects in the "coordinates" key. The rest is adding the star parameters like color, magnitudes, etc.

Colors can be specified in RGB or using the B-V color index. If both are specified, RGB takes precedence. In case Gaia Sky only finds the B-V color index, it translates it to RGB using `this procedure <http://stackoverflow.com/questions/21977786/star-b-v-color-index-to-apparent-rgb-color>`__.

Star absolute magnitudes and sizes, ``"absmag"`` and ``"size"``, are intertwined. If only the absolute magnitude is specified, it is converted to a radius. The conversion is callibrated with the Sun, so that an absolute magnitude of ~4.85 produces roughly the radius of the Sun, ~3.5e4 km. Otherwise, the size can be specified directly. Since single stars and star catalogs use different render paths, the relatve brightness of single stars with respect to stars in catalogs may not be entirely accurate.

Planets
-------

Now, let's have a look at the planets. They are very similar to the stars in that they also need orbit objects. They differ in the object type and attributes tough. *Exonia e* uses JPG images as textures, so at this point you should get the `Exonia <https://gaia.ari.uni-heidelberg.de/gaiasky/repository/systems/exonia/>`__ data pack, which contains these textures. The rest of the objects use :ref:`procedurally generated <procedural-gen>` data.

.. raw:: html

   <div class="new new-prev">Since 3.2.0</div>

Here is the definition of the planets:

.. code:: json

    {
        "name" : "Exonia c orbit",
        "color" : [0.3, 0.2, 0.9, 0.7],
        "componentType": [ "Orbits", "Planets" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod": true,
        "orbit"	: {
            "period" : 1325.85,
            "epoch" : 2455400.5,
            "semimajoraxis" : 353350171.0,
            "eccentricity" : 0.08862,
            "inclination" : 7.134,
            "ascendingnode" : 103.91,
            "argofpericenter" : 149.84,
            "meananomaly" : 307.80
        }
    },
    {
        "name" : "Exonia c",
        "color" : [0.5, 0.6, 1.0, 1.0],
        "size" : 2410.3,
        "componentType" : "Planets",

        "absmag" : 0.5,

        "parent" : "Exonia Center", 
        "archetype" : "Planet",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia c orbit"
        },

        "rotation" 		: {
            "period" : 400.536,
            "axialtilt" : 0.0,
            "inclination" : 0.281,
            "meridianangle" : 200.39
        },
        "model"			: {
            "args" : [true],
            "type" : "sphere",
            "params" : {
                "quality" : 400,
                "diameter" : 1.0,
                "flip" : false
            },
            "material" : {
                "height" : "generate",
                "diffuse" : "generate",
                "normal" : "generate",
                "specular" : "generate",
                "biomelut" : "data/tex/base/biome-smooth-lut.png",
                "biomehueshift" : 80.0,
                "noise" : {
                    "seed" : 5229243,
                    "scale" : 0.2,
                    "type" : "simplex",
                    "fractaltype" : "ridgemulti",
                    "frequency" : 4.34,
                    "octaves" : 10,
                    "range" : [-1.8, 1.0],
                    "power" : 4.0
                },
                "heightScale" : 3.0
            }
        },
        "cloud"         : {
            "size" : 2430.0,
            "cloud" : "generate",
            "noise" : {
                "seed" : 1234,
                "scale" : [1.0, 1.0, 0.4],
                "type" : "simplex",
                "fractaltype" : "ridgemulti",
                "frequency" : 4.34,
                "octaves" : 6,
                "range" : [-1.5, 0.4],
                "power" : 10.0
            },

            "params" : {
                "quality" : 200,
                "diameter" : 2.0,
                "flip" : false
            }
        },
        "atmosphere" 	: {
            "size" : 2580.0,
            "wavelengths" : [0.7, 0.8, 0.9],
            "m_Kr" : 0.0025,
            "m_Km" : 0.0015,
            "m_eSun" : 1.0,
            "fogdensity" : 2.5,
            "fogcolor" : [1.0, 0.7, 0.6],

            "params" : {
                "quality" : 600,
                "diameter" : 2.0,
                "flip" : true
            }
        }
    },
    {
        "name" : "Exonia c1 orbit",
        "color" : [0.8, 0.4, 0.4, 0.7],
        "componentType": [ "Orbits", "Moons" ],

        "parent" : "Exonia c", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "newmethod": true,
        "orbit" 		: {
            "mu" : 4.2e13,
            "period" : 1.2624407,
            "epoch" : 2455198.0,
            "semimajoraxis" : 23463.2,
            "eccentricity" : 0.00033,
            "inclination" : 1.791,
            "ascendingnode" : 0.370,
            "argofpericenter" : 0.233,
            "meananomaly" : 0.554
        }
    },
    {
        "name" : "Exonia c1",
        "color" : [0.5, 0.6, 1.0, 1.0],
        "size" : 410.3,
        "componentType" : "Moons",

        "absmag" : 0.5,

        "parent" : "Exonia c", 
        "archetype" : "Planet",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia c1 orbit"
        },

        "rotation" 		: {
            "period" : 40.536,
            "axialtilt" : 0.0,
            "inclination" : 0.281,
            "meridianangle" : 200.39
        },
        "model"			: {
            "args" : [true],
            "type" : "sphere",
            "params" : {
                "quality" : 400,
                "diameter" : 1.0,
                "flip" : false
            },
            "material" : {
                "height" : "generate",
                "diffuse" : "generate",
                "normal" : "generate",
                "specular" : "generate",
                "biomelut" : "data/tex/base/rock-smooth-lut.png",
                "biomehueshift" : 289.0,
                "noise" : {
                    "seed" : 963249243,
                    "scale" : 0.12,
                    "type" : "simplex",
                    "fractaltype" : "ridgemulti",
                    "frequency" : 3.0,
                    "octaves" : 8,
                    "range" : [0.0, 1.0],
                    "power" : 1.0
                },
                "heightScale" : 20.0
            }
        }
    },
    {
        "name" : "Exonia d orbit",
        "color" : [1.0, 0.7, 0.5, 0.7],
        "componentType": [ "Orbits", "Planets" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod" : true,
        "orbit" 		: {
            "period" : 3851.7655287755297,
            "epoch" : 2455198.0,
            "semimajoraxis" : 719622085.246326,
            "eccentricity" : 0.1,
            "inclination" : 0.93415027853740,
            "ascendingnode" : 130.74959784132,
            "argofpericenter" : 180.31984971,
            "meananomaly" : 0.40983227102735
        }

    },
    {
        "name" : "Exonia d",
        "color" : [0.71, 0.32, 0.08, 1.0],
        "size" : 7439.7,
        "componentType" : "Planets",

        "absmag" : -2.67,
        "appmag" : 5.73,

        "parent" : "Exonia Center", 
        "archetype" : "Planet",
        "refplane" : "equatorial",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia d orbit"
        },

        "rotation" 		: {
            "period" : 1407.509405,
            "axialtilt" : 2.1833,
            "inclination" : 7.005,
            "meridianangle" : 329.548
        },

        "model"			: {
            "args" : [true],
            "type" : "sphere",
            "params" : {
                "quality" : 400,
                "diameter" : 1.0,
                "flip" : false
            },
            "material" : {
                "height" : "generate",
                "diffuse" : "generate",
                "normal" : "generate",
                "specular" : "generate",
                "biomelut" : "data/tex/base/biome-smooth-lut.png",
                "biomehueshift" : -15.0,
                "noise" : {
                    "seed" : 993390,
                    "scale" : 0.1,
                    "type" : "simplex",
                    "fractaltype" : "ridgemulti",
                    "frequency" : 5.34,
                    "octaves" : 10,
                    "range" : [-1.4, 1.0],
                    "power" : 7.5
                },
                "heightScale" : 14.0
            }
        },
        "cloud"         : {
            "size" : 7475.0,
            "cloud" : "generate",
            "noise" : {
                "seed" : 1983,
                "scale": [1.8, 1.8, 1.0],
                "type" : "simplex",
                "fractaltype" : "ridgemulti",
                "frequency" : 2.34,
                "octaves" : 4,
                "range" : [-1.5, 0.8],
                "power" : 7.0
            },

            "params" : {
                "quality" : 200,
                "diameter" : 2.0,
                "flip" : false
            }
        },
        "atmosphere" 	: {
            "size" : 7730.0,
            "wavelengths" : [0.6, 0.56, 0.475],
            "m_Kr" : 0.0025,
            "m_Km" : 0.0015,
            "m_eSun" : 3.0,
            "fogdensity" : 4.5,
            "fogcolor" : [0.8, 0.9, 1.0],

            "params" : {
                "quality" : 600,
                "diameter" : 2.0,
                "flip" : true
            }
        }
    },
    {
        "name" : "Exonia e orbit",
        "color" : [0.2, 1.0, 0.5, 0.7],
        "componentType": [ "Orbits", "Planets" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod": true,
        "orbit"	: {
            "period" : 9905.85,
            "epoch" : 2455400.5,
            "semimajoraxis" : 1353350171.0,
            "eccentricity" : 0.08862,
            "inclination" : 9.134,
            "ascendingnode" : 83.91,
            "argofpericenter" : 149.84,
            "meananomaly" : 209.80
        }
    },
    {
        "name" : "Exonia e",
        "color" : [0.71, 0.32, 0.08, 1.0],
        "size" : 3389.7,
        "componentType" : "Planets",

        "absmag" : -2.67,
        "appmag" : 5.73,

        "parent" : "Exonia Center", 
        "archetype" : "Planet",
        "refplane" : "equatorial",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia e orbit"
        },

        "rotation" 		: {
            "period" : 1407.509405,
            "axialtilt" : 2.1833,
            "inclination" : 7.005,
            "meridianangle" : 329.548
        },

        "model"			: {
            "args" : [true],
            "type" : "sphere",
            "params" : {
                "quality" : 400,
                "diameter" : 1.0,
                "flip" : false
            },
            "material" : {
                "diffuse" : "data/tex/base/exoniae-diffuse.jpg",
                "emissive" : "data/tex/base/exoniae-emissive.jpg",
                "normal" : "data/tex/base/exoniae-normal.jpg",
                "metallic" : "data/tex/base/exoniae-metallic.jpg",
                "height" : "data/tex/base/exoniae-height.jpg",
                "heightScale" : 25.9848
            }
        },
        "atmosphere" 	: {
            "size" : 3500.0,
            "wavelengths" : [0.55, 0.5, 0.45],
            "m_Kr" : 0.0025,
            "m_Km" : 0.0015,
            "m_eSun" : 3.0,
            "fogdensity" : 4.5,
            "fogcolor" : [0.8, 0.9, 1.0],

            "params" : {
                "quality" : 600,
                "diameter" : 2.0,
                "flip" : true
            }
        }
    },
    {
        "name" : "Exonia f orbit",
        "color" : [1.0, 1.0, 0.4, 0.7],
        "componentType": [ "Orbits", "Planets" ],

        "parent" : "Exonia Center", 
        "archetype" : "Orbit",
        "provider" : "gaiasky.data.orbit.OrbitalParametersProvider",

        "transformFunction" : "galacticToEquatorial",
        "newmethod": true,
        "orbit"	: {
            "period" : 11095.85,
            "epoch" : 2455400.5,
            "semimajoraxis" : 1453350171.0,
            "eccentricity" : 0.08862,
            "inclination" : 9.134,
            "ascendingnode" : 83.91,
            "argofpericenter" : 149.84,
            "meananomaly" : 209.80
        }
    },
    {
        "name" : "Exonia f",
        "color" : [0.71, 0.72, 0.78, 1.0],
        "size" : 3389.7,
        "componentType" : "Planets",

        "absmag" : -2.67,
        "appmag" : 5.73,

        "parent" : "Exonia Center", 
        "archetype" : "Planet",
        "refplane" : "equatorial",

        "coordinates" : {
            "impl" : "gaiasky.util.coord.OrbitLintCoordinates",
            "orbitname" : "Exonia f orbit"
        },

        "rotation" 		: {
            "period" : 1407.509405,
            "axialtilt" : 2.1833,
            "inclination" : 7.005,
            "meridianangle" : 329.548
        },
        "seed" : [9858457687, 11448],
        "randomize" : [ "model", "atmosphere" ],
        "cloud"         : {
            "size" : 3400.0,
            "cloud" : "generate",
            "noise" : {
                "seed" : 1234,
                "scale": [0.05, 0.05, 0.4],
                "type" : "gradval",
                "fractaltype" : "decarpenterswiss",
                "frequency" : 5.34,
                "octaves" : 9,
                "range" : [-0.1, 1.0],
                "power" : 2.0
            },

            "params" : {
                "quality" : 200,
                "diameter" : 2.0,
                "flip" : false
            }
        }
    }

The orbits are essentially the same as in the case of stars. The objects are now using the archetype ``Planet``, and they define a rotation and a model. The rotation specifies the parameters of the orientation and rotation of the planet like the period, the axial tilt and the inclination. The model defines the 3D model object properties. In this case we use spheres. Within the model is the material, which defines the textures to use for each of the material attributes and optionally the procedural generation parameters. We also have clouds and atmospheres, but these are covered in the :ref:`procedural generation section <procedural-gen>`.

Here is a short clip of the system once loaded into Gaia Sky:

.. raw:: html

    <iframe id="lbry-iframe" width="100%" height="315" src="https://odysee.com/$/embed/gaia-sky-extrasolar-system-example/0dddba9a91c601398952e95edd066efbfe814508?r=621u1MynW1hV1p9kTVvSiB3pZyjj9tJW" allowfullscreen></iframe>
