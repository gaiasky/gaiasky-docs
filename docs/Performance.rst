
.. _performance:

Performance
***********

The performance of the application may vary significantly depending on the characteristics of your system. This chapter describes what are the factors that have an impact in a greater or lesser degree in the performance of the Gaia Sky and explains how to tweak them. It is organised in two parts, namely GPU performance (graphics performance) and CPU performance.

.. contents::
  :backlinks: none

.. _heap-memory:

Maximum heap memory
===================

Gaia Sky allocates a maximum heap memory value that can not be circumvented
but can be adjusted or modified. 
If you encounter an ``OutOfMemoryError``, chances are that your maximum
heap memory is not enough for your usage. The default values are 4 GB (Gaia Sky ``3.0.0`` 
and below) and 6 GB (Gaia Sky ``3.0.1+``).

In order to modify the maximum heap memory, follow the instructions below depending on 
your operating system.

Heap memory on Linux 
--------------------
On **Linux**, you need to edit the ``gaiasky`` executable script. It is usually
located in ``/opt/gaiasky/`` when installed from your package manager, or wherever
you extracted the package if installed from the ``tar.gz``. Edit the script and find
the line with ``-Xmx?g``, where ``?`` is the default max heap memory. Change it to your desired value.
For example, if you want to increase the maximum heap size to 12 GB, set it
to ``-Xmx12g``.

If installed using a ``.deb`` or ``.rpm``, you need to edit the ``/opt/gaiasky/gaiasky.vmoptions`` file and uncomment and edit the line that reads:

.. code::

    # -Xmx512m

into:

.. code::

    -Xmx12g

Where ``12g`` is the desired amount of heap space.

Heap memory on Windows
----------------------
On **Windows**, edit the file ``gaiasky.vmoptions`` in your Gaia Sky installation
folder, and uncomment the line that reads ``# -Xmx512m``, setting it to the 
heap space that you desire.  So, in order to set the maximum heap to 12 GB, 
edit it from:

.. code:: 
    
   # Enter one VM parameter per line
   # For example, to adjust the maixmum memory usage to 512 MB, uncomment the following line:
   # -Xmx512m
   # To include another file, uncomment the following line:
   # -include-options [path to other .vmoption file]

to:

.. code::

   # Enter one VM parameter per line
   # For example, to adjust the maixmum memory usage to 512 MB, uncomment the following line:
   -Xmx12g
   # To include another file, uncomment the following line:
   # -include-options [path to other .vmoption file]

Heap memory on macOS
--------------------
On **macOS**, you need to edit the file ``vmoptions.txt`` and uncomment the ``-Xmx`` line 
to suit your needs.

.. code:: bash

    /Applications/Gaia\ Sky.app/Contents/vmoptions.txt

So, in order to set the maximum heap to 12 GB, edit the ``/Applications/Gaia\ Sky/Contents/vmoptions.txt`` from:

.. code:: 
    
   # Enter one VM parameter per line
   # For example, to adjust the maixmum memory usage to 512 MB, uncomment the following line:
   # -Xmx512m
   # To include another file, uncomment the following line:
   # -include-options [path to other .vmoption file]

to:

.. code::

   # Enter one VM parameter per line
   # For example, to adjust the maixmum memory usage to 512 MB, uncomment the following line:
   -Xmx12g
   # To include another file, uncomment the following line:
   # -include-options [path to other .vmoption file]

If you are not comfortable editing files from the terminal, you can just open the
``Applications`` folder in Finder, right-click on Gaia Sky and select "Show Package
Contents". That gives you access to the application folder structure. Use Finder to navigate
to ``Gaia Sky.app/Contents/`` and use your favorite text editor to edit ``vmoptions.txt``.


Heap memory when running from source
------------------------------------

If you run from source you need to edit the ``core/build.gradle`` file. In there,
you will find a ``GaiaSkyRun`` class with a ``setup()`` method. In this method, is
a variable definition called ``maxHeapSpace``, whose value you need to modify. The
default value is ``6g``, for 6 GB of maximum heap space. You can increase it at will.


Graphics performance
====================
Refer to the :ref:`graphics-performance` chapter.


CPU performance
===============
The CPU also plays an obvious role in updating the scene state
(positions, orientations, etc.), managing the input and events,
executing the scripts and calling and running the rendering subsystem,
which streams all the texturing and geometric information to the GPU
for rendering. This section describes what are the elements that can
cause a major impact in CPU performance and explains how to tune them.


Multithreading
--------------
Gaia Sky uses background threads to index and update meta-information on the stars
that are currently in view. The multithreading option controls the number of 
threads devoted to these indexing and updating tasks. If multithreading is 
disabled, only one background thread is used. Otherwise, it uses the defined
number of threads in the setting.

.. raw:: html

   <div class="new new-prev">Since 2.0.1</div>



Limiting FPS
------------
Gaia Sky offers a way to limit the frames per second. This will
ease the CPU of some work, especially if the max FPS is set to a value lower than 60.
To do it, just edit the value in the preferences dialog, performance tab.

.. _draw-distance:



Draw distance (levels of detail)
--------------------------------
These settings apply only when using a catalog with levels of detail like ``Gaia DR2+``. You can configure whether you want smooth transitions between the levels (fade-outs and fade-ins) and also the draw distance, which is represented by a range slider. The draw distance is a solid angle threshold against which we compare the octree nodes to determine their visibility.

.. figure:: img/lodslider.jpg
  :align: center
  :alt: Levels of Detail slider

  Draw distance slider in preferences dialog

Basically, the slider sets the view angle above which a particular octree node (axis aligned cubic volume) 
is marked as observed and thus its stars are processed and drawn.

*  Set the knob to the **right** to lower the draw distance and increase performance.
*  Set the knob to the **left** to higher the draw distance at the expense of performance.

Find more in-depth information about this in the :ref:`data streaming section <data-streaming-lod>`.

.. figure:: img/lodoctree.png
  :alt: Octree and levels of detail
  :class: filter-invert

  Octree and levels of detail. Image: `Wikipedia <https://en.wikipedia.org/wiki/Octree>`__.
  
.. |theta| replace:: :math:`{\theta}`


Smooth transitions
------------------
This setting controls whether particles fade in and out depending on the octree view angle. This will prevent
pop-ins when using a catalog backed by an octree but it **will have a hit on peformance due to the opacity information
being sent to the GPU continuously**. If smooth transitions are enabled, there
is a fade-in between the draw distance angle angle and the draw distance angle + 0.4 rad.


.. _safe-mode:

Safe mode
=========

Gaia Sky includes a *safe mode*, in which graphical effects and capabilities are tuned down to adapt to ageing systems. You can activate it by running Gaia Sky with the ``--safe-mode`` CLI flag. Some of the measures in place during safe mode are the following:

- OpenGL 3.2 context instead of 4.2.
- Usage of regular 8-bit buffers instead of 32-bit float buffers for rendering.
- No tessellation, screen-space reflections, or other advanced effects.

Safe mode is automatically activated at startup on systems that do not support OpenGL 4.2.
