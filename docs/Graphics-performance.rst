.. _graphics-performance:

Graphics performance
********************

The Gaia Sky uses `OpenGL <https://www.opengl.org/>`__ to render
advanced graphics and thus its performance may be affected
significantly by your graphics card. Below you can find some tips to
improve the performance of the application by tweaking or deactivating
some graphical effects.

.. contents::
  :backlinks: none

Graphics quality setting
========================

Please see the :ref:`graphics-quality-setting` section.

.. raw:: html

   <div class="new">Since 3.2.0</div>

Dynamic resolution
==================

The dynamic resolution can improve the performance in demanding graphics situations and older hardware. See the :ref:`graphics-experimental` section for more information.

Star brightness
===============

The **star brightness** setting has an effect on the graphics
performance because it causes more or less stars to be rendered using the close-by mode where the floating camera transformation is applied in the CPU and the vertices are computed and sent each frame. The effect on performance should not be too great though, unless your CPU is very old. The star brightness can be increased or decreased from the ``Star brightness`` slider in the :ref:`interface-lighting` section.

.. hint:: :guilabel:`Ctrl` + :guilabel:`d` - activate **debug mode** to get some information on how many stars are currently being rendered as points and quads as well as the frames per second, frame time and more.


Star groups
===========

Star groups are an internal concept in Gaia Sky where a bunch of stars enter and leave the video memory together. Usually, a single catalog is loaded as a single star group, but it is not always the case. The main exception are the level-of-detail catalogs. In these, each octree node (octant) maps to a different star group.

A number of quantities are limited at the star group level. These are the maximum number
of quad star billboards, the maximum number of labels and the maximum number of velocity vectors.  All of these quantities have a rather strong impact on performance, and can be modified by editing the configuration file directly. They are not exposed in the GUI.

Billboards
----------

.. raw:: html

   <div class="new new-prev">Since 3.0.4</div>

Stars, when close to the camera, are rendered with high quality billboards. Billboards
are images which always face the camera (i.e. their normal vector is aligned with the
vector that joins the camera position with the object's position).

The number of stars that will be rendered as billboards has a strong impact
on performance, as we need to compute the quaternions to rotate the images correctly.
This number is capped to a maximum value set in the
configuration file. This number is set to 30 stars per star group by default, but you can
edit it by editing the following line in your ``config.yaml`` file.

.. code:: yaml

    scene:
        star:
            group:
                # Maximum number of billboards per star group
                numBillboard: 30


Labels
------

Object labels or names in the Gaia Sky are rendered using a special
shader which implements `distance field
fonts </libgdx/libgdx/wiki/Distance-field-fonts>`__. This means that
labels look great at all distances but it is costlier than the regular
method.

The label factor basically determines the stars for which a label will
be rendered if labels are active. It is a real number between 1 and 5,
and it is used to scale the *threshold angle point*, which
determines the solid angle boundary between rendering objects as *points* or as *quads*
to select whether a label should be rendered or not.

The label is rendered if the formula below yields true.

::

    solid_angle > threshold_angle_point / label_factor

The label number factor impacts how many labels are displayed. You can modify this 
value by editing your ``config.yaml`` file.

.. code:: yaml

    scene:
        label:
            # Label number factor. Controls how many stars have labels
            number: 1.3

.. raw:: html

   <div class="new new-prev">Since 3.0.4</div>


Additionally, the maximum number of labels per star group has a huge impact on performance and is also defined in the configuration file. The default value is 50.

.. code:: yaml

    scene:
        star:
            group:
                # Maximum number of labels per star group
                numLabels: 50


Velocity vectors
----------------

When active, velocity vectors can become a big toll on performance. To mitigate that,
you can adjust the number of vectors shown using the slider at the bottom of the
type visibility pane.

.. raw:: html

   <div class="new new-prev">Since 3.0.4</div>

Moreover, the maximum number of velocity vectors per star group is defined in the
configuration file. The default value is 500.

.. code:: yaml

    scene:
        star:
            group:
                # Maximum number of velocity vectors per star group
                numVelocityVector: 500

Model detail
============

Some models (mainly spherical planets, planetoids, moons and asteroids)
are automatically generated when the Gaia Sky is initializing and
accept parameters which tell the loader how many vertices the model
should have. These parameters are set in the ``json`` data files and can
have an impact on devices with low-end graphics processors. Let's see an
example:

.. code:: json

    {
        "model" : {
                "args" : [true],
                "type" : "sphere",
                "params" : {
                    "quality" : 150,
                    "diameter" : 1.0,
                    "flip" : false
                    },
                "texture" : {
                    "base" : "data/tex/neptune.jpg",
                    }
            }
    }

The ``quality`` parameter specifies here the number of both vertical and
horizontal divisions that the sphere will have.

Additionally, some other models, such as that of the Gaia spacecraft,
come from a binary model file ``.g3db``. These models are created using
a 3D modeling software and then exported to either ``.g3db`` (``bin``)
or ``.g3dj`` (``JSON``) using
`fbx-conv <https://github.com/libgdx/fbx-conv>`__. You can create
your own low-resolution models and export them to the right format. Then
you just need to point the ``json`` data file to the right low-res model
file. The attribute's name is ``model``.

.. code:: json

    {
        "model" : {
                "args" : [true],
                "model" : "data/models/gaia/gaia.g3db"
            }
    }

Bloom, lens flare and light glow
================================

All post-processing algorithms (those algorithms that are applied to the
image after it has been rendered) take a toll on the graphics card and
can be disabled.

.. hint:: Disable the **light glow** effect for a significant performance boost in low-end graphics cards

-  The **bloom** is not very taxing on fairly capable GPUs, but might be on integrated graphics.
-  The **lens flare** effect is a bit harder on the GPU, but most modern cards should be able to handle it with no problems. In order of cost, from less costly to more costly, the shaders are *SIMPLE*, *PSEUDO*, *COMPLEX*.
-  The **light glow** effect is far more demanding, and disabling it can result in a significant performance gain in some GPUs. It samples the image around the principal light sources using a spiral pattern and applies a light glow texture which is rather large.

To disable these post-processing effects, find the controls in the UI window, as described in the :ref:`graphics configuration section <graphics-conf>`.

.. _performance-antialiasing:

Antialiasing
============

Antialiasing is a term to refer to a number of techniques for **reducing
jagged edges**, stairstep-like lines that should be smooth. It reduces
the jagged appearance of lines and edges, but it also makes the image
smoother. The result are generally better looking images, even though
this depends on the resolution display device.

There are several groups of anti-aliasing techniques, some of them
implemented in the Gaia Sky and available for you to choose from the
:ref:`graphics settings <config-graphics>`. They all come at a
cost, which may vary depending on your system.

+-----------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Name                  | Type              | Description                                                                                                                                                                                                                    |
+=======================+===================+================================================================================================================================================================================================================================+
| **No Antialiasing**   | No antialiasing   | This has no cost since it does not apply any antialiasing technique.                                                                                                                                                           |
+-----------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **FXAA**              | Post-processing   | This has a mild performance cost and produces reasonably good results. If you have a good graphics card, this is super-fast.                                                                                                   |
+-----------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **NFAA**              | Post-processing   | Based on the creation of a normal map to identify edges, this is slightly costlier than FXAA but it may produce better results in some devices.                                                                                |
+-----------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Here are some sample images.

+-----------------------+-------------+
| Name                  | Image       |
+=======================+=============+
| **No Antialiasing**   | |NOAA|      |
+-----------------------+-------------+
| **FXAA**              | |FXAA|      |
+-----------------------+-------------+
| **NFAA**              | |NFAA|      |
+-----------------------+-------------+

.. |NOAA| image:: img/aa/NOAA.png
.. |FXAA| image:: img/aa/FXAA.png
.. |NFAA| image:: img/aa/NFAA.png

Some graphics drivers allow you to override the anti-aliasing settings
of applications with some default configuration (usually MSAA or FXAA).
To use this, select `No antialiasing` in Gaia Sky.

Find more information on anti-aliasing in the :ref:`graphics-antialiasing` section.
