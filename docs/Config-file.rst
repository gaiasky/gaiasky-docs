.. _configuration-file:

.. raw:: html

   <div class="new new-prev">Since 3.1.5</div>

The configuration file
**********************

There is a configuration file which stores the settings
of Gaia Sky. This file is in the `YAML format <https://yaml.org>`__ and is located in
``$GS_CONFIG/config.yaml`` (see :ref:`folders <folders>`). The default location is:

-  Linux: ``~/.config/gaiasky/config.yaml``
-  Windows: ``C:\Users\[username]\.gaiasky\config.yaml``
-  macOS: ``~/.gaiasky/config.yaml``

The default `config.yaml <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/conf/config.yaml>`__ file in our code repository is annotated with comments describing each setting.

.. contents::
  :backlinks: none

**The following sections document the settings that can only be modified by editing the configuration file itself**. The rest of settings can be edited from within Gaia Sky itself, usually using the preferences window or the control panel. A double colon ``::`` in the list below indicates nested settings.

Program settings
----------------

-  ``program::minimap::inWindow`` -- enables the rendering of the mini-map in a window.
-  ``program::net`` -- this group contains the configuration of the REST server, as well as the master-slave infrastructure. Find more information in the :ref:`connect instances <gaiasky-master-slave>` section.
-  ``program::scriptsLocation`` -- default location of script files in the file system.
-  ``program::ui::animationMs`` -- duration of UI animations in Gaia Sky, in milliseconds.
-  ``program::url`` -- contains the URLs for the version check (Codeberg API), the data repository mirror and the data descriptor file.
-  ``program::net`` -- contains the configuration of the REST API (port), and the master/slave instances. See :ref:`here <gaiasky-master-slave>` for more information.
-  ``program::offlineMode`` -- Gaia Sky won't attempt any HTTP connection to the internet in this mode. This means that the data descriptor file containing the information on server datasets can't be fetched. You need to download the desired datasets manually and extract them in your data folder. More information can be found in our `Gaia Sky datasets repository <https://gaia.ari.uni-heidelberg.de/gaiasky/repository>`__.
-  ``program::safeMode`` -- this is activated automatically whenever OpenGL incompatibilities are detected at startup. On macOS, this is on by default. Safe mode disables 'advanced' graphics features like 32-bit float buffers.

Controls settings
-----------------

-  ``controls::gamepad::blacklist`` -- a list of controller names to blacklist. You can find out the controller names recognized by Gaia Sky in the controls tab of the preferences window.

Graphics settings
-----------------

-  ``graphics::useSRGB`` -- use the sRGB color space as a frame buffer format. Only supported by OpenGL 3.2 and above. If this is activated, the internal format ``GL_SRGB8_ALPHA8`` is used. Only available when safe graphics mode is not active.
-  ``graphics::backBufferScale`` -- fixed scaling factor for the backbuffer. Increase this to improve image fidelity at the expense of performance. If dynamic resolution (:ref:`see this <graphics-experimental>`) is enabled, this setting is ignored. This setting is exposed to the UI as "Dynamic resolution" in the experimental graphics settings section.

Data settings
-------------

-  ``data::skybloxLocation`` -- contains the location of the default skybox used for reflections.

Scene settings
--------------

-  ``scene::renderer::line::glWidthBias`` -- additive bias to add to the line width when rendering lines using the driver ``GL_LINES`` method. This is useful because the implementation of ``GL_LINES`` depends on the vendor (driver), and different implementations may interpret the line width differently.
-  ``scene::star::saturate`` -- additive value to apply to the saturation value of star colors (in HSV color model) to oversaturate (positive) or desaturate (negative) stars.
-  ``scene::star::textureIndex`` -- the index of the texture used for stars. Star texture files are PNG files provided by the ``default-data`` package, and are of the form ``$data/default-data/tex/base/star-texture-[NUM].png``.
-  ``scene::star::textureIndexLens`` -- the index of the texture used for close-by stars in the light glow effect. Texture files are PNG files provided by the ``default-data`` package, and are of the form ``$data/default-data/tex/base/star-texture-[NUM].png``.
-  ``scene::star::group::numLabel`` -- the maximum number of labels rendered by any star set. Be careful with increasing this value, as it may have very negative effects on performance with LOD catalogs (like most of Gaia DRx). 
-  ``scene::octree::maxStars`` -- the maximum number of stars loaded at any single time from LOD catalogs.
-  ``scene::label::number`` -- controls the global number of stars in the scene by lowering the label solid angle threshold. Increase to get more labels, decrease to get less labels. 
-  ``scene::initialization`` -- contains the ``lazyTexture`` and ``lazyMesh`` properties, which enable the lazy initialization of textures and meshes respectively.

.. _conf-postprocess:

Post-processing settings
------------------------

-  ``postprocess::bloom::fboScale`` -- frame buffer scale factor (applied to the current viewport dimensions) to determine the frame buffer size to render the bloom effect.
-  ``postprocess::lensFlare::type`` -- choose the type of lens flare shader to use. Possible options are *SIMPLE* (a simple, nice-looking lens flare), *COMPLEX* (uses a complex and more demanding lens flare shader), and *PSEUDO* (uses a pseudo lens flare shader, described `here <https://john-chapman.github.io/2017/11/05/pseudo-lens-flare.html>`__).
-  ``postprocess::antialiasing::quality`` -- this setting only affects FXAA, and defines its quality. One of [0|1|2], from worse to better.
-  ``postprocess::lensFlare::numGhosts`` -- number of ghost artifacts of the **pseudo lens flare** shader. 
-  ``postprocess::lensFlare::haloWidth`` -- halo width of the **pseudo lens flare** shader.
-  ``postprocess::lensFlare::blurPasses`` -- number of blur passes for the **pseudo lens flare** shader.
-  ``postprocess::lensFlare::flareSaturation`` -- saturation value for the flare in the **pseudo lens flare** shader.
-  ``postprocess::lensFlare::bias`` -- bias value for the original image in the **pseudo lens flare** shader.
-  ``postprocess::lensFlare::texLensColor`` -- color lookup texture path for the **pseudo lens flare** shader.
-  ``postprocess::lensFlare::texLensDirt`` -- dirt texture path for all lens flare effects.
-  ``postprocess::lensFlare::texLensStarburst`` -- starburst texture path for all lens flare effects.
-  ``postprocess::lensFlare::fboScale`` -- scale of the frame buffer object to render the **pseudo lens flare** effect.
-  ``postprocess::lightGlow::samples`` -- number of samples to use to detect the brightness of the underlying star in the light glow effect/shader.
-  ``postprocess::warpingMesh::pfmFile`` -- absolute path to a PFM (portable float map) ``.pfm`` file that contains the warping mesh to apply. For more info, see :ref:`mesh-warping`.

Proxy settings
--------------

-  ``proxy`` -- configure an HTTP/HTTPS proxy. Find the full documentation to configure a proxy in the :ref:`proxy configuration section <proxy-configuration>`.
