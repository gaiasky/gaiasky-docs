.. _quick-start-guide:

.. role:: grey
.. role:: red
.. role:: green
.. role:: yellow
.. role:: orange
.. role:: blue

.. |start| image:: img/ui/iconic-play-circle.png
   :class: filter-invert
.. |download| image:: img/ui/iconic-data-transfer-download.png
   :class: filter-invert
.. |minimap| image:: img/ui/map-icon.png
   :class: filter-invert
.. |dsload| image:: img/ui/open-icon.png
   :class: filter-invert
.. |prefsicon| image:: img/ui/prefs-icon.png
   :class: filter-invert
.. |logicon| image:: img/ui/log-icon.png
   :class: filter-invert
.. |helpicon| image:: img/ui/help-icon.png
   :class: filter-invert
.. |quiticon| image:: img/ui/quit-icon.png
   :class: filter-invert
.. |time| image:: img/ui/iconic-clock.png
   :class: filter-invert
.. |camera| image:: img/ui/iconic-video.png
   :class: filter-invert
.. |eye| image:: img/ui/eye-icon.png
   :class: filter-invert
.. |bolt| image:: img/ui/iconic-bolt.png
   :class: filter-invert
.. |datasets| image:: img/ui/iconic-hard-drive.png
   :class: filter-invert
.. |log| image:: img/ui/iconic-map-marker.png
   :class: filter-invert
.. |bookmark| image:: img/ui/iconic-bookmark.png
   :class: filter-invert
.. |download-ds| image:: img/ui/iconic-arrow-circle-bottom.png
   :class: filter-invert
.. |terminal| image:: img/ui/iconic-terminal.png
   :class: filter-invert
.. |docs| image:: img/ui/iconic-book.png
   :class: filter-invert

   
Quick start guide
=================

   .. tip:: This guide is designed to be followed with the latest version of Gaia Sky!

The main aim of this quick start guide is to provide a concise on-ramp to the Gaia Sky platform by describing its operation and most common features.

**Gaia Sky crash course:** see this `companion web presentation <https://gaia.ari.uni-heidelberg.de/gaiasky/presentation/gaiasky-crash-course/>`__ for a visual introduction to Gaia Sky.

The topics covered in this guide are the following:

- Gaia Sky introduction:

  - :ref:`qs-dataset-manager`.
  - :ref:`Controls <qs-controls>`, :ref:`movement <qs-movement>`, :ref:`selection <qs-selection>`.
  - :ref:`User interface <qs-user-interface>`.
  - :ref:`Camera operation and modes <qs-camera>`.
  - :ref:`Render modes <qs-render-modes>` (3D, planetarium, 360, re-projection).
  - :ref:`Object and type visibility <qs-type-visibility>`.
  - :ref:`Visual settings <qs-visual-settings>`.
  - :ref:`qs-datasets` (loading, filters, SAMP).

- :ref:`qs-scripting`:

  - :ref:`Scripting basics <qs-scripting-basics>`.
  - :ref:`The API <qs-api>`.
  - :ref:`Showcase scripts <qs-showcase-scripts>`.
  - :ref:`Hands-on session <qs-handson>`.

- :ref:`Camera paths <qs-camera-paths>`:

  - :ref:`Recording and playback <qs-record-playback>`.
  - :ref:`Keyframes system <qs-keyframes>`.

- :ref:`Still frame output mode <qs-frame-output>`.
  
  - :ref:`Video from still frames <qs-ffmpeg>` with ``ffmpeg``.


Before starting...
------------------

In order to follow this guide it is strongly recommended to have a local installation of Gaia Sky. To install Gaia Sky, follow the instructions for your operating system in |docs| :ref:`the installation section <installation>`.

Welcome window
--------------

When we start up Gaia Sky, we are greeted with this view:

.. figure:: img/welcome/welcome-screen.jpg
    :alt: Gaia Sky welcome screen.
    :align: center
    :target: _images/welcome-screen.jpg

    The welcome screen in Gaia Sky contains buttons to start the program, open the dataset manager, access the settings and more.


From here, we have access to the global preferences (|prefsicon|, bottom-right), the help window (|helpicon|, bottom-right), fire up the |download| :guilabel:`Dataset manager`, or |start| :guilabel:`Start Gaia Sky`.

.. _qs-dataset-manager:

Dataset manager
---------------

The |download| :guilabel:`Dataset manager` is used to download, update, delete and enable/disable datasets. It consists of two tabs:

- :guilabel:`Available for download` -- contains datasets that are available to be downloaded and installed.  
- :guilabel:`Installed` -- contains the datasets currently available locally.

The datasets are downloaded directly from our servers over an encrypted HTTPS connection, and SHA256 checksums are used to verify the integrity of downloaded files.

The first time we start Gaia Sky we need to download at least the :yellow:`Base data pack` (dataset key: ``default-data``) to be able to start the program. This is a **REQUIRED** step. The base data pack contains essential data like most of the Solar System (planets, moons, orbits, asteroids, etc.), the Milky Way, grids, constellations and other important objects.


.. figure:: img/quickstart/202402_dataset-manager.jpg
    :alt: The dataset manager
    :align: center
    :target: _images/202402_dataset-manager.jpg

    The dataset manager with the base data pack selected.

We may download any dataset in the :guilabel:`Available for download` tab by clicking on the |download-ds| icon . 

Feel free to explore the available datasets.

The :guilabel:`Installed` tab shows the datasets that we have already downloaded and are available locally. 

- **Enable/disable** a dataset using the **checkbox** in the dataset pane. Enabled datasets are loaded when Gaia Sky starts. 
- **Remove** a dataset by right clicking with your mouse on it and selecting |binicon| :guilabel:`Remove`.

.. figure:: img/welcome/catalog-selection.jpg
    :alt: The Installed tab in the dataset manager.
    :align: center
    :target: _images/catalog-selection.jpg

    Enable and disable datasets from the :guilabel:`Installed` tab.

Now, close the dataset manager and |start| :guilabel:`Start Gaia Sky`.

.. _qs-controls:

Basic controls
--------------

When Gaia Sky is ready to go, we are presented with this screen:

.. figure:: img/quickstart/202402_ui-initial.jpg
    :alt: The scene
    :align: center
    :target: _images/202402_ui-initial.jpg

    Gaia Sky starts focussed on the Earth.

In it, we can see a few things already:

- To the bottom-right, the :ref:`camera info panel <camera-info-panel>` tells us that we are in focus mode, meaning that all our movement is relative to the focus object. The default focus of Gaia Sky is the Earth. 
- To the top, the :ref:`quick info bar <quick-info-bar>` tells us that our focus is the :green:`Earth`, and that the closest object to our location is also the :blue:`Earth`. Additionally, we see that our home object is, again, the :orange:`Earth`. 
- Anchored to the top-left, we see some buttons that give access to the :ref:`control panes <control-panes>`. If we click on one of these buttons, the respective pane opens. We will use them later.

.. _qs-movement:

Movement
^^^^^^^^

But right now let's try some movement. In **focus mode** the camera orbits around the focus object, always pointing in the direction of the focus. **Try clicking and dragging with your left mouse button**. The camera should orbit around the Earth showing parts of the surface which were previously hidden. You may notice that the whole scene rotates. Now **try scrolling with your mouse wheel**. The camera moves either farther away from (scroll down) or closer to (scroll up) the Earth. We can always press and hold :guilabel:`z` to speed-up the camera considerably. This is useful to traverse long distances quickly.

Now, if we **click and drag with your right mouse button**, you can offset the focus object from the center, but your movement will still be relative to it.

You can also use your keyboard arrows :guilabel:`←` :guilabel:`↑` :guilabel:`→` :guilabel:`↓` to orbit left or right around the focus object, or move closer to or away from it.

You can use :guilabel:`shift` with a **mouse drag** in order to roll the camera. 

.. admonition:: Docs

  See the :ref:`controls section <controls>` of the user manual for more.

.. _qs-selection:

Selection
^^^^^^^^^

You can change the focus by simply **double clicking** on any object on the scene. You can also press :guilabel:`f` to bring up the **search** dialog where you can look up objects by name. Try it now. Press :guilabel:`f` and type in "mars", without the quotes, and hit :guilabel:`esc`. You should see that the camera now points in the direction of :red:`Mars`. To actually go to :red:`Mars` simply scroll up until you reach it, or click on the |goto| icon next to the name in the focus info panel. If you do so, Gaia Sky takes control of the camera and brings you to :red:`Mars`. 

If you want to move instantly to your current focus object, hit :guilabel:`ctrl` + :guilabel:`g`.

Any time, we can use the :guilabel:`Home` key to return back to Earth (in fact, we return to the :orange:`home` object, which is defined in the :ref:`configuration file <configuration-file>`).

.. |goto| image:: img/ui/go-to.png
   :class: filter-invert

.. _qs-user-interface:

The user interface
------------------

The user interface of Gaia Sky consists of a few panes, buttons and windows. The most important of those are the **control panes**, accessible via a series of buttons anchored to the top-left.

.. figure:: img/quickstart/202402_ui-all.jpg
    :alt: Gaia Sky user interface
    :align: center
    :target: _images/202402_ui-all.jpg

    Gaia Sky user interface with the most useful functions

.. admonition:: Docs
   
  See the :ref:`user interface section <user-interface>` of the user manual for more information.

Control panes
^^^^^^^^^^^^^

The control panes (previously called *control panel* in the old UI---it can still be used but is off by default) are made up of seven different panes: 

- |time| Time -- shortcut: :guilabel:`t`.
- |camera| Camera -- shortcut: :guilabel:`c`.
- |eye| Type visibility -- shortcut: :guilabel:`v`.
- |bolt| Visual settings -- shortcut: :guilabel:`l`.
- |datasets| Datasets -- shortcut: :guilabel:`d`.
- |log| Location log.
- |bookmark| Bookmarks -- shortcut: :guilabel:`b`.

Each pane can be **expanded** and **collapsed** by clicking on the button or by using the respective keyboard shortcut (listed in the button tooltip).

Anchored to the bottom-left of the screen we can find six buttons to perform a few special actions:

- |minimap| Toggle the mini-map -- shortcut: :guilabel:`Tab`.
- |dsload| Load a dataset -- shortcut: :guilabel:`Ctrl` + :guilabel:`o`.
- |prefsicon| Open the preferences window -- shortcut: :guilabel:`p`.
- |logicon| Show the session log -- shortcut: :guilabel:`Alt` + :guilabel:`l`.
- |helpicon| Show the help dialog -- shortcut: :guilabel:`h` or :guilabel:`F1`.
- |quiticon| Exit Gaia Sky -- shortcut: :guilabel:`Esc`.

Camera info panel
^^^^^^^^^^^^^^^^^

The **camera info panel**, also known as **focus info pane**, is anchored to the bottom-right of the main window.

.. figure:: img/ui/camera-info-pane-focusmode.jpg
  :width: 35%                                  
  :align: center
  :target: _images/camera-info-pane-focusmode.jpg

  The camera info pane when the camera is in focus mode. In this state, it is also referred to as focus info pane, and it displays information on the focus (top), the mouse pointer (middle), and the camera position and state (bottom).

.. admonition:: Docs

   See the :ref:`camera info panel section <camera-info-panel>` of the user manual.


Quick info bar
^^^^^^^^^^^^^^

To the top of the screen, we can see the **quick info bar** which provides information on the current time, the :green:`current focus` object (if any), the :blue:`current closest` object to our location and the :orange:`current home` object. The colors of these objects (:green:`green`, :blue:`blue`, :orange:`orange`) correspond to the colors of the crosshairs. The crosshairs can be enabled or disabled from the interface tab in the preferences window (use :guilabel:`p` to bring it up).

.. figure:: img/ui/quick-info-bar.jpg
  :align: center
  :target: _images/quick-info-bar.jpg

  The quick info bar is anchored to the top of the window and displays useful information at a glance.

.. admonition:: Docs 

   See the :ref:`quick info bar section <quick-info-bar>` for more information.

System info panel
^^^^^^^^^^^^^^^^^

Gaia Sky has a built-in system information panel that provides system information and is hidden by default. We can bring it up with :guilabel:`ctrl` + :guilabel:`d`, or by ticking the :guilabel:`Show debug info`" check box in the |terminal| :guilabel:`System` tab of the preferences window. By default, the debug panel is collapsed.

.. figure:: img/debug/debug-collapsed.jpg
    :alt: Collapsed debug panel
    :align: center
    :target: _images/debug-collapsed.jpg

    Collapsed system info panel

Expand the system info panel with the :guilabel:`+` symbol to get additional information.

.. figure:: img/debug/debug-expanded.jpg
    :alt: Expanded debug panel
    :align: center
    :target: _images/debug-expanded.jpg

    Expanded system info panel

The debug panel shows information on the current graphics device, system and graphics memory, the amount of objects loaded and on display, the octree (if a LOD dataset is in use) or the SAMP status.

Additional debug information can be obtained in the system tab of the help dialog (:guilabel:`?` or :guilabel:`h`).

.. admonition:: Docs

   See the :ref:`system info panel section <debug-panel>` for a full description.

Time controls
^^^^^^^^^^^^^

.. tip:: Open the time pane by clicking on the clock |time| button, or by pressing :guilabel:`t`.

Gaia Sky can simulate time. Play and pause the simulation using the |play-icon|/|pause-icon| :guilabel:`Play/Pause` buttons in the time pane, or toggle using :guilabel:`Space`. 

The :guilabel:`Time warp` slider lets us modify the speed at which the simulation time runs w.r.t. real time. Use :guilabel:`,` or |bwd-icon| and :guilabel:`.` or |fwd-icon| to divide by 2 and double the value of the time warp respectively. If we keep either :guilabel:`.` or :guilabel:`,` pressed, the warp factor will increase or decrease steadily.

Use the :guilabel:`Reset time and warp` button to reset the time warp to x1, and set the time to the current real world time (UTC).

.. figure:: img/ui/pane-time.jpg  
   :width: 35%                                  
   :align: center
   :target: _images/pane-time.jpg
    
   The time pane in the controls window of Gaia Sky.


.. |play-icon| image:: img/ui/media/iconic-media-play.png
   :class: filter-invert
.. |pause-icon| image:: img/ui/media/iconic-media-pause.png
   :class: filter-invert
.. |fwd-icon| image:: img/ui/media/iconic-media-skip-forward.png
   :class: filter-invert
.. |bwd-icon| image:: img/ui/media/iconic-media-skip-backward.png
   :class: filter-invert

Now, go ahead and press :guilabel:`Home`. This will bring us back to Earth. Now, start the time with |play-icon| or :guilabel:`Space` and **drag the slider slightly to the right** to increase its speed. We see that the Earth rotates faster and faster as we move the slider to the right. Now, **drag it to the left** until time is reversed and the Earth starts rotating in the opposite direction. Now time is going backwards!

If we set the time warp high enough we will notice that as the bodies in the Solar System start going crazy, the stars start to slightly move. That's right: Gaia Sky also simulates proper motions.

.. _qs-camera:

Camera modes
------------

We have already talked about the **focus camera mode**, but Gaia Sky provides some more camera modes:

- :guilabel:`0` - **Free mode**: the camera is not locked to a focus object and can roam freely. The movement is achieved with the scroll wheel of the mouse, and the view is controlled by clicking and draggin the left and right mouse buttons
- :guilabel:`1` - **Focus mode**: the camera is locked to a focus object and its movement depends on it
- :guilabel:`2` - **Game mode**: similar to free mode but the camera is moved with :guilabel:`w`:guilabel:`a`:guilabel:`s`:guilabel:`d` and the view (pitch and yaw) is controlled with the mouse. This control system is commonly found in FPS (First-Person Shooter) games on PC
- :guilabel:`3` - **Spacecraft mode**: take control of a spacecraft (outside the scope of this tutorial)

The most interesting mode is **free mode** which lets us roam freely. Go ahead and press :guilabel:`0` to try it out. The controls are a little different from those of **focus mode**, but they should not be to hard to get used too. Basically, use the **left mouse button** to yaw and pitch the view, use :guilabel:`shift` to roll, and use the **right mouse button** to pan.

.. admonition:: Docs

   See the :ref:`camera modes section <camera-modes>` of the user manual.

.. _qs-render-modes:

Special render modes
--------------------

There are three special render modes: **3D mode**, **planetarium mode**, **panorama mode** and **orthosphere view**. We can access these modes using the buttons at the bottom of the camera pane or the following shortcuts:

- |3d-icon| or :guilabel:`ctrl` + :guilabel:`s` - 3D mode
- |dome-icon| or :guilabel:`ctrl` + :guilabel:`p` - Planetarium mode
- |cubemap-icon| or :guilabel:`ctrl` + :guilabel:`k` - Panorama mode
- |orthosphere-icon| or :guilabel:`ctrl` + :guilabel:`j` - Orthosphere view

.. |3d-icon| image:: img/ui/3d-icon.png
   :class: filter-invert
.. |dome-icon| image:: img/ui/dome-icon.png
   :class: filter-invert
.. |cubemap-icon| image:: img/ui/cubemap-icon.png
   :class: filter-invert
.. |orthosphere-icon| image:: img/ui/orthosphere-icon.png
   :class: filter-invert

.. admonition:: Docs

   See the :ref:`stereoscopic mode <3d-mode>`, the :ref:`planetarium mode <planetarium-mode>`, the :ref:`panorama mode <panorama-mode>`, and the :ref:`orthosphere view <orthosphere-mode>` sections of the user manual.

.. _qs-type-visibility:

Type visibility
---------------

.. tip:: Expand and collapse the visibility pane by clicking on the eye |eye| button or with :guilabel:`v`.

The visibility pane offers controls to hide and show object types. Object types are groups of objects that are of the same category, like stars, planets, labels, galaxies, grids, etc. The pane also contains a button at the bottom that gives access to the |docs| :ref:`per-object visibility window <individual-visibility>`, which enables visibility control for individual objects.

.. figure:: img/ui/pane-visibility.jpg  
  :width: 45%                                  
  :align: center
  :target: _images/pane-visibility.jpg
    
  The visibility pane contains controls to hide and show types of objects.

For example, we can hide the stars by clicking on the |stars| stars button. The object types available are the following:

-  |stars| -- Stars
-  |planets| -- Planets
-  |moons| -- Moons
-  |satellites| -- Satellites
-  |asteroids| -- Asteroids
-  |clusters| -- Star clusters
-  |milkyway| -- Milky Way
-  |galaxies| -- Galaxies
-  |nebulae| -- Nebulae
-  |meshes| -- Meshes
-  |equatorial| -- Equatorial grid
-  |ecliptic| -- Ecliptic grid
-  |galactic| -- Galactic grid
-  |labels| -- Labels
-  |titles| -- Titles
-  |orbits| -- Orbits
-  |locations| -- Locations
-  |cosmiclocations| -- Cosmic locations
-  |countries| -- Countries
-  |constellations| -- Constellations
-  |boundaries| -- Constellation boundaries
-  |ruler| -- Rulers
-  |effects| -- Particle effects
-  |atmospheres| -- Atmospheres
-  |clouds| -- Clouds
-  |axes| -- Axes
-  |arrows| -- Velocity vectors
-  |others| -- Others

.. |stars| image:: img/ui/ct/icon-elem-stars.png
   :class: filter-invert
.. |planets| image:: img/ui/ct/icon-elem-planets.png
   :class: filter-invert
.. |moons| image:: img/ui/ct/icon-elem-moons.png
   :class: filter-invert
.. |satellites| image:: img/ui/ct/icon-elem-satellites.png
   :class: filter-invert
.. |asteroids| image:: img/ui/ct/icon-elem-asteroids.png
   :class: filter-invert
.. |clusters| image:: img/ui/ct/icon-elem-clusters.png
   :class: filter-invert
.. |milkyway| image:: img/ui/ct/icon-elem-milkyway.png
   :class: filter-invert
.. |galaxies| image:: img/ui/ct/icon-elem-galaxies.png
   :class: filter-invert
.. |nebulae| image:: img/ui/ct/icon-elem-nebulae.png
   :class: filter-invert
.. |meshes| image:: img/ui/ct/icon-elem-meshes.png
   :class: filter-invert
.. |equatorial| image:: img/ui/ct/icon-elem-equatorial.png
   :class: filter-invert
.. |ecliptic| image:: img/ui/ct/icon-elem-ecliptic.png
   :class: filter-invert
.. |galactic| image:: img/ui/ct/icon-elem-galactic.png
   :class: filter-invert
.. |labels| image:: img/ui/ct/icon-elem-labels.png
   :class: filter-invert
.. |titles| image:: img/ui/ct/icon-elem-titles.png
   :class: filter-invert
.. |orbits| image:: img/ui/ct/icon-elem-orbits.png
   :class: filter-invert
.. |locations| image:: img/ui/ct/icon-elem-locations.png
   :class: filter-invert
.. |cosmiclocations| image:: img/ui/ct/icon-elem-cosmiclocations.png
   :class: filter-invert
.. |countries| image:: img/ui/ct/icon-elem-countries.png
   :class: filter-invert
.. |constellations| image:: img/ui/ct/icon-elem-constellations.png
   :class: filter-invert
.. |boundaries| image:: img/ui/ct/icon-elem-boundaries.png
   :class: filter-invert
.. |ruler| image:: img/ui/ct/icon-elem-ruler.png
   :class: filter-invert
.. |effects| image:: img/ui/ct/icon-elem-effects.png
   :class: filter-invert
.. |atmospheres| image:: img/ui/ct/icon-elem-atmospheres.png
   :class: filter-invert
.. |clouds| image:: img/ui/ct/icon-elem-clouds.png
   :class: filter-invert
.. |axes| image:: img/ui/ct/icon-elem-axes.png
   :class: filter-invert
.. |arrows| image:: img/ui/ct/icon-elem-arrows.png
   :class: filter-invert
.. |others| image:: img/ui/ct/icon-elem-others.png
   :class: filter-invert

Velocity vectors
^^^^^^^^^^^^^^^^

One of the elements, the **velocity vectors**, enable a few properties when selected.

*  **Number factor** -- control how many velocity vectors are rendered. The stars are sorted by magnitude (ascending) so the brightest stars will get velocity vectors first
*  **Length factor** -- length factor to scale the velocity vectors
*  **Color mode** -- choose the color scheme for the velocity vectors
*  **Show arrowheads** -- Whether to show the vectors with arrow caps or not

.. tip:: Control the width of the velocity vectors with the **line width** slider in the **visual settings** pane.

.. figure:: img/screenshots/velocity-vectors.jpg
    :alt: Velocity vectors in Gaia Sky
    :align: center
    :target: _images/velocity-vectors.jpg

    Velocity vectors in Gaia Sky

.. admonition:: Docs

   See the :ref:`velocity vectors section <velocityvectors>` of the user manual.

.. _qs-visual-settings:

Visual settings
---------------

.. tip:: Expand and collapse the visual settings pane by clicking on the |bolt| bolt button or with :guilabel:`l`.

The **visual settings** pane contains a few options to control the shading of stars and other elements:

-  **Star brightness** -- control the brightness of stars.
-  **Magnitude multiplier** -- exponent of power function that controls the brightness of stars. Controls the brightness difference between bright and faint stars.
-  **Star glow factor** -- close-by star size.
-  **Point size** -- size of point-like stars and other objects.
-  **Base star level** -- the minimum brightness level for all stars.
-  **Ambient light** -- control the amount of ambient light. This only affects the models such as the planets or satellites.
-  **Line width** -- control the width of all lines in Gaia Sky (orbits, velocity vectors, etc.).
-  **Label size** -- control the size of the labels.
-  **Elevation multiplier** -- scale the height representation for planets with elevation maps.

.. figure:: img/ui/pane-visual-settings.jpg
    :alt: Visual settings pane
    :align: center
    :target: _images/pane-visual-settings.jpg

    The visual settings pane.

.. _qs-datasets:

External datasets
-----------------

We can also load datasets into Gaia Sky at run time. Right now, the VOTable, CSV and FITS formats are supported. Gaia Sky needs some metadata in the form of *UCDs* or column names in order to parse the dataset columns correctly. 

.. admonition:: Docs

   See to the :ref:`stil-data-provider` section of the Gaia Sky user manual for more information on how to prepare your datasets for Gaia Sky.

The datasets loaded in Gaia Sky at a certain moment can be found in the :ref:`datasets pane <datasets-pane>` of the control panel. Open it by clicking on the |datasets| button or by pressing :guilabel:`d`.

.. figure:: img/quickstart/qs-datasets-pane.jpg
    :alt: Datasets pane
    :align: center
    :target: _images/gs-datasets-pane.jpg

    Datasets pane of Gaia Sky.


There are four main ways to load new datasets into Gaia Sky:

* Directly from the UI, using the |dsload| button (anchored to the bottom-left) or pressing :guilabel:`ctrl` + :guilabel:`o`.
* Through :ref:`SAMP <samp>`, via a connection to another astronomy software package such as Topcat or Aladin.
* Via a script, using one of the `dataset loading API calls <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/gaiasky/script/IScriptingInterface.html#loadDataset(java.lang.String,java.lang.String)>`__.
* Creating a dataset in the Gaia Sky format so that it appears in the dataset manager (see :ref:`here <catalog-formats>`.

.. admonition:: Docs

   See the :ref:`data format section <catalog-formats>` to know how to create a Gaia Sky dataset (advanced!).

**Loading a dataset from the UI** -- Go ahead and remove the current star catalog by clicking on the |binicon| icon in the datasets pane. Now, download a raw `Hipparcos dataset VOTable <https://gaia.ari.uni-heidelberg.de/gaiasky/files/catalogs/hip/hipparcos.vot>`__, click on the |dsload| icon (or press :guilabel:`ctrl` + :guilabel:`o`) and select the file. In the next dialog just click :guilabel:`Ok` to start loading the catalog. In a few moments the Hipparcos new reduction dataset should be loaded into Gaia Sky.

**Loading a dataset via SAMP** -- This section presupposes that Topcat is installed on the machine and that the user knows how to use it to connect to the VO to get some data. The following video demonstrates how to do this (`Odysee mirror <https://odysee.com/@GaiaSky:8/gaia-sky-loading-data-with-topcat:9>`__, `YouTube mirror <https://youtu.be/sc0q-VbeoPE>`__):

.. figure:: img/quickstart/qs-gaiasky-topcat.jpg
	:width: 500px
	:align: center
	:target: https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/tap-load-topcat.mkv

	Loading a dataset from Topcat through SAMP (click for video)

**Loading a dataset via scripting** -- Wait for the scripting section of this course.

**Preparing a descriptor file** -- Not addressed in this tutorial. See :ref:`the catalog formats section <catalog-formats>` for more information.

Working with datasets
^^^^^^^^^^^^^^^^^^^^^

.. tip:: Expand and collapse the datasets pane by clicking on the hard disk |datasets| button or with :guilabel:`d`.

All datasets loaded are displayed in the datasets pane in the control panel.
A few useful tips for working with datasets:

-  The visibility of individual datasets can be switched on and off by clicking on the |eye-s-on| button
-  Remove datasets with the |binicon| button
-  We can **highlight a dataset** by clicking on the |highlight-s-off| button. The highlight color is defined by the color selector right on top of it. Additionally, we can map an attribute to the highlight color using a color map. Let's try it out:
  
    1.  Click on the color box in the Hipparcos dataset we have just loaded from Topcat via SAMP
    2.  Select the radio button "Color map"
    3.  Select the *rainbow* color map
    4.  Choose the attribute. In this case, we will use the number of transits, *ntr*
    5.  Click :guilabel:`Ok`
    6.  Click on the highlight dataset |highlight-s-off| icon to apply the color map

-  We can **define basic filters** on the objects of the dataset using their attributes from the dataset preferences window |prefs-s-icon|. For example, we can filter out all stars with :math:`\delta > 50^{\circ}`:

    1.  Click on the dataset preferences button |prefs-s-icon|
    2.  Click on :guilabel:`Add filter`
    3.  Select your attribute (declination :math:`\delta`)
    4.  Select your comparator (:math:`<`)
    5.  Enter your value, in this case 50
    6.  Click :guilabel:`Ok`
    7.  The stars with a declination greater than 50 degrees should be filtered out

Multiple filters can be combined with the **AND** and **OR** operators

.. |binicon| image:: img/ui/bin-icon.png
   :class: filter-invert
.. |eye-s-on| image:: img/ui/eye-s-on.png
   :class: filter-invert
.. |prefs-s-icon| image:: img/ui/prefs-s-icon.png
   :class: filter-invert
.. |highlight-s-off| image:: img/ui/highlight-s-off.png
   :class: filter-invert

External information
--------------------

Gaia Sky offers three ways to display external information on the current focus object: **Wikipedia**, **Gaia archive** and **Simbad**.

.. figure:: img/quickstart/external-info.jpg
    :align: center
    :alt: External information
    :target: _images/external-info.jpg

    Wikipedia, Gaia archive and Simbad connections

-  The :guilabel:`+Info` button opens a view that contains the local data on the object, and a preview of the Wikipedia article on this object, if it exists.
-  When the :guilabel:`Archive` button appears in the focus info pane, it means that the full table information of selected star can be pulled from the Gaia archive.
-  When the ``Simbad`` link appears in the focus info pane, it means that the objects has been found on Simbad, and we can click the link to open it in the web browser.

.. _qs-scripting:

Scripting
---------

Gaia Sky exposes an API that is accessible through Python (via Py4j) or through HTTP over a network (using the :ref:`REST API HTTP server <rest-server>`). 

In this tutorial, we focus on the writing of Python scripts that tap into the Gaia Sky API. You will need `Python 3 <https://python.org>`__ installed, along with the packages NumPy and Py4j. 

To **install** the packages, run this in a terminal:

.. code:: bash

  pip3 install --user numpy py4j

Once you have those installed, you can run a script with the system Python 3 interpreter. Of course, you need to launch Gaia Sky in the same computer for the connection to succeed. Right now, only local scripting is supported. If you need to operate Gaia Sky over the network, have a look at the :ref:`REST API section <rest-server>`.

To **run a script** named ``my-gaiasky-script.py``, run this in a terminal:

.. code:: bash

   python3 my-gaiasky-script.py

If everything works well, the connection should succeed and Gaia Sky should react accordingly.

But wait, we don't have a script to run yet! Do not fret, in the next section we learn the basics of writing a script for Gaia Sky.

.. admonition:: Docs

   See the :ref:`scripting section <scripting>` in the user manual.


.. _qs-scripting-basics:

A basic script
^^^^^^^^^^^^^^

Writing a basic script is quite simple. Essentially, you need a header that imports Py4j and creates the connection object. Then, you can start using the connection object to run calls.

The following script simply connects to Gaia Sky and prints *"Hello from a script!"* to both Python and the Gaia Sky log.

.. code:: python

  from py4j.clientserver import ClientServer, JavaParameters

  gateway = ClientServer(java_parameters=JavaParameters(auto_convert=True))
  gs = gateway.entry_point

  # User code goes here.
  # We use the 'gs' object to access the API.

  # Let's print something.
  message = "Hello from a script!"
  # Print to Gaia Sky.
  gs.print(message)
  # Print with Python.
  print(message)

  # Shutdown the gateway at the end.
  gateway.shutdown()

Note that you need to **shutdown** the gateway at the end, this is important to clean things up and be able to run more scripts afterwards!
 
It is cool that we can print messages, but what other actions can we perform via scripting? Read on to know more about the API.

.. _qs-api:

Gaia Sky API
^^^^^^^^^^^^

The Gaia Sky **API** (`here <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/gaiasky/script/IScriptingInterface.html>`__) contains many more calls to interact with the platform in real time from Python scripts or a REST HTTP server. The API includes calls to:

- Add and remove messages and images to the interface,
- start and stop time, and change the time warp,
- add scene elements like shapes, lines, etc.,
- load full datasets in VOTable, CSV, FITS, or the internal JSON format,
- manage datasets (highlight, change settings, etc.),
- manipulate the camera position, orientation and mode,
- move the camera by simulating mouse actions (rotate around, forward, etc.),
- activate special modes like planetarium or panorama,
- create smooth camera transitions in position and orientation,
- change the various settings and preferences,
- back-up and restore the full configuration state,
- take screenshots, use the frame output mode.

The API specification is documented in the links below:

- `Latest API version <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/gaiasky/script/IScriptingInterface.html>`__
- `Older API versions (javadoc) <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/>`__.

.. _qs-showcase-scripts:

Showcase scripts
^^^^^^^^^^^^^^^^

The Gaia Sky repository contains many test and showcase scripts that may help with getting up to speed with Gaia Sky scripting. Many of them contain comments explaining what is going on:

- Interesting **showcase scripts** can be found `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/scripts/showcases>`__.
- Basic **testing scripts** can be found `here <https://codeberg.org/gaiasky/gaiasky/src/branch/master/assets/scripts/tests>`__.

.. _qs-handson:

Hands-on session
^^^^^^^^^^^^^^^^

Here, we have a look at some real world scripts (`full file listing <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/>`__), and write our own to later run them on Gaia Sky. 

- `Scripting presentation <https://www.dropbox.com/scl/fi/sctuhkvru2x4gawx51for/Gaia-Sky-Scripting-and-Video-Tutorial-DPAC-Plenary_Nice_Stefan_Jordan.pdf?rlkey=g61lgkjiy4yvvqh283k91xp1j&dl=0>`__ (dropbox link). 

The proposed scripts are:

- `Locating_the_Hyades_tidal_tails.py <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/Locating_the_Hyades_tidal_tails.py>`__ -- a simple sequential script which exemplifies some of the most common API calls, and can be used to capture a video. The script requires the following data and subtitles files to run (save them in the same directory as the script):

    - `Aldebaran.vot <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/Aldebaran.vot>`__
    - `Hyades_stars.csv <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/Hyades_stars.csv>`__
    - `Hyades_subtitles.srt <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/Hyades_subtitles.srt>`__
    - `distSDR3_N.csv <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/distSDR3_N.csv>`__

- `line-objects-update.py <https://gaia.ari.uni-heidelberg.de/gaiasky/files/temp/scripts/quickstartguide/line-objects-update.py>`__ -- a script showcasing the feature to run scripting code within the Gaia Sky main loop, so that it runs synchronized with the main loop, every frame. This is used to run update operations every single frame. In our test script, we create a line between the Earth and the Moon, start the time simulation, and update the position of the line every frame so that it stays in sync with the scene.

.. _qs-camera-paths:

Camera paths
-------------

Gaia Sky includes a feature to record and play back camera paths. This comes in handy if we want to showcase a certain itinerary through a dataset, for example.

.. _qs-record-playback:

**Recording a camera path** --- The system will capture the camera state at every frame and save it into a ``.gsc`` (for Gaia Sky camera) file. We can start a recording by clicking on the |rec| icon in the camera pane of the control panel. Once the recording mode is active, the icon will turn red |recred|. Click on it again in order to stop recording and save the camera file to disk with an auto-generated file name (default location is ``$GS_DATA/camera`` (see the :ref:`folders <folders>` section in the Gaia Sky documentation).

**Playing a camera path** --- In order to playback a previously recorded ``.gsc`` camera file, click on the |play| icon and select the desired camera path. The recording will start immediately.

.. tip:: **Mind the FPS!** The camera recording system stores the position of the camera for every frame! It is important that recording and playback are done with the same (stable) frame rate. To set the target recording frame rate, edit the "Target FPS" field in the camcorder settings of the preferences window. That will make sure the camera path is using the right frame rate. In order to play back the camera file at the right frame rate, we can edit the "Maximum frame rate" input in the graphics settings of the preferences window.

.. figure:: img/quickstart/202402_camera-paths.jpg
    :alt: Camrecorder
    :align: center
    :target: _images/202402_camera-paths.jpg

    Location of the controls of the camcorder in Gaia Sky

.. |rec| image:: img/ui/rec-icon-gray.png
   :class: filter-invert
.. |recred| image:: img/ui/rec-icon-red.png
.. |play| image:: img/ui/play-icon.png
   :class: filter-invert


.. admonition:: Docs

   See the :ref:`camera paths section <camera-paths>` in the user manual.

.. _qs-keyframes:

Keyframe system
^^^^^^^^^^^^^^^

The camera path system offers an additional way to define camera paths based on keyframes. Essentially, the user defines the position and orientation of the camera at certain times and the system generates the camera path from these definitions. Gaia Sky incorporates a whole keyframe definition system which is outside the scope of this tutorial.

As a very short preview, in order to bring up the keyframes window to start defining a camera path, click on the icon |keyframes|. 

.. |keyframes| image:: img/ui/rec-key-icon-gray.png
   :class: filter-invert

.. admonition:: Docs

   See the :ref:`keyframes system section <keyframe-system>` in the user manual.

.. _qs-frame-output:

Frame output mode
-----------------

In order to create high-quality videos, Gaia Sky offers the possibility to export every single still frame to an image file using the :ref:`frame output subsystem <frame-output>`. The resolution of these still frames can be set independently of the current screen resolution.

We can start the frame output system by pressing :guilabel:`F6`. Once active, the system starts saving each still frame to disk (frame rate goes down, most probably). The save location of the still frame images is, by default, ``$GS_DATA/frames/[prefix]_[num].jpg``, where ``[prefix]`` is an arbitrary string that can be defined in the preferences. The save location, mode (simple or advanced), and the resolution can also be defined in the preferences.

.. figure:: img/quickstart/2023_frame-output-prefs.jpg
    :alt: Frame output
    :align: center
    :target: _images/2023_frame-output-prefs.jpg

    The configuration screen for the frame output system

.. _qs-ffmpeg:

Create a video with ``ffmpeg``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once we have the still frame images, we can convert them to a video using ``ffmpeg`` or any other encoding software. Additional information on how to convert the still frames to a video can be found in the :ref:`capturing videos section <capture-videos>` of the Gaia Sky user manual.

Conclusion
----------

Congratulations! You have reached the end of the quick start guide. You are now a totally legit Gaia Sky master ;)
