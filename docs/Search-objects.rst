.. _search-objects:

Search objects
**************

.. figure:: img/ui/search-dialog.jpg
  :alt: The search dialog in Gaia Sky
  :align: center
  :target: _images/search-dialog.jpg

  The search dialog in Gaia Sky

.. hint:: You can search objects by pressing :guilabel:`f`, :guilabel:`/` or :guilabel:`Shift` + :guilabel:`f` at any time.

You can look up any object by name by pressing the search key binding (see info box above). This brings up the search dialog and focuses the search input field. Just enter the name of the object in that input field and Gaia Sky will focus it immediately if there is an exact match. Otherwise, search suggestions are shown as you type, with the most relevant results at the top. Use :guilabel:`Tab` to cycle between them, and :guilabel:`Enter` to focus on the current selection.

.. note:: In level-of-detail (LOD) catalogs, only stars that have been loaded are included in the index, and thus are searchable. If you don't find a specific star within a LOD catalog, chances are that the star has not been loaded. More info in :ref:`LOD catalogs section <data-streaming-lod>`. 

.. video:: _static/video/search-cutout-s.mp4
  :alt: Interactive search in Gaia Sky
  :class: video
  :autoplay:
  :loop:

A successful search puts the camera in :ref:`focus mode <camera-modes>`. 
